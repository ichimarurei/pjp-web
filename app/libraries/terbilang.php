<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of terbilang
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Terbilang {

    public function format($nilai, $ucase = 'l') {
        if ($nilai < 0) {
            $hasil = 'minus ' . trim(self::_penyebut($nilai));
        } else {
            $hasil = trim(self::_penyebut($nilai));
        }

        if ($ucase === 'l') {
            $hasil = strtolower($hasil);
        } else if ($ucase === 'u') {
            $hasil = strtoupper($hasil);
        }

        return $hasil;
    }

    private function _penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas');
        $temp = '';

        if ($nilai < 12) {
            $temp = ' ' . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = self::_penyebut($nilai - 10) . ' belas';
        } else if ($nilai < 100) {
            $temp = self::_penyebut($nilai / 10) . ' puluh' . self::_penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = ' seratus' . self::_penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = self::_penyebut($nilai / 100) . ' ratus' . self::_penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = ' seribu' . self::_penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = self::_penyebut($nilai / 1000) . ' ribu' . self::_penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = self::_penyebut($nilai / 1000000) . ' juta' . self::_penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = self::_penyebut($nilai / 1000000000) . ' milyar' . self::_penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = self::_penyebut($nilai / 1000000000000) . ' trilyun' . self::_penyebut(fmod($nilai, 1000000000000));
        }

        return $temp;
    }

}
