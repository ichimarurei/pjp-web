<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Tabel</li><li>Invoice</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Invoice
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Invoice</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Proyek</th>
                                                    <th>Nomor</th>
                                                    <th>Tanggal</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="invoice">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="presensi-input" name="presensi-input" value="">
                                <input type="hidden" id="penyetuju-input" name="penyetuju-input" value="">
                                <input type="hidden" id="nomor-input" name="nomor-input" value="">
                                <input type="hidden" id="thp-input" name="thp-input" value="">
                                <input type="hidden" id="perusahaan-input" name="perusahaan-input" value="">
                                <input type="hidden" id="peralatan-input" name="peralatan-input" value="">
                                <input type="hidden" id="total-input" name="total-input" value="">
                                <input type="hidden" id="bulat-input" name="bulat-input" value="">
                                <input type="hidden" id="fee-input" name="fee-input" value="">
                                <input type="hidden" id="pph-input" name="pph-input" value="">
                                <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                <input type="hidden" id="status-input" name="status-input" value="">
                                <input type="hidden" id="klien-input" name="klien-input" value="">
                                <input type="hidden" id="catatan-input" name="catatan-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var urlParam = '<?php echo ($this->uri->segment(5) === FALSE) ? 'proses' : $this->uri->segment(5); ?>';

            $(document).ready(function () {
                if (urlParam === 'tagih') {
                    urlParam = 'proses';
                }

                initTable("<?php echo site_url('data/tabel/invoice/0'); ?>___" + urlParam, '#dt_basic', [
                    {"data": "proyek"},
                    {"data": "nomor"},
                    {"data": "waktu"},
                    {"data": "status"},
                    {"data": "aksi", 'width': '17%'}
                ]);

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        var tabelnya = $('#dt_basic').DataTable();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/invoice'); ?>/" + $('#proyek-filter').val() + '___' + urlParam).load();
                        tabelnya.columns.adjust().draw();
                    }
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/ig/dataInv'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData);
                    });

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#presensi-input').val(response['data'].presensi);
                        $('#penyetuju-input').val(response['data'].penyetuju);
                        $('#nomor-input').val(response['data'].nomor);
                        $('#thp-input').val(response['data'].thp);
                        $('#perusahaan-input').val(response['data'].perusahaan);
                        $('#peralatan-input').val(response['data'].peralatan);
                        $('#total-input').val(response['data'].total);
                        $('#bulat-input').val(response['data'].bulat);
                        $('#fee-input').val(response['data'].fee);
                        $('#pph-input').val(response['data'].pph);
                        $('#ppn-input').val(response['data'].ppn);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#status-input').val(response['data'].status);
                        $('#klien-input').val(response['data'].klien);
                        $('#catatan-input').val(response['data'].catatan);
                        doSave('Dihapus', "<?php echo site_url('modul/tampil/ig/tabelInv/' . $this->uri->segment(5)); ?>", $('#model-form'));
                    }
                });
            }
        </script>
    </body>
</html>