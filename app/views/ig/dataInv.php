<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Pendataan</li><li>Invoice</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Invoice
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Invoice</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="invoice">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                                    <input type="hidden" id="nomor-input" name="nomor-input" value="">
                                                    <input type="hidden" id="thp-input" name="thp-input" value="">
                                                    <input type="hidden" id="perusahaan-input" name="perusahaan-input" value="">
                                                    <input type="hidden" id="peralatan-input" name="peralatan-input" value="">
                                                    <input type="hidden" id="total-input" name="total-input" value="">
                                                    <input type="hidden" id="fee-input" name="fee-input" value="">
                                                    <input type="hidden" id="pph-input" name="pph-input" value="">
                                                    <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                                    <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Disetujui</label>
                                                            <div class="col-md-10">
                                                                <select style="width:100%" class="select2 form-control" id="penyetuju-input" name="penyetuju-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Klien</label>
                                                            <div class="col-md-10" style="display: none;">
                                                                <select style="width:100%" class="select2 form-control" id="klien-input" name="klien-input"></select>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="Klien" type="text" id="klien-text" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Catatan</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Catatan Terkait Invoice" rows="6" id="catatan-input" name="catatan-input" readonly></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pembulatan</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control nominal-text" placeholder="Pembulatan Nominal Total Invoice (Rp)" type="text" id="bulat-input" name="bulat-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Status</label>
                                                            <div class="col-md-10">
                                                                <select style="width:100%" class="select2 form-control" id="status-input" name="status-input">
                                                                    <option value="proses">DITAGIHKAN</option>
                                                                    <option value="bayar">SUDAH DIBAYAR</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-sm-5">
                                                <h1 class="font-400 text-right">INVOICE</h1>
                                                <br>
                                                <div>
                                                    <div>
                                                        <strong>NO :</strong>
                                                        <span class="pull-right" id="nomor-text"> XXXXX </span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="font-md">
                                                        <strong>TANGGAL :</strong>
                                                        <span class="pull-right" id="tanggal-text"> <i class="fa fa-calendar"></i> XXXXX </span>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="well well-sm  bg-color-darken txt-color-white no-border">
                                                    <div class="fa-lg">
                                                        Total :
                                                        <span class="pull-right"><b id="total-text"> Rp. 0 </b></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%;" class="text-center">NO</th>
                                                    <th style="width: 45%;" class="text-center">URAIAN</th>
                                                    <th style="width: 10%;" class="text-center">VOLUME</th>
                                                    <th style="width: 20%;" class="text-center">BIAYA</th>
                                                    <th style="width: 20%;" class="text-center">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id="urai-tabel-isi">
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <form class="form-horizontal">
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <a class="btn btn-default batal-button" id="batal1-button" href="<?php echo site_url('modul/tampil/ig/tabelInv/tagih'); ?>">Batal</a>
                                                                <a class="btn btn-default batal-button" id="batal2-button" href="<?php echo site_url('modul/tampil/ig/tabelInv/bayar'); ?>">Batal</a>
                                                                <button class="btn btn-primary" type="button" id="simpan-button">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var totalin = {};

            $(document).ready(function () {
                $('.batal-button').hide();
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');
                var urlProyek = '<?php echo ($this->uri->segment(6) === FALSE) ? 0 : $this->uri->segment(6); ?>';

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#klien-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Klien'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/klien'); ?>/" + urlProyek,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#penyetuju-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Disetujui Oleh'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + urlProyek,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#simpan-button').click(function () {
                    if ($('#bulat-input').val().trim() !== '') {
                        if ($('#penyetuju-input').val() === null) {
                            swal({
                                title: 'Peringatan',
                                html: 'Mohon pilih pihak yang "Menyetujui Invoice"!',
                                timer: 3000,
                                type: 'warning',
                                showConfirmButton: false
                            });
                        } else {
                            $.blockUI({message: '<h1>Memproses...</h1>'});
                            nominalFormats();
                            doSave('Disimpan', '<?php echo site_url('modul/tampil/ig/tabelInv/tagih'); ?>', $('#model-form'));
                        }
                    } else {
                        swal({
                            title: 'Peringatan',
                            html: 'Nominal Pembulatan tidak boleh kosong (isikan default value <b>"Rp. 0"</b>) jika tidak ingin dilakukan pembulatan <b>Total Invoice</b>!',
                            timer: 7000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#nomor-input').val(response['data'].nomor);
                        $('#thp-input').val(response['data'].thp);
                        $('#perusahaan-input').val(response['data'].perusahaan);
                        $('#peralatan-input').val(response['data'].peralatan);
                        $('#total-input').val(response['data'].total);
                        $('#bulat-input').val(response['data'].rp_bulat);
                        $('#fee-input').val(response['data'].fee);
                        $('#pph-input').val(response['data'].pph);
                        $('#ppn-input').val(response['data'].ppn);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#catatan-input').val(response['data'].catatan);
                        $('#status-input').val(response['data'].status).trigger('change');
                        // view
                        var bulatan = parseInt(response['data'].bulat);
                        $('#nomor-text').html(response['data'].nomor);
                        $('#tanggal-text').html('<i class="fa fa-calendar"></i> ' + response['data'].tanggal);
                        $('#total-text').html((bulatan > 0) ? response['data'].rp_bulat : response['data'].rp_total);
                        fillUrai(response['data']);

                        if (response['data'].status === 'bayar') {
                            $('#batal2-button').show();
                            $('#simpan-button').hide();
                        } else {
                            $('#batal1-button').show();
                        }

                        if (response['data'].key > 0) {
                            var pilihKlien = $('#klien-input');
                            var pilihBio = $('#penyetuju-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=klien&kode=' + response['data'].klien,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKlien.append(new Option(data['data'].nama + ' (@' + data['data'].id + ')', data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKlien.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                                $('#klien-text').val(data['data'].nama);
                            });

                            if (response['data'].penyetuju !== '-') {
                                $.ajax({
                                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + response['data'].penyetuju,
                                    dataType: 'json', type: 'POST', cache: false
                                }).then(function (data) {
                                    // create the option and append to Select2
                                    pilihBio.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                                    // manually trigger the `select2:select` event
                                    pilihBio.trigger({
                                        type: 'select2:select',
                                        params: {
                                            data: data['data']
                                        }
                                    });
                                });
                            }
                        }
                    }
                });
            }

            function fillUrai(objek) {
                var htmlUrai = '<tr>';
                htmlUrai += '<td class="text-center"><b>1</b></td>';
                htmlUrai += '<td colspan="4"><b>Gaji/Upah Tenaga Kerja (THP)</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += uraianBaris(objek.thp, 1);
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>2</b></td>';
                htmlUrai += '<td colspan="4"><b>Over Head Perusahaan</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += uraianBaris(objek.perusahaan, 2);
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>3</b></td>';
                htmlUrai += '<td colspan="4"><b>Peralatan Khusus</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += uraianBaris(objek.peralatan, 3);
                var biaya = totalin.urai1 + totalin.urai2 + totalin.urai3;
                htmlUrai += '<tr>';
                htmlUrai += '<td colspan="2" class="text-center"><b>TOTAL BIAYA</b></td>';
                htmlUrai += '<td colspan="3" class="text-right"><b>' + formatRp(biaya.toString()) + '</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>4</b></td>';
                htmlUrai += '<td><b>Management Fee</b></td>';
                htmlUrai += '<td colspan="2" class="text-center"><b>' + objek.persen_fee + '</b></td>';
                htmlUrai += '<td class="text-right"><b>' + objek.rp_fee + '</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>5</b></td>';
                htmlUrai += '<td><b>PPN</b></td>';
                htmlUrai += '<td colspan="2" class="text-center"><b>' + objek.persen_ppn + '</b></td>';
                htmlUrai += '<td class="text-right"><b>' + objek.rp_ppn + '</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>6</b></td>';
                htmlUrai += '<td><b>PPH 23</b></td>';
                htmlUrai += '<td colspan="2" class="text-center"><b>' + objek.persen_pph + '</b></td>';
                htmlUrai += '<td class="text-right"><b>' + objek.rp_pph + '</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>7</b></td>';
                htmlUrai += '<td colspan="3"><b>Grand Total</b></td>';
                htmlUrai += '<td class="text-right"><b>' + objek.rp_total + '</b></td>';
                htmlUrai += '</tr>';
                htmlUrai += '<tr>';
                htmlUrai += '<td class="text-center"><b>8</b></td>';
                htmlUrai += '<td colspan="3"><b>Pembulatan</b></td>';
                htmlUrai += '<td class="text-right"><b>' + objek.rp_bulat + '</b></td>';
                htmlUrai += '</tr>';
                $('#urai-tabel-isi').html(htmlUrai);
            }

            function uraianBaris(param, at) {
                var htmlUrai = '';
                var params = param.split('___');
                var subTotal = 0;

                $.each(params, function (index, value) {
                    var values = value.split('::');
                    htmlUrai += '<tr>';
                    htmlUrai += '<td class="text-center">&nbsp;</td>';
                    htmlUrai += '<td>' + values[0] + '</td>';
                    htmlUrai += '<td class="text-center">' + values[1] + '</td>';
                    htmlUrai += '<td class="text-right">' + values[2] + '</td>';
                    htmlUrai += '<td class="text-right">' + values[3] + '</td>';
                    htmlUrai += '</tr>';
                    subTotal += parseInt(rpToNum(values[3]));
                });

                htmlUrai += '<tr>';
                htmlUrai += '<td colspan="2" class="text-right"><b>Sub Total ' + at + '</b></td>';
                htmlUrai += '<td colspan="3" class="text-right"><b>' + formatRp(subTotal.toString()) + '</b></td>';
                htmlUrai += '</tr>';
                totalin['urai' + at] = subTotal;

                return htmlUrai;
            }
        </script>
    </body>
</html>