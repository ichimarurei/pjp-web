<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$kodeanParam = $this->uri->segment(5);
$rProyek = NULL;
$lTHP = array();
$lKantor = array();
$lAlat = array();
$jmlPersonil = 0;
$tempSubTotal = array('thp' => 'Rp. 0', 'kantor' => 'Rp. 0', 'alat' => 'Rp. 0', 'total' => 'Rp. 0', 'fee' => 'Rp. 0', 'ppn' => 'Rp. 0', 'pph' => 'Rp. 0', 'final' => 'Rp. 0');

if ($kodeanParam !== FALSE) {
    $rProyek = $this->model->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $kodeanParam, 'terpakai' => 1)));
}

if ($rProyek != NULL) {
    $upahVals = explode(':', $rProyek->upah);
    $biayaVals = explode(':', $rProyek->biaya);
    $jmlPersonil = intval($rProyek->personil);
    $umk = 0;
    $upahan = 0;
    $bayaran = 0;
    $sewaan = 0;

    foreach ($upahVals as $value) {
        $values = explode('=', $value);
        $info = explode('|', $values[0]);
        $nominal = 0;

        if (strpos(strtolower($values[1]), 'rp') !== FALSE) {
            $nominal = intval(str_replace('.', '', substr($values[1], 4)));
        }

        if (strpos(strtolower($info[1]), 'gaji') !== FALSE) {
            $umk = $nominal;
        }

        $diUpah = ($nominal * $jmlPersonil);
        $upahan += $diUpah;
        $lTHP[$info[0]] = array($info[1], $values[1], 'Rp. ' . number_format($diUpah, 0, ',', '.'));
    }

    foreach ($biayaVals as $value) {
        $values = explode('=', $value);
        $info = explode('|', $values[0]);
        $nominal = 0;
        $diBayar = 0;
        $diSewa = 0;
        $bayarNo = NULL;
        $sewaNo = NULL;

        if (strpos(strtolower($values[1]), 'rp') !== FALSE) {
            $nominal = intval(str_replace('.', '', substr($values[1], 4)));
        }

        if (strpos(strtolower($info[1]), 'bpjs') !== FALSE) {
            if (strpos(strtolower($values[1]), '%') !== FALSE) {
                $nominal = round((floatval(str_replace(' %', '', $values[1])) / 100) * $umk);
                $bayarNo = 'Rp. ' . number_format($nominal, 0, ',', '.');
                $diBayar = ($nominal * $jmlPersonil);
            }
        } else {
            $isKantor = FALSE;

            if (strpos(strtolower($info[1]), 'seragam') !== FALSE) {
                $diBayar = ($nominal * ($jmlPersonil * 2));
                $isKantor = TRUE;
            }

            if (strpos(strtolower($info[1]), 'pelatihan') !== FALSE) {
                $diBayar = ($nominal * $jmlPersonil);
                $isKantor = TRUE;
            }

            if ($isKantor) {
                $bayarNo = 'Rp. ' . number_format($diBayar, 0, ',', '.');
                $diBayar = round($diBayar / 12);
            } else {
                $sewaNo = 'Rp. ' . number_format($nominal, 0, ',', '.');
                $diSewa = round($nominal / 12);
            }
        }

        if ($bayarNo !== NULL) {
            $bayaran += $diBayar;
            $lKantor[$info[0]] = array($info[1], $bayarNo, 'Rp. ' . number_format($diBayar, 0, ',', '.'));
        }

        if ($sewaNo !== NULL) {
            $sewaan += $diSewa;
            $lAlat[$info[0]] = array($info[1], $sewaNo, 'Rp. ' . number_format($diSewa, 0, ',', '.'));
        }
    }

    $lKantor[random_string('unique')] = array('THR', 'Rp. ' . number_format($upahan, 0, ',', '.'), 'Rp. ' . number_format(round($upahan / 12), 0, ',', '.'));
    $tempSubTotal['thp'] = 'Rp. ' . number_format($upahan, 0, ',', '.');
    $tempSubTotal['kantor'] = 'Rp. ' . number_format($bayaran, 0, ',', '.');
    $tempSubTotal['alat'] = 'Rp. ' . number_format($sewaan, 0, ',', '.');
    $totalan = $sewaan + $bayaran + $upahan;
    $feean = round((floatval($rProyek->fee) / 100) * $totalan);
    $ppnan = round((floatval($rProyek->ppn) / 100) * $feean);
    $finalan = $totalan + $feean + $ppnan;
    $pphan = round((floatval($rProyek->pph) / 100) * $finalan);
    $finalan += $pphan;
    $tempSubTotal['total'] = 'Rp. ' . number_format($totalan, 0, ',', '.');
    $tempSubTotal['fee'] = 'Rp. ' . number_format($feean, 0, ',', '.');
    $tempSubTotal['ppn'] = 'Rp. ' . number_format($ppnan, 0, ',', '.');
    $tempSubTotal['pph'] = 'Rp. ' . number_format($pphan, 0, ',', '.');
    $tempSubTotal['final'] = 'Rp. ' . number_format($finalan, 0, ',', '.');
}

// invoicing
$lInvoice = $this->model->getList(array('table' => 'data_proyek_invoice', 'where' => array('YEAR(waktu)' => date('Y'), 'MONTH(waktu)' => date('m'))));
$noInv = count($lInvoice) + 1;
$txtInv = '';

for ($at = 4; $at > strlen($noInv); $at--) {
    $txtInv .= '0';
}

$txtInv = $txtInv . '' . $noInv . '/PJP-INV/' . $this->formatdate->getMonthRoman(date('n')) . '/' . date('y');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Pendataan</li><li>Invoice & Penggajian</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Invoice & Penggajian
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Invoice & Penggajian</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="invoice">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                                    <input type="hidden" id="penyetuju-input" name="penyetuju-input" value="">
                                                    <input type="hidden" id="nomor-input" name="nomor-input" value="">
                                                    <input type="hidden" id="thp-input" name="thp-input" value="">
                                                    <input type="hidden" id="perusahaan-input" name="perusahaan-input" value="">
                                                    <input type="hidden" id="peralatan-input" name="peralatan-input" value="">
                                                    <input type="hidden" id="total-input" name="total-input" value="">
                                                    <input type="hidden" id="bulat-input" name="bulat-input" value="">
                                                    <input type="hidden" id="fee-input" name="fee-input" value="">
                                                    <input type="hidden" id="pph-input" name="pph-input" value="">
                                                    <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                                    <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                    <input type="hidden" id="status-input" name="status-input" value="">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-7">
                                                                <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                                            </div>
                                                            <div class="col-md-3 text-right">
                                                                <button class="btn btn-info" type="button" id="pilih-button">
                                                                    <i class="fa fa-check"></i>
                                                                    Pilih
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Klien</label>
                                                            <div class="col-md-10">
                                                                <select style="width:100%" class="select2 form-control" id="klien-input" name="klien-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Catatan</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Catatan Terkait Invoice" rows="6" id="catatan-input" name="catatan-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-sm-5">
                                                <h1 class="font-400 text-right">INVOICE</h1>
                                                <br>
                                                <div>
                                                    <div>
                                                        <strong>NO :</strong>
                                                        <span class="pull-right"> <?php echo $txtInv; ?> </span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="font-md">
                                                        <strong>TANGGAL :</strong>
                                                        <span class="pull-right"> <i class="fa fa-calendar"></i> <?php echo $this->formatdate->getDate(date('Y-m-d')); ?> </span>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="well well-sm  bg-color-darken txt-color-white no-border">
                                                    <div class="fa-lg">
                                                        Total :
                                                        <span class="pull-right"><b id="total-span"><?php echo $tempSubTotal['final']; ?></b></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%;" class="text-center">NO</th>
                                                    <th style="width: 45%;" class="text-center">URAIAN</th>
                                                    <th style="width: 10%;" class="text-center">VOLUME</th>
                                                    <th style="width: 20%;" class="text-center">BIAYA</th>
                                                    <th style="width: 20%;" class="text-center">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center"><b>1</b></td>
                                                    <td colspan="4"><b>Gaji/Upah Tenaga Kerja (THP)</b></td>
                                                </tr>
                                                <?php foreach ($lTHP as $thpKode => $thpVal) { ?>
                                                    <tr>
                                                        <td class="text-center">&nbsp;</td>
                                                        <td class="thp-isi-text"><?php echo $thpVal[0]; ?></td>
                                                        <td class="text-center"><input class="form-control volume1-isian" placeholder="Volume" type="text" style="width: 70%; margin: 0 auto; text-align: center;" id="volume-<?php echo $thpKode; ?>-isian" data-untuk="<?php echo $thpKode; ?>-total" data-biaya="<?php echo $thpVal[1]; ?>" value="<?php echo $jmlPersonil; ?>" /></td>
                                                        <td class="text-right thp-biaya-text"><?php echo $thpVal[1]; ?></td>
                                                        <td class="text-right total1-isian" id="<?php echo $thpKode; ?>-total"><?php echo $thpVal[2]; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td colspan="2" class="text-right"><b>Sub Total 1</b></td>
                                                    <td colspan="3" class="text-right"><b id="subtotal1-isian"><?php echo $tempSubTotal['thp']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>2</b></td>
                                                    <td colspan="4"><b>Over Head Perusahaan</b></td>
                                                </tr>
                                                <?php foreach ($lKantor as $kantorVal) { ?>
                                                    <tr>
                                                        <td class="text-center">&nbsp;</td>
                                                        <td class="kantor-isi-text"><?php echo $kantorVal[0]; ?></td>
                                                        <td class="text-center kantor-voulme-text"><?php echo (strpos(strtolower($kantorVal[0]), 'seragam') !== FALSE) ? ($jmlPersonil * 2) : $jmlPersonil; ?></td>
                                                        <td class="text-right kantor-biaya-text"<?php echo ($kantorVal[0] === 'THR') ? ' id="thr-biaya-isian"' : ''; ?>><?php echo $kantorVal[1]; ?></td>
                                                        <td class="text-right total2-isian"<?php echo ($kantorVal[0] === 'THR') ? ' id="thr-total-isian"' : ''; ?>><?php echo $kantorVal[2]; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td colspan="2" class="text-right"><b>Sub Total 2</b></td>
                                                    <td colspan="3" class="text-right"><b id="subtotal2-isian"><?php echo $tempSubTotal['kantor']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>3</b></td>
                                                    <td colspan="4"><b>Peralatan Khusus</b></td>
                                                </tr>
                                                <?php foreach ($lAlat as $alatKode => $alatVal) { ?>
                                                    <tr>
                                                        <td class="text-center">&nbsp;</td>
                                                        <td class="alat-isi-text"><?php echo $alatVal[0]; ?></td>
                                                        <td class="text-center"><input class="form-control volume2-isian" placeholder="Volume" type="text" style="width: 70%; margin: 0 auto; text-align: center;" id="volume-<?php echo $alatKode; ?>-isian" data-untuk="<?php echo $alatKode; ?>-biaya" data-hasil="<?php echo $alatKode; ?>-total" data-biaya="<?php echo $alatVal[1]; ?>" value="1" /></td>
                                                        <td class="text-right alat-biaya-text" id="<?php echo $alatKode; ?>-biaya"><?php echo $alatVal[1]; ?></td>
                                                        <td class="text-right total3-isian" id="<?php echo $alatKode; ?>-total"><?php echo $alatVal[2]; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td colspan="2" class="text-right"><b>Sub Total 3</b></td>
                                                    <td colspan="3" class="text-right"><b id="subtotal3-isian"><?php echo $tempSubTotal['alat']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center"><b>TOTAL BIAYA</b></td>
                                                    <td colspan="3" class="text-right"><b id="subtotal4-isian"><?php echo $tempSubTotal['total']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>4</b></td>
                                                    <td><b>Management Fee</b></td>
                                                    <td colspan="2" class="text-center"><b><?php echo ($rProyek != NULL) ? str_replace('.00', '', $rProyek->fee) : 0; ?>%</b></td>
                                                    <td class="text-right"><b id="subtotal5-isian"><?php echo $tempSubTotal['fee']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>5</b></td>
                                                    <td><b>PPN</b></td>
                                                    <td colspan="2" class="text-center"><b><?php echo ($rProyek != NULL) ? str_replace('.00', '', $rProyek->ppn) : 0; ?>%</b></td>
                                                    <td class="text-right"><b id="subtotal6-isian"><?php echo $tempSubTotal['ppn']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>6</b></td>
                                                    <td><b>PPH 23</b></td>
                                                    <td colspan="2" class="text-center"><b><?php echo ($rProyek != NULL) ? str_replace('.00', '', $rProyek->pph) : 0; ?>%</b></td>
                                                    <td class="text-right"><b id="subtotal7-isian"><?php echo $tempSubTotal['pph']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>7</b></td>
                                                    <td colspan="3"><b>Grand Total</b></td>
                                                    <td class="text-right"><b id="subtotal8-isian"><?php echo $tempSubTotal['final']; ?></b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><b>8</b></td>
                                                    <td colspan="3"><b>Pembulatan</b></td>
                                                    <td class="text-right"><input class="form-control" placeholder="Pembulatan" type="text" style="text-align: right;" id="bulat-isian" value="Rp. 0" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <form class="form-horizontal">
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <a class="btn btn-default" href="<?php echo site_url('modul/tampil/ig/tabelRekap'); ?>">Batal</a>
                                                                <button class="btn btn-primary" type="button" id="simpan-button">
                                                                    <i class="fa fa-save"></i>
                                                                    Simpan
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            var kodeProyek = '<?php echo ($rProyek != NULL) ? $rProyek->kode : 'none'; ?>';

            $(document).ready(function () {
                fillInputs();

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#klien-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Klien'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/klien'); ?>/" + kodeProyek,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#pilih-button').click(function () {
                    // reload page
                    if ($('#proyek-filter').val() !== null) {
                        $(location).attr('href', "<?php echo site_url('modul/tampil/ig/dataIG'); ?>/" + $('#proyek-filter').val());
                    }
                });
                $(".volume1-isian").keyup(function () {
                    aksiVolume($(this), true);
                });
                $(".volume2-isian").keyup(function () {
                    aksiVolume($(this), false);
                });
                $("#bulat-isian").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                if (kodeProyek !== 'none') {
                    var pilihProyek = $('#proyek-filter');
                    $.ajax({
                        url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + kodeProyek,
                        dataType: 'json', type: 'POST', cache: false
                    }).then(function (data) {
                        // create the option and append to Select2
                        pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                        // manually trigger the `select2:select` event
                        pilihProyek.trigger({
                            type: 'select2:select',
                            params: {
                                data: data['data']
                            }
                        });
                    });
                }

                $('#simpan-button').click(function () {
                    if ($('#bulat-isian').val().trim() !== '') {
                        if (isiUraian()) {
                            $.blockUI({message: '<h1>Memproses...</h1>'});
                            itungUlang();
                            $('#bulat-input').val(rpToNum($('#bulat-isian').val()));
                            doSave('Disimpan', "<?php echo site_url('modul/tampil/ig/tabelInv/tagih'); ?>", $('#model-form'));
                        } else {
                            swal({
                                title: 'Peringatan',
                                html: 'Terjadi kesalahan pada saat memproses <b>Uraian Invoice</b>',
                                timer: 3000,
                                type: 'error',
                                showConfirmButton: false
                            });
                        }
                    } else {
                        swal({
                            title: 'Peringatan',
                            html: 'Nominal Pembulatan tidak boleh kosong (isikan default value <b>"Rp. 0"</b>) jika tidak ingin dilakukan pembulatan <b>Total Invoice</b>!',
                            timer: 7000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
            });

            function fillInputs() {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=invoice&kode=0',
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#penyetuju-input').val(response['data'].penyetuju);
                        $('#catatan-input').val(response['data'].catatan);
                        $('#nomor-input').val('<?php echo $txtInv; ?>');
                        $('#proyek-input').val(kodeProyek);

                        if (response['data'].key > 0) {
                            var pilihKlien = $('#klien-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=klien&kode=' + response['data'].klien,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKlien.append(new Option(data['data'].nama + ' (@' + data['data'].id + ')', data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKlien.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                        }
                    }
                });
            }

            function aksiVolume(input, isKantor) {
                var biaya = parseInt(rpToNum(input.data('biaya')));
                var volume = input.val();
                var total = 0;

                if ($.isNumeric(volume)) {
                    total = parseInt(volume) * biaya;
                }

                if (!isKantor) {
                    var bagian = Math.round(total / 12);
                    $('#' + input.data('hasil')).text(formatRp(bagian.toString()));
                }

                $('#' + input.data('untuk')).text(formatRp(total.toString()));
                itungUlang();
            }

            function itungUlang() {
                var total1 = 0;
                var total2 = 0;
                var total3 = 0;
                var total4 = 0;
                var total5 = 0;
                var total6 = 0;
                var total7 = 0;
                var total8 = 0;
                var cicilan = 0;

                $('.total1-isian').each(function (i, obj) {
                    total1 += parseInt(rpToNum($(obj).text()));
                });

                $('.total2-isian').each(function (i, obj) {
                    total2 += parseInt(rpToNum($(obj).text()));
                });

                $('.total3-isian').each(function (i, obj) {
                    total3 += parseInt(rpToNum($(obj).text()));
                });

                if (total1 > 0) {
                    cicilan = Math.round(total1 / 12);
                }

                total4 = total1 + total2 + total3;
                var feeProyek = '<?php echo ($rProyek == NULL) ? 'none' : $rProyek->fee; ?>';
                var ppnProyek = '<?php echo ($rProyek == NULL) ? 'none' : $rProyek->ppn; ?>';
                var pphProyek = '<?php echo ($rProyek == NULL) ? 'none' : $rProyek->pph; ?>';

                if (feeProyek !== 'none') {
                    total5 = Math.round((parseFloat(feeProyek) / 100) * total4);
                    total6 = Math.round((parseFloat(ppnProyek) / 100) * total5);
                }

                total8 = total4 + total5 + total6;

                if (feeProyek !== 'none') {
                    total7 = Math.round((parseFloat(pphProyek) / 100) * total8);
                }

                total8 += total7;
                $('#subtotal1-isian').text(formatRp(total1.toString()));
                $('#subtotal2-isian').text(formatRp(total2.toString()));
                $('#subtotal3-isian').text(formatRp(total3.toString()));
                $('#subtotal4-isian').text(formatRp(total4.toString()));
                $('#subtotal5-isian').text(formatRp(total5.toString()));
                $('#subtotal6-isian').text(formatRp(total6.toString()));
                $('#subtotal7-isian').text(formatRp(total7.toString()));
                $('#subtotal8-isian').text(formatRp(total8.toString()));
                $('#thr-biaya-isian').text(formatRp(total1.toString()));
                $('#thr-total-isian').text(formatRp(cicilan.toString()));
                $('#total-span').html($('#subtotal8-isian').html());
                // input invoice
                $('#fee-input').val(total5);
                $('#ppn-input').val(total6);
                $('#pph-input').val(total7);
                $('#total-input').val(total8);
            }

            function isiUraian() {
                var thpIsiStr = [];
                var thpVolStr = [];
                var thpJmlStr = [];
                var thpNumStr = [];
                var kantorIsiStr = [];
                var kantorVolStr = [];
                var kantorJmlStr = [];
                var kantorNumStr = [];
                var alatIsiStr = [];
                var alatVolStr = [];
                var alatJmlStr = [];
                var alatNumStr = [];
                var thpStr = '';
                var kantorStr = '';
                var sewaStr = '';

                $('.thp-isi-text').each(function (i, obj) {
                    thpIsiStr.push($(obj).text().trim());
                });
                $('.volume1-isian').each(function (i, obj) {
                    thpVolStr.push($(obj).val().trim());
                });
                $('.thp-biaya-text').each(function (i, obj) {
                    thpJmlStr.push($(obj).text().trim());
                });
                $('.total1-isian').each(function (i, obj) {
                    thpNumStr.push($(obj).text().trim());
                });
                $.each(thpIsiStr, function (index, value) {
                    if (thpStr !== '') {
                        thpStr += '___';
                    }

                    thpStr += value.trim() + '::' + thpVolStr[index].trim() + '::' + thpJmlStr[index].trim() + '::' + thpNumStr[index].trim();
                });

                $('.kantor-isi-text').each(function (i, obj) {
                    kantorIsiStr.push($(obj).text().trim());
                });
                $('.kantor-voulme-text').each(function (i, obj) {
                    kantorVolStr.push($(obj).text().trim());
                });
                $('.kantor-biaya-text').each(function (i, obj) {
                    kantorJmlStr.push($(obj).text().trim());
                });
                $('.total2-isian').each(function (i, obj) {
                    kantorNumStr.push($(obj).text().trim());
                });
                $.each(kantorIsiStr, function (index, value) {
                    if (kantorStr !== '') {
                        kantorStr += '___';
                    }

                    kantorStr += value.trim() + '::' + kantorVolStr[index].trim() + '::' + kantorJmlStr[index].trim() + '::' + kantorNumStr[index].trim();
                });

                $('.alat-isi-text').each(function (i, obj) {
                    alatIsiStr.push($(obj).text().trim());
                });
                $('.volume2-isian').each(function (i, obj) {
                    alatVolStr.push($(obj).val().trim());
                });
                $('.alat-biaya-text').each(function (i, obj) {
                    alatJmlStr.push($(obj).text().trim());
                });
                $('.total3-isian').each(function (i, obj) {
                    alatNumStr.push($(obj).text().trim());
                });
                $.each(alatIsiStr, function (index, value) {
                    if (sewaStr !== '') {
                        sewaStr += '___';
                    }

                    sewaStr += value.trim() + '::' + alatVolStr[index].trim() + '::' + alatJmlStr[index].trim() + '::' + alatNumStr[index].trim();
                });

                $('#thp-input').val(thpStr);
                $('#perusahaan-input').val(kantorStr);
                $('#peralatan-input').val(sewaStr);

                return (thpStr.trim() !== '' && kantorStr.trim() !== '' && sewaStr.trim() !== '');
            }
        </script>
    </body>
</html>