<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Penagihan</li><li>Tabel</li><li>Gaji</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Gaji
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-10"></div>
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="presensi-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Gaji</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>Jabatan</th>
                                                    <th>Nominal</th>
                                                    <th>Status</th>
                                                    <th>Tanggal Proses</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="gaji-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Proses Gaji</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal" id="model-form">
                                                <!-- Model Mandatories -->
                                                <input type="hidden" name="model-input" value="gaji">
                                                <input type="hidden" name="action-input" id="action-input" value="">
                                                <input type="hidden" name="key-input" id="key-input" value="">
                                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                                <!-- Model Data -->
                                                <input type="hidden" name="terpakai-input" value="1">
                                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                                <input type="hidden" id="presensi-input" name="presensi-input" value="">
                                                <input type="hidden" id="biodata-input" name="biodata-input" value="">
                                                <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                <input type="hidden" id="status-input" name="status-input" value="">
                                                <fieldset>
                                                    <legend>PEGAWAI</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">ID Pegawai</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control" placeholder="ID Pegawai" type="text" id="id-text" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nama</label>
                                                        <div class="col-md-10">
                                                            <input class="form-control" placeholder="Nama Lengkap Pegawai" type="text" id="nama-text" readonly />
                                                        </div>
                                                    </div>
                                                    <legend>PENGHASILAN</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Gaji</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Gaji Pokok (Rp)" type="text" id="gaji-input" name="gaji-input" />
                                                        </div>
                                                        <label class="col-md-2 control-label">Tunjangan Tetap</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Tunjangan Tetap (Rp)" type="text" id="tunjangan_tetap-input" name="tunjangan_tetap-input" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Tunjangan Tidak Tetap</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Tunjangan Tidak Tetap (Rp)" type="text" id="tunjangan_rancu-input" name="tunjangan_rancu-input" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Rapel</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Rapel (Rp)" type="text" id="rapel-input" name="rapel-input" />
                                                        </div>
                                                        <label class="col-md-2 control-label">THR</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="THR (Rp)" type="text" id="thr-input" name="thr-input" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Lembur Paket</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Lembur Paket (Rp)" type="text" id="paket-input" name="paket-input" />
                                                        </div>
                                                        <label class="col-md-2 control-label">Insentif</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Insentif (Rp)" type="text" id="insentif-input" name="insentif-input" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Lembur</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Lembur (Rp)" type="text" id="lembur-input" name="lembur-input" />
                                                        </div>
                                                        <label class="col-md-2 control-label">Klaim</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Klaim (Rp)" type="text" id="klaim-input" name="klaim-input" />
                                                        </div>
                                                    </div>
                                                    <legend>PENGURANGAN</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Pot. Kehadiran</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Potongan Kehadiran (Rp)" type="text" id="pot_hadir-input" name="pot_hadir-input">
                                                        </div>
                                                        <label class="col-md-2 control-label">Pot. Lainnya</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Potongan Lain-lain (Rp)" type="text" id="pot_lain_isi-input" name="pot_lain_isi-input">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Catatan</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Perihal Potongan Lainnya" rows="2" id="pot_lain_info-input" name="pot_lain_info-input"></textarea>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary simpan-button" type="button" data-status="proses">Proses</button>
                                            <button class="btn btn-success simpan-button" type="button" data-status="bayar">Bayar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/gaji/0___0'); ?>", '#dt_basic', [
                    {"data": "id"},
                    {"data": "biodata"},
                    {"data": "jabatan"},
                    {"data": "nominal"},
                    {"data": "status"},
                    {"data": "tanggal"},
                    {"data": "aksi", 'width': '15%'}
                ]);

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#presensi-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Cut/Off Rekap'});
                    }
                });
                $('#proyek-filter').change(function () {
                    var kode = $(this).val();
                    $('#presensi-filter').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Cut/Off Rekap'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/rekap'); ?>/" + kode + '___presensi',
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null || $('#presensi-filter').val() !== null) {
                        var tabelnya = $('#dt_basic').DataTable();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/gaji'); ?>/" + $('#proyek-filter').val() + '___' + $('#presensi-filter').val()).load();
                        tabelnya.columns.adjust().draw();
                    }
                });

                $('.simpan-button').click(function () {
                    $('#status-input').val($(this).data('status'));
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Proses!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        nominalFormats();
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/ig/kelolaGaji'); ?>", $('#model-form'));
                    });
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    $('#id-text').val($(this).data('nik'));
                    $('#nama-text').val($(this).data('nama'));
                    fillInputs(idData);

                    if (idData === '0') {
                        $('#biodata-input').val($(this).data('who'));
                        $('#proyek-input').val($('#proyek-filter').val());
                        $('#presensi-input').val($('#presensi-filter').val());
                        $('#gaji-input').val($(this).data('gajian'));
                        $('#tunjangan_tetap-input').val($(this).data('tetapan'));
                        $('#tunjangan_rancu-input').val($(this).data('rancuan'));
                        $('#thr-input').val('');
                        $('#rapel-input').val('');
                        $('#paket-input').val('');
                        $('#insentif-input').val('');
                        $('#lembur-input').val('');
                        $('#klaim-input').val('');
                        $('#pot_lain_isi-input').val('');
                        $('#pot_hadir-input').val('');
                        $('#pot_lain_info-input').val('');
                    }

                    return false;
                });
                $(table + " .printBtn").on("click", function () {
                    var idData = $(this).attr('href');

                    if (idData === '0') {
                        swal({
                            title: 'Peringatan',
                            html: 'Proses terlebih dahulu!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    } else {
                    }

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=gaji&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#status-input').val(response['data'].status);
                        $('#waktu-input').val(response['data'].waktu);

                        if (response['data'].key > 0) {
                            $('#proyek-input').val(response['data'].proyek);
                            $('#presensi-input').val(response['data'].presensi);
                            $('#biodata-input').val(response['data'].biodata);
                            $('#gaji-input').val(response['data'].gaji);
                            $('#thr-input').val(response['data'].thr);
                            $('#rapel-input').val(response['data'].rapel);
                            $('#tunjangan_tetap-input').val(response['data'].tunjangan_tetap);
                            $('#tunjangan_rancu-input').val(response['data'].tunjangan_rancu);
                            $('#paket-input').val(response['data'].paket);
                            $('#insentif-input').val(response['data'].insentif);
                            $('#lembur-input').val(response['data'].lembur);
                            $('#klaim-input').val(response['data'].klaim);
                            $('#pot_lain_isi-input').val(response['data'].pot_lain_isi);
                            $('#pot_hadir-input').val(response['data'].pot_hadir);
                            $('#pot_lain_info-input').val(response['data'].pot_lain_info);
                        }

                        $('#gaji-modal').modal();
                    }
                });
            }
        </script>
    </body>
</html>