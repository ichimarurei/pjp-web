<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Master</li><li>Pendataan</li><li>Uraian</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Uraian
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Uraian</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="uraian">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="jabatan-input" name="jabatan-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tunjangan Jabataan</label>
                                                    <div class="col-md-3">
                                                        <select style="width:100%" class="select2 form-control" id="tunjangan-filter">
                                                            <option value="x">Tidak</option>
                                                            <option value="y">Ya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4" id="jabatan-div">
                                                        <select style="width:100%" class="select2 form-control" id="jabatan-filter"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Uraian</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Nama Uraian" type="text" id="uraian-input" name="uraian-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Penjelasan</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Penjelasan" type="text" id="penjelasan-input" name="penjelasan-input">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/master/tabelUraian'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#jabatan-div').hide();
                $('#jabatan-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Jabatan'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/jabatan'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#tunjangan-filter').change(function () {
                    if ($(this).val() === 'x') {
                        $('#jabatan-div').hide();
                        $('#uraian-input').val('');
                        $('#uraian-input').attr('readonly', false);
                    } else {
                        $('#jabatan-div').show();
                        $('#uraian-input').val('Tunjangan Jabatan');
                        $('#uraian-input').attr('readonly', true);
                    }
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    var strJabatan = '-';

                    if ($('#tunjangan-filter').val() === 'y') {
                        strJabatan = $('#jabatan-filter').val();
                    }

                    $('#jabatan-input').val(strJabatan);
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/master/tabelUraian'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=uraian&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#uraian-input').val(response['data'].uraian);
                        $('#penjelasan-input').val(response['data'].penjelasan);
                        $('#jabatan-input').val(response['data'].jabatan);
                        $('#uraian-input').focus();

                        if (response['data'].key > 0) {
                            if (response['data'].jabatan !== '-') {
                                $('#tunjangan-filter').val('y').trigger('change');
                                var pilihJabatan = $('#jabatan-filter');
                                $.ajax({
                                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=jabatan&kode=' + response['data'].jabatan,
                                    dataType: 'json', type: 'POST', cache: false
                                }).then(function (data) {
                                    // create the option and append to Select2
                                    pilihJabatan.append(new Option(data['data'].jabatan, data['data'].kode, true, true)).trigger('change');
                                    // manually trigger the `select2:select` event
                                    pilihJabatan.trigger({
                                        type: 'select2:select',
                                        params: {
                                            data: data['data']
                                        }
                                    });
                                });
                            }
                        }
                    }
                });
            }
        </script>
    </body>
</html>