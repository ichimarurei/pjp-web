<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Proyek</li><li>Tabel</li><li>Info Proyek</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Info Proyek
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Proyek</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Proyek</th>
                                                    <th>Jenis</th>
                                                    <th>Alamat</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="proyek-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Data Proyek</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <legend>Info Proyek</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Proyek</label>
                                                        <div class="col-md-10">
                                                            <p><b id="proyek-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jenis</label>
                                                        <div class="col-md-4">
                                                            <p><b id="tipe-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Alamat</label>
                                                        <div class="col-md-10">
                                                            <p><b id="alamat-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jumlah Personil</label>
                                                        <div class="col-md-10">
                                                            <p><b id="personil-text"> ..... </b></p>
                                                            <p class="note"><strong>Note:</strong> jumlah yang ditampilkan merupakan hasil isian manual!</p>
                                                        </div>
                                                    </div>
                                                    <legend>Upah Tenaga Kerja</legend>
                                                    <div class="form-group" id="upah-div">
                                                    </div>
                                                    <legend>Biaya Tidak Langsung</legend>
                                                    <p class="alert alert-info no-margin"><small><code><i><b>THR</b> secara otomatis akan ditambahkan pada uraian di <b>Invoice</b></i></code></small></p>
                                                    <br>
                                                    <div class="form-group" id="biaya-div">
                                                    </div>
                                                    <legend>Biaya Lainnya</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Management Fee</label>
                                                        <div class="col-md-2">
                                                            <p><b id="fee-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPN</label>
                                                        <div class="col-md-2">
                                                            <p><b id="ppn-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPH 23</label>
                                                        <div class="col-md-2">
                                                            <p><b id="pph-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="lanjut-button">Ubah</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="proyek">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="alamat-input" name="alamat-input" value="">
                                <input type="hidden" id="tipe-input" name="tipe-input" value="">
                                <input type="hidden" id="upah-input" name="upah-input" value="">
                                <input type="hidden" id="biaya-input" name="biaya-input" value="">
                                <input type="hidden" id="pph-input" name="pph-input" value="">
                                <input type="hidden" id="ppn-input" name="ppn-input" value="">
                                <input type="hidden" id="fee-input" name="fee-input" value="">
                                <input type="hidden" id="personil-input" name="personil-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var nextURL = null;

            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/proyek'); ?>", '#dt_basic', [
                    {"data": "proyek"},
                    {"data": "tipe"},
                    {"data": "alamat"},
                    {"data": "aksi", 'width': '17%'}
                ]);

                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
                $('#lanjut-button').click(function () {
                    $(location).attr('href', nextURL);
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    nextURL = "<?php echo site_url('modul/tampil/proyek/dataProyek'); ?>/" + $(this).attr('href');
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    fillInputs($(this).attr('href'), false);

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData, true);
                    });

                    return false;
                });
            }

            function fillInputs(param, isRemove) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        if (isRemove) {
                            $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                            $('#key-input').val(response['data'].key);
                            $('#kode-input').val(response['data'].kode);
                            $('#proyek-input').val(response['data'].proyek);
                            $('#alamat-input').val(response['data'].alamat);
                            $('#tipe-input').val(response['data'].tipe);
                            $('#upah-input').val(response['data'].upah);
                            $('#biaya-input').val(response['data'].biaya);
                            $('#pph-input').val(response['data'].pph);
                            $('#ppn-input').val(response['data'].ppn);
                            $('#fee-input').val(response['data'].fee);
                            $('#personil-input').val(response['data'].personil);
                            doSave('Dihapus', "<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>", $('#model-form'));
                        } else {
                            $('#proyek-text').text(response['data'].proyek);
                            $('#tipe-text').text(response['data'].tipeText);
                            $('#alamat-text').text(response['data'].alamat);
                            $('#pph-text').text(response['data'].pph + ' %');
                            $('#ppn-text').text(response['data'].ppn + ' %');
                            $('#fee-text').text(response['data'].fee + ' %');
                            $('#personil-text').text(response['data'].personil);
                            var upahHtml = '';
                            var biayaHtml = '';

                            if (response['data'].upah !== '') {
                                var upahVals = response['data'].upah.split(':');

                                $.each(upahVals, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split('|');
                                    upahHtml += '<label class="col-md-4">' + info[1] + '</label>';
                                    upahHtml += '<div class="col-md-2">';
                                    upahHtml += '<p><b>' + values[1] + '</b></p>';
                                    upahHtml += '</div>';
                                });
                            }

                            if (response['data'].biaya !== '') {
                                var biayaVals = response['data'].biaya.split(':');

                                $.each(biayaVals, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split('|');
                                    biayaHtml += '<label class="col-md-4">' + info[1] + '</label>';
                                    biayaHtml += '<div class="col-md-2">';
                                    biayaHtml += '<p><b>' + values[1] + '</b></p>';
                                    biayaHtml += '</div>';
                                });
                            }

                            $('#upah-div').html(upahHtml);
                            $('#biaya-div').html(biayaHtml);
                            $.unblockUI();
                            $('#proyek-modal').modal();
                        }
                    }
                });
            }
        </script>
    </body>
</html>