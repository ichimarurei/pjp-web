<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Proyek</li><li>Pendataan</li><li>Info Proyek</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Info Proyek
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Proyek</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <ul class="nav nav-tabs bordered">
                                            <li class="active"><a href="#fieldset-info" data-toggle="tab">Proyek</a></li>
                                            <li><a href="#fieldset-utk" data-toggle="tab">Upah Tenaga Kerja</a></li>
                                            <li><a href="#fieldset-btl" data-toggle="tab">Biaya Tidak Langsung</a></li>
                                            <li><a href="#fieldset-lain" data-toggle="tab">Lainnya</a></li>
                                        </ul>
                                        <div class="tab-content padding-10">
                                            <div class="tab-pane fade in active" id="fieldset-info">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="proyek">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="upah-input" name="upah-input" value="">
                                                    <input type="hidden" id="biaya-input" name="biaya-input" value="">
                                                    <input type="hidden" id="pph-input" name="pph-input" class="nominal-pph" value="">
                                                    <input type="hidden" id="ppn-input" name="ppn-input" class="nominal-ppn" value="">
                                                    <input type="hidden" id="fee-input" name="fee-input" class="nominal-fee" value="">
                                                    <fieldset>
                                                        <legend>Info Proyek</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="Nama Proyek" type="text" id="proyek-input" name="proyek-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Jenis</label>
                                                            <div class="col-md-5">
                                                                <select style="width:100%" class="select2 form-control" id="tipe-input" name="tipe-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Alamat</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Alamat Proyek" rows="4" id="alamat-input" name="alamat-input"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-utk">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Uraian</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Personil</label>
                                                            <div class="col-md-3">
                                                                <input class="form-control" placeholder="Jumlah Pegawai" type="text" id="personil-input" name="personil-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uraian</label>
                                                            <div class="col-md-8">
                                                                <select style="width:100%" class="select2 form-control uraian-filter" id="uraian1-filter"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah1-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <br>
                                                <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Uraian</th>
                                                            <th>Nominal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-btl">
                                                <p class="alert alert-info no-margin"><small><code><i><b>THR</b> secara otomatis akan ditambahkan pada uraian di <b>Invoice</b></i></code></small></p>
                                                <br>
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Uraian</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uraian</label>
                                                            <div class="col-md-8">
                                                                <select style="width:100%" class="select2 form-control uraian-filter" id="uraian2-filter"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah2-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <br>
                                                <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Uraian</th>
                                                            <th>Nominal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-lain">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Biaya Lainnya</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Management Fee</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control nominal-fee" placeholder="Management Fee (%)" type="text" id="fee-filter">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">PPN</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control nominal-ppn" placeholder="PPN (%)" type="text" id="ppn-filter">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                            <label class="col-md-2 control-label">PPH 23</label>
                                                            <div class="col-md-2">
                                                                <div class="input-group">
                                                                    <input class="form-control nominal-pph" placeholder="PPH 23 (%)" type="text" id="pph-filter">
                                                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <form class="form-horizontal">
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn btn-default" href="<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>">Batal</a>
                                                            <button class="btn btn-primary" type="button" id="lihat-button">
                                                                <i class="fa fa-save"></i>
                                                                Simpan
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="proyek-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Data Proyek</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <legend>Info Proyek</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Proyek</label>
                                                        <div class="col-md-10">
                                                            <p><b id="proyek-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jenis</label>
                                                        <div class="col-md-4">
                                                            <p><b id="tipe-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Alamat</label>
                                                        <div class="col-md-10">
                                                            <p><b id="alamat-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Jumlah Personil</label>
                                                        <div class="col-md-10">
                                                            <p><b id="personil-text"> ..... </b></p>
                                                            <p class="note"><strong>Note:</strong> jumlah yang ditampilkan merupakan hasil isian manual!</p>
                                                        </div>
                                                    </div>
                                                    <legend>Upah Tenaga Kerja</legend>
                                                    <div class="form-group" id="upah-div">
                                                    </div>
                                                    <legend>Biaya Tidak Langsung</legend>
                                                    <p class="alert alert-info no-margin"><small><code><i><b>THR</b> secara otomatis akan ditambahkan pada uraian di <b>Invoice</b></i></code></small></p>
                                                    <br>
                                                    <div class="form-group" id="biaya-div">
                                                    </div>
                                                    <legend>Biaya Lainnya</legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Management Fee</label>
                                                        <div class="col-md-2">
                                                            <p><b id="fee-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPN</label>
                                                        <div class="col-md-2">
                                                            <p><b id="ppn-text"> ..... </b></p>
                                                        </div>
                                                        <label class="col-md-2">PPH 23</label>
                                                        <div class="col-md-2">
                                                            <p><b id="pph-text"> ..... </b></p>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="simpan-button">Lanjutkan Proses</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- Modal -->
                            <div class="modal fade" id="uraian-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Nominal Uraian Proyek</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <input type="hidden" id="kode-filter" value="">
                                                    <input type="hidden" id="urai-filter" value="">
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nominal</label>
                                                        <div class="col-md-4">
                                                            <select style="width:100%" class="select2 form-control" id="jenis-filter">
                                                                <option value="uang">Uang (Rupiah)</option>
                                                                <option value="persen">Persen (%)</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 nominal-div" id="uang-div">
                                                            <input class="form-control nominal-text" placeholder="Nominal (Rp)" type="text" id="uang-filter">
                                                        </div>
                                                        <div class="col-md-3 nominal-div" id="persen-div">
                                                            <div class="input-group">
                                                                <input class="form-control" placeholder="Nominal (%)" type="text" id="persen-filter">
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="isi-button">Tambahkan</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                basicTable('#dt_basic1', [{"data": "kode", 'visible': false}, {"data": "uraian"}, {"data": "nominal"}]);
                basicTable('#dt_basic2', [{"data": "kode", 'visible': false}, {"data": "uraian"}, {"data": "nominal"}]);
                var tabel1 = $('#dt_basic1').DataTable();
                var tabel2 = $('#dt_basic2').DataTable();
                var activeUrai = 1;
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>', tabel1, tabel2);

                $('#tipe-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Jenis Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/tipe'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('.uraian-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Uraian'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/uraian'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });
                $("#jenis-filter").change(function () {
                    $('.nominal-div').hide();
                    $('#uang-filter').val(formatRp('0'));
                    $('#persen-filter').val('0');
                    $('#' + $(this).val() + '-div').show();
                });
                $('#dt_basic1').on('click', 'tbody tr', function () {
                    var dataRow = tabel1.row(this);
                    swal({
                        title: 'Lanjutkan Menghapus "' + dataRow.data().uraian + '"?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        dataRow.remove().draw();
                    });
                });
                $('#dt_basic2').on('click', 'tbody tr', function () {
                    var dataRow = tabel2.row(this);
                    swal({
                        title: 'Lanjutkan Menghapus "' + dataRow.data().uraian + '"?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        dataRow.remove().draw();
                    });
                });

                $('#tambah1-button').click(function () {
                    var uraiOpt = $('#uraian1-filter').select2('data');
                    activeUrai = 1;

                    if ((typeof uraiOpt[0] !== 'undefined')) {
                        $('#kode-filter').val(uraiOpt[0].id);
                        $('#urai-filter').val(uraiOpt[0].text);
                        $('#jenis-filter').val('uang').trigger('change');
                        $('#uang-div').show();
                        $('#uraian-modal').modal();
                    } else {
                        swal({
                            title: 'Peringatan',
                            html: 'Pilih Uraian terlebih dahulu!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
                $('#tambah2-button').click(function () {
                    var uraiOpt = $('#uraian2-filter').select2('data');
                    activeUrai = 2;

                    if ((typeof uraiOpt[0] !== 'undefined')) {
                        $('#kode-filter').val(uraiOpt[0].id);
                        $('#urai-filter').val(uraiOpt[0].text);
                        $('#jenis-filter').val('uang').trigger('change');
                        $('#uang-div').show();
                        $('#uraian-modal').modal();
                    } else {
                        swal({
                            title: 'Peringatan',
                            html: 'Pilih Uraian terlebih dahulu!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
                $('#isi-button').click(function () {
                    var isExist = false;

                    tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (!isExist) {
                            isExist = (this.data().kode === $('#kode-filter').val());
                        }
                    });
                    tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (!isExist) {
                            isExist = (this.data().kode === $('#kode-filter').val());
                        }
                    });

                    if (!isExist) {
                        var activeRow = (activeUrai === 1) ? tabel1.row : tabel2.row;
                        var nominal = ($('#jenis-filter').val() === 'persen') ? $('#persen-filter').val() + ' %' : $('#uang-filter').val();
                        activeRow.add({
                            'kode': $('#kode-filter').val(), 'uraian': $('#urai-filter').val(), 'nominal': nominal
                        }).draw();
                    }

                    $('#uraian-modal').modal('hide');
                });
                $('#lihat-button').click(function () {
                    var tipeOpt = $('#tipe-input').select2('data');
                    var upah = '';
                    var biaya = '';
                    var upahHtml = '';
                    var biayaHtml = '';

                    if ((typeof tipeOpt[0] !== 'undefined')) {
                        $('#tipe-text').text(tipeOpt[0].text);
                    }

                    tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (upah !== '') {
                            upah += ':';
                        }

                        upah += this.data().kode + '|' + this.data().uraian + '=' + this.data().nominal;
                        upahHtml += '<label class="col-md-4">' + this.data().uraian + '</label>';
                        upahHtml += '<div class="col-md-2">';
                        upahHtml += '<p><b>' + this.data().nominal + '</b></p>';
                        upahHtml += '</div>';
                    });
                    tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (biaya !== '') {
                            biaya += ':';
                        }

                        biaya += this.data().kode + '|' + this.data().uraian + '=' + this.data().nominal;
                        biayaHtml += '<label class="col-md-4">' + this.data().uraian + '</label>';
                        biayaHtml += '<div class="col-md-2">';
                        biayaHtml += '<p><b>' + this.data().nominal + '</b></p>';
                        biayaHtml += '</div>';
                    });

                    $('#upah-input').val(upah);
                    $('#biaya-input').val(biaya);
                    $('#pph-input').val($('#pph-filter').val());
                    $('#ppn-input').val($('#ppn-filter').val());
                    $('#fee-input').val($('#fee-filter').val());
                    $('#proyek-text').text($('#proyek-input').val());
                    $('#alamat-text').text($('#alamat-input').val());
                    $('#personil-text').text($('#personil-input').val());
                    $('#pph-text').text($('#pph-input').val() + ' %');
                    $('#ppn-text').text($('#ppn-input').val() + ' %');
                    $('#fee-text').text($('#fee-input').val() + ' %');
                    $('#upah-div').html(upahHtml);
                    $('#biaya-div').html(biayaHtml);
                    $('#proyek-modal').modal();
                });
                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param, tabel1, tabel2) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#upah-input').val(response['data'].upah);
                        $('#biaya-input').val(response['data'].biaya);
                        $('#personil-input').val(response['data'].personil);
                        $('.nominal-pph').val(response['data'].pph);
                        $('.nominal-ppn').val(response['data'].ppn);
                        $('.nominal-fee').val(response['data'].fee);
                        $('#proyek-input').focus();

                        if (response['data'].key > 0) {
                            var pilihTipe = $('#tipe-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=tipe&kode=' + response['data'].tipe,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihTipe.append(new Option(data['data'].tipe, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihTipe.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            if (response['data'].upah !== '') {
                                var upahVals = response['data'].upah.split(':');

                                $.each(upahVals, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split('|');
                                    tabel1.row.add({
                                        'kode': info[0], 'uraian': info[1], 'nominal': values[1]
                                    }).draw();
                                });
                            }

                            if (response['data'].biaya !== '') {
                                var biayaVals = response['data'].biaya.split(':');

                                $.each(biayaVals, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split('|');
                                    tabel2.row.add({
                                        'kode': info[0], 'uraian': info[1], 'nominal': values[1]
                                    }).draw();
                                });
                            }
                        }
                    }
                });
            }

            function aksi(table) {}
        </script>
    </body>
</html>