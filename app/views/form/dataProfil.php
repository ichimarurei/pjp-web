<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Pendataan</li><li>Pegawai</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Pegawai
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Pegawai</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="bio">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" id="rekening-input" name="rekening-input" value="">
                                            <input type="hidden" id="telepon-input" name="telepon-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ID Pegawai</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="ID Pegawai" type="text" id="id-input" name="id-input" readonly>
                                                    </div>
                                                    <label class="col-md-2 control-label">KTP</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No KTP" type="text" id="ktp-input" name="ktp-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">NPWP</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No NPWP" type="text" id="npwp-input" name="npwp-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">BPJS</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="No BPJS" type="text" id="bpjs-input" name="bpjs-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Nama</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Nama Lengkap Pegawai" type="text" id="nama-input" name="nama-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Kelamin</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="kelamin-input" name="kelamin-input">
                                                            <option value="cowok">Laki-Laki</option>
                                                            <option value="cewek">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-md-2 control-label">Agama</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="agama-input" name="agama-input">
                                                            <option value="Islam">Islam</option>
                                                            <option value="Katolik">Katholik</option>
                                                            <option value="Protestan">Protestan</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Budha">Budha</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tempat Lahir</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tempat Lahir" type="text" id="tempat_lahir-input" name="tempat_lahir-input">
                                                    </div>
                                                    <label class="col-md-2 control-label">Tanggal Lahir</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tanggal Lahir" type="text" id="tanggal_lahir-input" name="tanggal_lahir-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Telepon</label>
                                                    <div class="col-md-5">
                                                        <input class="form-control" placeholder="No Telepon Utama" type="text" id="telepon-utama">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input class="form-control" placeholder="No Telepon Alternatif" type="text" id="telepon-alternatif">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Email</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" placeholder="Email Pegawai" type="email" id="email-input" name="email-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Status Pernikahan</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                            <option value="belum">Belum Menikah</option>
                                                            <option value="nikah">Menikah</option>
                                                            <option value="cerai">Bercerai</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-md-2 control-label">Jumlah Anak</label>
                                                    <div class="col-md-2">
                                                        <input class="form-control" placeholder="Jumlah Anak" type="text" id="anak-input" name="anak-input">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Rekening</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="bank-rekening"></select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control" placeholder="No Rekening" type="text" id="no-rekening">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pendidikan</label>
                                                    <div class="col-md-4">
                                                        <select style="width:100%" class="select2 form-control" id="pendidikan-input" name="pendidikan-input">
                                                            <option>TK</option>
                                                            <option>SD</option>
                                                            <option>SMP</option>
                                                            <option>SMA</option>
                                                            <option>S1</option>
                                                            <option>S2</option>
                                                            <option>S3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Alamat</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control text-tetap" placeholder="Alamat Lengkap Pegawai" rows="4" id="alamat-input" name="alamat-input"></textarea>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="row" id="galeri-div">
                                                <hr>
                                                <div class="superbox col-sm-12" id="galeri-akun">
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelProfil/aktif'); ?>">Kembali</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#bank-rekening').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Bank'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bank'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#id-input').val(response['data'].id);
                        $('#ktp-input').val(response['data'].ktp);
                        $('#npwp-input').val(response['data'].npwp);
                        $('#bpjs-input').val(response['data'].bpjs);
                        $('#nama-input').val(response['data'].nama);
                        $('#kelamin-input').val(response['data'].kelamin).trigger('change');
                        $('#tempat_lahir-input').val(response['data'].tempat_lahir);
                        $('#tanggal_lahir-input').val(response['data'].tanggal_lahir);
                        $('#agama-input').val(response['data'].agama).trigger('change');
                        $('#pendidikan-input').val(response['data'].pendidikan).trigger('change');
                        $('#rekening-input').val(response['data'].rekening);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#telepon-input').val(response['data'].telepon);
                        $('#email-input').val(response['data'].email);
                        $('#nikah-input').val(response['data'].nikah).trigger('change');
                        $('#anak-input').val(response['data'].anak);
                        $('#galeri-div').hide();

                        if (response['data'].key > 0) {
                            var rekeningBank = response['data'].rekening.split('_');
                            var noTelp = response['data'].telepon.split('_');
                            var pilihBank = $('#bank-rekening');
                            var pics = ['foto.jpg', 'ktp.jpg', 'kk.jpg', 'sim.jpg', 'ijazah.jpg', 'npwp.jpg'];
                            $('#no-rekening').val(rekeningBank[1]);
                            $('#telepon-utama').val(noTelp[0]);

                            if (noTelp[1] !== '-') {
                                $('#telepon-alternatif').val(noTelp[1]);
                            }

                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=bank&kode=' + rekeningBank[0],
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihBank.append(new Option(data['data'].bank, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihBank.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.when(lihatGaleri('etc/akun/' + response['data'].kode)).done(function (galleries) {
                                var images = '';

                                $.each(galleries.galeri, function (index, value) {
                                    if (pics.includes(value)) {
                                        images += '<div class="superbox-list">';
                                        images += '<a href="<?php echo base_url('etc/akun'); ?>/' + response['data'].kode + '/' + value + '" data-toggle="lightbox" data-gallery="galeri-' + response['data'].kode + '">';
                                        images += '<img src="<?php echo base_url('etc/akun'); ?>/' + response['data'].kode + '/' + value + '" class="superbox-img">';
                                        images += '</a>';
                                        images += '</div>';
                                    }
                                });

                                $('#galeri-akun').html(images);

                                if (images !== '') {
                                    $('#galeri-div').show();
                                }
                            });
                        }
                    }
                });
            }
        </script>
    </body>
</html>