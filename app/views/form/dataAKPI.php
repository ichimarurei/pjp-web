<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Operasional</li><li>Pendataan</li><li>KPI (Penilaian) Anggota</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                KPI (Penilaian) Anggota
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>KPI (Penilaian) Anggota</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="kpi">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" name="level-input" value="pegawai">
                                            <input type="hidden" name="pembuat-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                            <input type="hidden" id="kpi-input" name="kpi-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pegawai</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="pegawai-input" name="pegawai-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Diketahui Oleh</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="pengawas-input" name="pengawas-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Disahkan Oleh</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="penerima-input" name="penerima-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tanggal</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tanggal Pendataan" type="text" id="tanggal-input" name="tanggal-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="no-padding">
                                                    <ul class="nav nav-tabs bordered">
                                                        <li class="active"><a href="#fieldset-seragam" data-toggle="tab">Seragam</a></li>
                                                        <li><a href="#fieldset-penampilan" data-toggle="tab">Penampilan</a></li>
                                                        <li><a href="#fieldset-sikap" data-toggle="tab">Sikap</a></li>
                                                        <li><a href="#fieldset-kerajinan" data-toggle="tab">Kerajinan</a></li>
                                                    </ul>
                                                    <div class="tab-content padding-10">
                                                        <div class="tab-pane fade in active" id="fieldset-seragam">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Topi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1a-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Baju Disetrika</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1b-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Papan Nama</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1c-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">ID Card</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1d-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Kewenangan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1e-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Pin / Badge Lotus</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1f-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Kopel Dibraso / Ikat Pinggang</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1g-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Sepatu Disemir</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1h-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Kaos Dalam Hitam / Putih</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1i-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-penampilan">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Rambut (1-2)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2a-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Mulut Wangi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2b-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Kumis Rapi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2c-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Jenggot Tidak Ada</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2d-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Kuku Tidak Panjang</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2e-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Perlengkapan &amp; Atribut Bersih</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2f-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Seragam Wangi &amp; Bersih</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2g-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Aksesoris Tidak Berlebihan, Sesuai Peraturan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2h-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-sikap">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Senyum, Tidak Cengengesan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3a-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Sapa, Disesuaikan Dengan Situasi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3b-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Salam</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3c-input">
                                                                        <span class="input-group-addon"> <strong>/ 2</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Tegas</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3d-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Patuh &amp; Disiplin Dalam Peraturan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3e-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Patuh Terhadap Pimpinan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3f-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Banyak Bicara Yang Tidak Perlu</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3g-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Hormat Sesuai Kondisi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3h-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-kerajinan">
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Kelengkapan Absensi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4a-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Ketepatan Waktu Kerja</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4b-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Pelaksanaan Kerja Sesuai Prosedur</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4c-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Tidak Meninggalkan Plottingan Tanpa Koordinasi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4d-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Tidak Pernah Terima Surat Peringatan (SP)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4e-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-2 control-label">Melaksanakan Pemantauan Area (Patroli)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4f-input">
                                                                        <span class="input-group-addon"> <strong>/ 3</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Total Bobot</label>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input class="form-control" placeholder="....." type="text" id="poin-input" name="poin-input" readonly>
                                                            <span class="input-group-addon"> <strong>/ 100</strong> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <hr>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelAKPI'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#pegawai-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#pengawas-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Diketahui Oleh'});
                    }
                });
                $('#penerima-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Disahkan Oleh'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    reInit($('#pegawai-input'), 'Pilih Pegawai', kode);
                    reInit($('#pengawas-input'), 'Pilih Diketahui Oleh', kode);
                    reInit($('#penerima-input'), 'Pilih Disahkan Oleh', kode);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});

                    if (joinPoin()) {
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/form/tabelAKPI'); ?>", $('#model-form'));
                    } else {
                        $.unblockUI();
                        swal({
                            title: 'Peringatan',
                            html: 'Total Bobot KPI tidak boleh melebihi 100!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kpi&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#tanggal-input').val(response['data'].tanggal);
                        $('#kpi-input').val(response['data'].kpi);
                        $('#poin-input').val(response['data'].poin);

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            editPilih($('#pegawai-input'), response['data'].pegawai);

                            if (response['data'].pengawas !== '-') {
                                editPilih($('#pengawas-input'), response['data'].pengawas);
                            }

                            if (response['data'].penerima !== '-') {
                                editPilih($('#penerima-input'), response['data'].penerima);
                            }

                            if (response['data'].kpi !== '') {
                                var kpiItems = response['data'].kpi.split(',');

                                $.each(kpiItems, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split(':');
                                    $('#' + info[0] + '-input').val(values[1]);
                                });
                            }
                        }
                    }
                });
            }

            function joinPoin() {
                var poin = 0;
                var strKPI = '';

                // Seragam
                poin += validatePoin($('#p1a-input'), 3);
                poin += validatePoin($('#p1b-input'), 3);
                poin += validatePoin($('#p1c-input'), 3);
                poin += validatePoin($('#p1d-input'), 4);
                poin += validatePoin($('#p1e-input'), 3);
                poin += validatePoin($('#p1f-input'), 3);
                poin += validatePoin($('#p1g-input'), 3);
                poin += validatePoin($('#p1h-input'), 3);
                poin += validatePoin($('#p1i-input'), 3);
                strKPI += 'p1a:3=' + validatePoin($('#p1a-input'), 3);
                strKPI += ',p1b:3=' + validatePoin($('#p1b-input'), 3);
                strKPI += ',p1c:3=' + validatePoin($('#p1c-input'), 3);
                strKPI += ',p1d:4=' + validatePoin($('#p1d-input'), 4);
                strKPI += ',p1e:3=' + validatePoin($('#p1e-input'), 3);
                strKPI += ',p1f:3=' + validatePoin($('#p1f-input'), 3);
                strKPI += ',p1g:3=' + validatePoin($('#p1g-input'), 3);
                strKPI += ',p1h:3=' + validatePoin($('#p1h-input'), 3);
                strKPI += ',p1i:3=' + validatePoin($('#p1i-input'), 3);
                // Penampilan
                poin += validatePoin($('#p2a-input'), 3);
                poin += validatePoin($('#p2b-input'), 3);
                poin += validatePoin($('#p2c-input'), 3);
                poin += validatePoin($('#p2d-input'), 3);
                poin += validatePoin($('#p2e-input'), 3);
                poin += validatePoin($('#p2f-input'), 3);
                poin += validatePoin($('#p2g-input'), 3);
                poin += validatePoin($('#p2h-input'), 3);
                strKPI += ',p2a:3=' + validatePoin($('#p2a-input'), 3);
                strKPI += ',p2b:3=' + validatePoin($('#p2b-input'), 3);
                strKPI += ',p2c:3=' + validatePoin($('#p2c-input'), 3);
                strKPI += ',p2d:3=' + validatePoin($('#p2d-input'), 3);
                strKPI += ',p2e:3=' + validatePoin($('#p2e-input'), 3);
                strKPI += ',p2f:3=' + validatePoin($('#p2f-input'), 3);
                strKPI += ',p2g:3=' + validatePoin($('#p2g-input'), 3);
                strKPI += ',p2h:3=' + validatePoin($('#p2h-input'), 3);
                // Sikap
                poin += validatePoin($('#p3a-input'), 3);
                poin += validatePoin($('#p3b-input'), 3);
                poin += validatePoin($('#p3c-input'), 2);
                poin += validatePoin($('#p3d-input'), 3);
                poin += validatePoin($('#p3e-input'), 4);
                poin += validatePoin($('#p3f-input'), 4);
                poin += validatePoin($('#p3g-input'), 3);
                poin += validatePoin($('#p3h-input'), 3);
                strKPI += ',p3a:3=' + validatePoin($('#p3a-input'), 3);
                strKPI += ',p3b:3=' + validatePoin($('#p3b-input'), 3);
                strKPI += ',p3c:2=' + validatePoin($('#p3c-input'), 2);
                strKPI += ',p3d:3=' + validatePoin($('#p3d-input'), 3);
                strKPI += ',p3e:4=' + validatePoin($('#p3e-input'), 4);
                strKPI += ',p3f:4=' + validatePoin($('#p3f-input'), 4);
                strKPI += ',p3g:3=' + validatePoin($('#p3g-input'), 3);
                strKPI += ',p3h:3=' + validatePoin($('#p3h-input'), 3);
                // Kerajinan
                poin += validatePoin($('#p4a-input'), 5);
                poin += validatePoin($('#p4b-input'), 4);
                poin += validatePoin($('#p4c-input'), 3);
                poin += validatePoin($('#p4d-input'), 4);
                poin += validatePoin($('#p4e-input'), 4);
                poin += validatePoin($('#p4f-input'), 3);
                strKPI += ',p4a:5=' + validatePoin($('#p4a-input'), 5);
                strKPI += ',p4b:4=' + validatePoin($('#p4b-input'), 4);
                strKPI += ',p4c:3=' + validatePoin($('#p4c-input'), 3);
                strKPI += ',p4d:4=' + validatePoin($('#p4d-input'), 4);
                strKPI += ',p4e:4=' + validatePoin($('#p4e-input'), 4);
                strKPI += ',p4f:3=' + validatePoin($('#p4f-input'), 3);

                if (poin > 0) {
                    $('#poin-input').val(poin);
                    $('#kpi-input').val(strKPI);

                    if (poin > 100) {
                        $('#poin-input').val('');
                        $('#kpi-input').val('');
                    }
                }

                return (poin <= 100);
            }

            function validatePoin(input, max) {
                var intValue = 0;

                if (input.val() !== '') {
                    intValue = parseInt(input.val());

                    if (intValue > max) {
                        intValue = max;
                    }
                }

                if (intValue > 0) {
                    input.val(intValue);
                }

                return intValue;
            }

            function reInit(selectID, placeholder, kode) {
                selectID.select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: placeholder});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            }

            function editPilih(selector, kode) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + kode,
                    dataType: 'json', type: 'POST', cache: false
                }).then(function (data) {
                    // create the option and append to Select2
                    selector.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                    // manually trigger the `select2:select` event
                    selector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data['data']
                        }
                    });
                });
            }
        </script>
    </body>
</html>