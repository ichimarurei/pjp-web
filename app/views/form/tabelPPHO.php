<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />
        <style>
            #map { width:100%; height:300px; }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Operasional</li><li>Tabel</li><li>PPHO &amp; Kunjungan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                PPHO &amp; Kunjungan
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>PPHO &amp; Kunjungan</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Tanggal</th>
                                                    <th>Tim PPHO (Pihak Pertama)</th>
                                                    <th>Tim PPHO (Pihak Kedua)</th>
                                                    <th>Chief</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="gps-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Peta Lokasi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id='map'></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="ppho">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="proyek-input" name="proyek-input" value="">
                                <input type="hidden" id="chief-input" name="chief-input" value="">
                                <input type="hidden" id="danru-input" name="danru-input" value="">
                                <input type="hidden" id="satpam-input" name="satpam-input" value="">
                                <input type="hidden" id="pihak_satu-input" name="pihak_satu-input" value="">
                                <input type="hidden" id="pihak_dua-input" name="pihak_dua-input" value="">
                                <input type="hidden" id="long-input" name="long-input" value="">
                                <input type="hidden" id="lat-input" name="lat-input" value="">
                                <input type="hidden" id="keadaan-input" name="keadaan-input" value="">
                                <input type="hidden" id="seragam-input" name="seragam-input" value="">
                                <input type="hidden" id="sikap-input" name="sikap-input" value="">
                                <input type="hidden" id="rapih-input" name="rapih-input" value="">
                                <input type="hidden" id="rajin-input" name="rajin-input" value="">
                                <input type="hidden" id="komunikasi-input" name="komunikasi-input" value="">
                                <input type="hidden" id="respon-input" name="respon-input" value="">
                                <input type="hidden" id="performa-input" name="performa-input" value="">
                                <input type="hidden" id="tanggal-input" name="tanggal-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
        <script>
            $(document).ready(function () {
                initTable("<?php echo site_url('data/tabel/ppho/0'); ?>", '#dt_basic', [
                    {"data": "tanggal"},
                    {"data": "pihak1"},
                    {"data": "pihak2"},
                    {"data": "chief"},
                    {"data": "aksi", 'width': '21%'}
                ]);

                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        var tabelnya = $('#dt_basic').DataTable();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/ppho'); ?>/" + $('#proyek-filter').val()).load();
                        tabelnya.columns.adjust().draw();
                    }
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/form/dataPPHO'); ?>/" + $(this).attr('href'));

                    return false;
                });
                $(table + " .mapBtn").on("click", function () {
                    var longlat = $(this).attr('href').split('___');
                    initMap(parseFloat(longlat[0]), parseFloat(longlat[1]));

                    return false;
                });
                $(table + " .removeBtn").on("click", function () {
                    var idData = $(this).attr('href');
                    swal({
                        title: 'Lanjutkan Proses?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        $.blockUI({message: '<h1>Memproses...</h1>'});
                        fillInputs(idData);
                    });

                    return false;
                });
            }

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=ppho&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#proyek-input').val(response['data'].proyek);
                        $('#chief-input').val(response['data'].chief);
                        $('#danru-input').val(response['data'].danru);
                        $('#satpam-input').val(response['data'].satpam);
                        $('#pihak_satu-input').val(response['data'].pihak_satu);
                        $('#pihak_dua-input').val(response['data'].pihak_dua);
                        $('#long-input').val(response['data'].long);
                        $('#lat-input').val(response['data'].lat);
                        $('#keadaan-input').val(response['data'].keadaan);
                        $('#seragam-input').val(response['data'].seragam);
                        $('#sikap-input').val(response['data'].sikap);
                        $('#rapih-input').val(response['data'].rapih);
                        $('#rajin-input').val(response['data'].rajin);
                        $('#komunikasi-input').val(response['data'].komunikasi);
                        $('#respon-input').val(response['data'].respon);
                        $('#performa-input').val(response['data'].performa);
                        $('#tanggal-input').val(response['data'].tanggal);
                        doSave('Dihapus', "<?php echo site_url('modul/tampil/form/tabelPPHO'); ?>", $('#model-form'));
                    }
                });
            }

            function initMap(lon, lat) {
                mapboxgl.accessToken = 'pk.eyJ1IjoiaWNoaW1hcnVyZWkiLCJhIjoiY2ozZmxxaTRnMDEzMDJ4bW8yb3MzZDhuayJ9.K1vvEnEMzVsvwjkdEXP7hA';
                var longlat = new mapboxgl.LngLat(lon, lat);
                var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    zoom: 18,
                    center: longlat
                });
                new mapboxgl.Marker().setLngLat(longlat).addTo(map);
                $('#gps-modal').modal();
                $('#gps-modal').on('shown.bs.modal', function (e) {
                    map.resize();
                });
            }
        </script>
    </body>
</html>