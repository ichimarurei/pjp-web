<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>APEL Serah Terima</li><li>Pendataan</li><li>APEL</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                APEL
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>APEL</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <ul class="nav nav-tabs bordered">
                                            <li class="active"><a href="#fieldset-umum" data-toggle="tab">Umum</a></li>
                                            <li><a href="#fieldset-personil" data-toggle="tab">Personil</a></li>
                                            <li><a href="#fieldset-kegiatan" data-toggle="tab">Laporan Kegiatan</a></li>
                                            <li><a href="#fieldset-lengkap" data-toggle="tab">Kondisi Kelengkapan</a></li>
                                            <li><a href="#fieldset-foto" data-toggle="tab">Lampiran (Gambar)</a></li>
                                        </ul>
                                        <div class="tab-content padding-10">
                                            <div class="tab-pane fade in active" id="fieldset-umum">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="apel">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="waktu-input" name="waktu-input" value="">
                                                    <input type="hidden" id="personil-input" name="personil-input" value="">
                                                    <input type="hidden" id="absen-input" name="absen-input" value="">
                                                    <input type="hidden" id="bko-input" name="bko-input" value="">
                                                    <input type="hidden" id="laporan-input" name="laporan-input" value="">
                                                    <input type="hidden" id="masalah-input" name="masalah-input" value="">
                                                    <input type="hidden" id="kondisi-input" name="kondisi-input" value="">
                                                    <input type="hidden" id="lat-input" name="lat-input" value="">
                                                    <input type="hidden" id="long-input" name="long-input" value="">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Yang Menyerahkan</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="penyerah-input" name="penyerah-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Yang Menerima</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="penerima-input" name="penerima-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Lokasi</label>
                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Lokasi APEL" type="text" id="lokasi-input" name="lokasi-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Waktu</label>
                                                            <div class="col-md-3">
                                                                <input class="form-control" placeholder="Tanggal APEL" type="text" id="tanggal-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <input class="form-control" placeholder="Jam APEL" type="text" id="jam-input" data-provide="timepicker" data-show-meridian="false">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Selama</label>
                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Lama Serah Terima" type="text" id="selama-input" name="selama-input">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-personil">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Anggota Personil</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pegawai</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="pegawai1-input"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah1-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <br>
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Tidak Hadir</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pegawai</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="pegawai2-input"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah2-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <br>
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>BKO</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pegawai</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="pegawai3-input"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah3-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic3" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-kegiatan">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-info pull-right" type="button" id="tambah4-button">
                                                            <i class="fa fa-plus"></i> Tambah Uraian
                                                        </button>
                                                        <table id="dt_basic4" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                                <tr><th>Taruna Hari ini Berdasarkan Buku Mutasi</th></tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <br><hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn btn-info pull-right" type="button" id="tambah5-button">
                                                            <i class="fa fa-plus"></i> Tambah Uraian
                                                        </button>
                                                        <table id="dt_basic5" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                                <tr><th>Penyelesaian Masalah</th></tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-lengkap">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Uraian</label>
                                                            <div class="col-md-8">
                                                                <select style="width:100%" class="select2 form-control" id="uraian-input"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah6-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <br>
                                                <table id="dt_basic6" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Kelengkapan Kebutuhan</th>
                                                            <th>Kondisi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-foto">
                                                <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                                <div class="row" id="galeri-div">
                                                    <hr>
                                                    <div class="superbox col-sm-12" id="galeri-akun">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelAPEL'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                basicTable('#dt_basic1', [{"data": "kode", 'visible': false}, {"data": "pegawai"}]);
                basicTable('#dt_basic2', [{"data": "kode", 'visible': false}, {"data": "pegawai"}]);
                basicTable('#dt_basic3', [{"data": "kode", 'visible': false}, {"data": "pegawai"}]);
                basicTable('#dt_basic4', [{"data": "item"}]);
                basicTable('#dt_basic5', [{"data": "item"}]);
                basicTable('#dt_basic6', [{"data": "kode", 'visible': false}, {"data": "uraian"}, {"data": "status"}]);
                var tabel1 = $('#dt_basic1').DataTable();
                var tabel2 = $('#dt_basic2').DataTable();
                var tabel3 = $('#dt_basic3').DataTable();
                var tabel4 = $('#dt_basic4').DataTable();
                var tabel5 = $('#dt_basic5').DataTable();
                var tabel6 = $('#dt_basic6').DataTable();
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>', [
                    tabel1, tabel2, tabel3, tabel4, tabel5, tabel6
                ]);
                initUploader();
                $('#galeri-div').hide();

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#penyerah-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pihak/Pegawai Yang Menyerahkan'});
                    }
                });
                $('#penerima-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pihak/Pegawai Yang Menerima'});
                    }
                });
                $('#pegawai1-input, #pegawai2-input, #pegawai3-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#uraian-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Uraian'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/uraian'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    reInit($('#penyerah-input'), 'Pilih Pihak/Pegawai Yang Menyerahkan', kode);
                    reInit($('#penerima-input'), 'Pilih Pihak/Pegawai Yang Menerima', kode);
                    reInit($('#pegawai1-input'), 'Pilih Pegawai', kode);
                    reInit($('#pegawai2-input'), 'Pilih Pegawai', kode);
                    reInit($('#pegawai3-input'), 'Pilih Pegawai', kode);
                });

                $('#dt_basic1').on('click', 'tbody tr', function () {
                    delPersonil(tabel1.row(this));
                });
                $('#dt_basic2').on('click', 'tbody tr', function () {
                    delPersonil(tabel2.row(this));
                });
                $('#dt_basic3').on('click', 'tbody tr', function () {
                    delPersonil(tabel3.row(this));
                });
                $('#dt_basic4').on('click', 'tbody tr', function () {
                    delUraian(tabel4.row(this));
                });
                $('#dt_basic5').on('click', 'tbody tr', function () {
                    delUraian(tabel5.row(this));
                });
                $('#dt_basic6').on('click', 'tbody tr', function () {
                    var dataRow = tabel6.row(this);
                    swal({
                        title: 'Lanjutkan Menghapus "' + dataRow.data().uraian + '"?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        dataRow.remove().draw();
                    });
                });

                $('#tambah1-button').click(function () {
                    addPersonil($('#pegawai1-input').select2('data'), tabel1, tabel2, tabel3, tabel1.row);
                });
                $('#tambah2-button').click(function () {
                    addPersonil($('#pegawai2-input').select2('data'), tabel1, tabel2, tabel3, tabel2.row);
                });
                $('#tambah3-button').click(function () {
                    addPersonil($('#pegawai3-input').select2('data'), tabel1, tabel2, tabel3, tabel3.row);
                });
                $('#tambah4-button').click(function () {
                    addUraian('Taruna Hari ini Berdasarkan Buku Mutasi', tabel4.row);
                });
                $('#tambah5-button').click(function () {
                    addUraian('Penyelesaian Masalah', tabel5.row);
                });
                $('#tambah6-button').click(function () {
                    var uraiOpt = $('#uraian-input').select2('data');

                    if ((typeof uraiOpt[0] !== 'undefined')) {
                        addKelengkapan(tabel6, uraiOpt[0].text, uraiOpt[0].id);
                    } else {
                        notifSwal('Pilih Uraian terlebih dahulu!');
                    }
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    assignPersonil(tabel1, tabel2, tabel3);
                    assignLaporan(tabel4, tabel5);
                    assignKondisi(tabel6);

                    if ($('#tanggal-input').val() !== '' && $('#jam-input').val() !== '') {
                        $('#waktu-input').val($('#tanggal-input').val() + ' ' + $('#jam-input').val());
                    }

                    doSave('Disimpan', "<?php echo site_url('modul/tampil/form/tabelAPEL'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param, tables) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=apel&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#lokasi-input').val(response['data'].lokasi);
                        $('#waktu-input').val(response['data'].waktu);
                        $('#personil-input').val(response['data'].personil);
                        $('#absen-input').val(response['data'].absen);
                        $('#bko-input').val(response['data'].bko);
                        $('#selama-input').val(response['data'].selama);
                        $('#laporan-input').val(response['data'].laporan);
                        $('#masalah-input').val(response['data'].masalah);
                        $('#kondisi-input').val(response['data'].kondisi);

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            var waktuStr = response['data'].waktu.split(' ');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            editPilih($('#penyerah-input'), response['data'].penyerah);
                            editPilih($('#penerima-input'), response['data'].penerima);
                            $('#lat-input').val(response['data'].lat);
                            $('#long-input').val(response['data'].long);
                            $('#tanggal-input').val(waktuStr[0]);
                            $('#jam-input').val(waktuStr[1]);
                            isiUlang(tables, [
                                response['data'].personil, response['data'].absen, response['data'].bko,
                                response['data'].laporan, response['data'].masalah, response['data'].kondisi
                            ]);
                            viewImages(response['data'].kode);
                        } else {
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(meOn, gagalGPS);
                            }
                        }
                    }
                });
            }

            function reInit(selectID, placeholder, kode) {
                selectID.select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: placeholder});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            }

            function editPilih(selector, kode) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + kode,
                    dataType: 'json', type: 'POST', cache: false
                }).then(function (data) {
                    // create the option and append to Select2
                    selector.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                    // manually trigger the `select2:select` event
                    selector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data['data']
                        }
                    });
                });
            }

            function nambahItemVer1(dataOpt, tabel1, tabel2, tabel3, activeRow) {
                var isExist = false;

                tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (!isExist) {
                        isExist = (this.data().kode === dataOpt.id);
                    }
                });
                tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (!isExist) {
                        isExist = (this.data().kode === dataOpt.id);
                    }
                });
                tabel3.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (!isExist) {
                        isExist = (this.data().kode === dataOpt.id);
                    }
                });

                if (!isExist) {
                    activeRow.add({
                        'kode': dataOpt.id, 'pegawai': dataOpt.text
                    }).draw();
                }
            }

            function addPersonil(dataOpt, tabel1, tabel2, tabel3, activeRow) {
                if ((typeof dataOpt[0] !== 'undefined')) {
                    nambahItemVer1(dataOpt[0], tabel1, tabel2, tabel3, activeRow);
                } else {
                    notifSwal('Pilih Pegawai terlebih dahulu!');
                }
            }

            function addUraian(tentang, activeRow) {
                swal({
                    title: 'Uraian',
                    input: 'textarea',
                    inputPlaceholder: tentang,
                    confirmButtonColor: "#00a65a",
                    confirmButtonText: "Tambahkan",
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {
                            if (value) {
                                resolve();
                            } else {
                                reject('Isikan ' + tentang.toLowerCase() + '!');
                            }
                        });
                    }
                }).then(function (uraian) {
                    activeRow.add({'item': uraian.replace(/\r?\n/g, '<br/>')}).draw();
                });
            }

            function addKelengkapan(tabel6, uraian, kode) {
                swal({
                    title: uraian,
                    input: 'text',
                    inputPlaceholder: 'Kondisi Kelengkapan',
                    confirmButtonColor: "#00a65a",
                    confirmButtonText: "Tambahkan",
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {
                            if (value) {
                                resolve();
                            } else {
                                reject('Isikan Kondisi Kelengkapan!');
                            }
                        });
                    }
                }).then(function (kondisi) {
                    var isExist = false;

                    tabel6.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (!isExist) {
                            isExist = (this.data().kode === kode);
                        }
                    });

                    if (!isExist) {
                        tabel6.row.add({'kode': kode, 'uraian': uraian.trim(), 'status': kondisi}).draw();
                    }
                });
            }

            function notifSwal(text) {
                swal({
                    title: 'Peringatan',
                    html: text,
                    timer: 3000,
                    type: 'warning',
                    showConfirmButton: false
                });
            }

            function delPersonil(dataRow) {
                swal({
                    title: 'Lanjutkan Menghapus "' + dataRow.data().pegawai + '"?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    dataRow.remove().draw();
                });
            }

            function delUraian(dataRow) {
                swal({
                    title: 'Lanjutkan Menghapus "' + dataRow.data().item.substring(0, 15) + '..."?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    dataRow.remove().draw();
                });
            }

            function assignPersonil(tabel1, tabel2, tabel3) {
                var hadir = '';
                var absen = '';
                var bko = '';

                tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (hadir !== '') {
                        hadir += '___';
                    }

                    hadir += this.data().kode + ':' + this.data().pegawai;
                });
                tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (absen !== '') {
                        absen += '___';
                    }

                    absen += this.data().kode + ':' + this.data().pegawai;
                });
                tabel3.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (bko !== '') {
                        bko += '___';
                    }

                    bko += this.data().kode + ':' + this.data().pegawai;
                });

                $('#personil-input').val(hadir);
                $('#absen-input').val(absen);
                $('#bko-input').val(bko);
            }

            function assignLaporan(tabel4, tabel5) {
                var laporan = '';
                var masalah = '';

                tabel4.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (laporan !== '') {
                        laporan += '___';
                    }

                    laporan += this.data().item;
                });
                tabel5.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (masalah !== '') {
                        masalah += '___';
                    }

                    masalah += this.data().item;
                });

                $('#laporan-input').val(laporan);
                $('#masalah-input').val(masalah);
            }

            function assignKondisi(tabel6) {
                var isian = '';

                tabel6.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (isian !== '') {
                        isian += '___';
                    }

                    isian += this.data().kode + ':' + this.data().uraian + '=' + this.data().status;
                });

                $('#kondisi-input').val(isian);
            }

            function isiUlang(tables, items) {
                $.each(items, function (index, item) {
                    if (item.trim() !== '') {
                        var values = item.split('___');

                        $.each(values, function (at, value) {
                            var isiData = {};

                            if (index <= 2) {
                                var info = value.split(':');
                                isiData = {'kode': info[0], 'pegawai': info[1]};
                            }

                            if (index === 3 || index === 4) {
                                isiData = {'item': value.replace(/\r?\n/g, '<br/>')};
                            }

                            if (index > 4) {
                                var urai = value.split('=');
                                var tentang = urai[0].split(':');
                                isiData = {'kode': tentang[0], 'uraian': tentang[1].trim(), 'status': urai[1]};
                            }

                            tables[index].row.add(isiData).draw();
                        });
                    }
                });
            }

            function meOn(gps) {
                $('#lat-input').val(gps.coords.latitude);
                $('#long-input').val(gps.coords.longitude);
            }

            function gagalGPS(error) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        notifSwal('Akses "Geolocation" Anda tolak!');

                        break;
                    case error.POSITION_UNAVAILABLE:
                        notifSwal('Informasi "Geolocation" tidak ditemukan!');

                        break;
                    case error.TIMEOUT:
                        notifSwal('Request "Geolocation" timed out!');

                        break;
                    case error.UNKNOWN_ERROR:
                        notifSwal('Terjadi masalah pada saat Request "Geolocation"!');

                        break;
                }
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 3, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/apel', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '') {
                        viewImages($('#kode-input').val());
                    }

                    $('#filedocument').fileinput('clear');
                });
            }

            function viewImages(kode) {
                $.when(lihatGaleri('etc/apel/' + kode)).done(function (galleries) {
                    var images = '';

                    $.each(galleries.galeri, function (index, value) {
                        images += '<div class="superbox-list">';
                        images += '<a href="<?php echo base_url('etc/apel'); ?>/' + kode + '/' + value + '" data-toggle="lightbox" data-gallery="galeri-' + kode + '">';
                        images += '<img src="<?php echo base_url('etc/apel'); ?>/' + kode + '/' + value + '" class="superbox-img">';
                        images += '</a>';
                        images += '</div>';
                    });

                    if (images !== '') {
                        $('#galeri-akun').html(images);
                        $('#galeri-div').show();
                    }
                });
            }

            function aksi(table) {}
        </script>
    </body>
</html>