<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>HRD</li><li>Tabel</li><li>Pegawai <i class="param-span"></i></li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Pegawai <i class="param-span"></i>
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <form class="form-horizontal" style="margin: 12px 12px 0 0;">
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <select style="width:100%" class="select2 form-control" id="proyek-filter"></select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" type="button" id="filter-button">Filter</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-large"></i> </span>
                                    <h2>Pegawai <i class="param-span"></i></h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Pegawai</th>
                                                    <th>Nama</th>
                                                    <th>No Telepon</th>
                                                    <th>Proyek</th>
                                                    <th>Jabatan</th>
                                                    <th>Akun</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <form id="model-form">
                                <!-- Model Mandatories -->
                                <input type="hidden" name="model-input" value="bio">
                                <input type="hidden" name="action-input" id="action-input" value="">
                                <input type="hidden" name="key-input" id="key-input" value="">
                                <input type="hidden" name="kode-input" id="kode-input" value="">
                                <!-- Model Data -->
                                <input type="hidden" name="terpakai-input" value="0">
                                <input type="hidden" id="id-input" name="id-input" value="">
                                <input type="hidden" id="ktp-input" name="ktp-input" value="">
                                <input type="hidden" id="npwp-input" name="npwp-input" value="">
                                <input type="hidden" id="bpjs-input" name="bpjs-input" value="">
                                <input type="hidden" id="nama-input" name="nama-input" value="">
                                <input type="hidden" id="kelamin-input" name="kelamin-input" value="">
                                <input type="hidden" id="tempat_lahir-input" name="tempat_lahir-input" value="">
                                <input type="hidden" id="tanggal_lahir-input" name="tanggal_lahir-input" value="">
                                <input type="hidden" id="agama-input" name="agama-input" value="">
                                <input type="hidden" id="pendidikan-input" name="pendidikan-input" value="">
                                <input type="hidden" id="rekening-input" name="rekening-input" value="">
                                <input type="hidden" id="alamat-input" name="alamat-input" value="">
                                <input type="hidden" id="telepon-input" name="telepon-input" value="">
                                <input type="hidden" id="email-input" name="email-input" value="">
                                <input type="hidden" id="nikah-input" name="nikah-input" value="">
                                <input type="hidden" id="anak-input" name="anak-input" value="">
                            </form>
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var statParam = '<?php echo ($this->uri->segment(5) === FALSE) ? 'semua' : $this->uri->segment(5); ?>';
            var urlParam = 's_a___p_a';
            var title = '';

            $(document).ready(function () {
                if (statParam === 'aktif') {
                    urlParam = 's_1___p_a';
                    title = '(Aktif)';
                } else if (statParam === 'pasif') {
                    urlParam = 's_0___p_a';
                    title = '(Tidak Aktif)';
                }

                $('.param-span').text(title);
                initTable("<?php echo site_url('data/tabel/bio'); ?>/" + urlParam, '#dt_basic', [
                    {"data": "id"},
                    {"data": "nama"},
                    {"data": "telepon"},
                    {"data": "proyek"},
                    {"data": "jabatan"},
                    {"data": "akun"},
                    {"data": "kontrak"},
                    {"data": "lihat", 'width': '7%'}
                ]);
                $('#proyek-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#filter-button').click(function () {
                    // reload tabel by ajax call
                    if ($('#proyek-filter').val() !== null) {
                        urlParam = urlParam.substring(0, 8) + $('#proyek-filter').val();
                    }

                    var tabelnya = $('#dt_basic').DataTable();
                    tabelnya.ajax.url("<?php echo site_url('data/tabel/bio'); ?>/" + urlParam).load();
                    tabelnya.columns.adjust().draw();
                });
                $(document).on('click', '.unduhan-link', function () {
                    swal.close();
                });
            });

            function aksi(table) {
                $(table + " .actionBtn").on("click", function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/form/dataProfil'); ?>/" + $(this).attr('href'));

                    return false;
                });
            }
        </script>
    </body>
</html>