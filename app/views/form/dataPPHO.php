<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Operasional</li><li>Pendataan</li><li>PPHO &amp; Kunjungan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                PPHO &amp; Kunjungan
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>PPHO &amp; Kunjungan</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <ul class="nav nav-tabs bordered">
                                            <li class="active"><a href="#fieldset-umum" data-toggle="tab">Umum</a></li>
                                            <li><a href="#fieldset-personil" data-toggle="tab">Personil</a></li>
                                            <li><a href="#fieldset-kpi" data-toggle="tab">KPI</a></li>
                                            <li><a href="#fieldset-lapangan" data-toggle="tab">Keadaan Lapangan</a></li>
                                            <li><a href="#fieldset-foto" data-toggle="tab">Lampiran (Gambar)</a></li>
                                        </ul>
                                        <div class="tab-content padding-10">
                                            <div class="tab-pane fade in active" id="fieldset-umum">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="ppho">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" id="lat-input" name="lat-input" value="">
                                                    <input type="hidden" id="long-input" name="long-input" value="">
                                                    <input type="hidden" id="chief-input" name="chief-input" value="">
                                                    <input type="hidden" id="danru-input" name="danru-input" value="">
                                                    <input type="hidden" id="satpam-input" name="satpam-input" value="">
                                                    <input type="hidden" id="seragam-input" name="seragam-input" value="">
                                                    <input type="hidden" id="sikap-input" name="sikap-input" value="">
                                                    <input type="hidden" id="rapih-input" name="rapih-input" value="">
                                                    <input type="hidden" id="rajin-input" name="rajin-input" value="">
                                                    <input type="hidden" id="komunikasi-input" name="komunikasi-input" value="">
                                                    <input type="hidden" id="respon-input" name="respon-input" value="">
                                                    <input type="hidden" id="performa-input" name="performa-input" value="">
                                                    <input type="hidden" id="keadaan-input" name="keadaan-input" value="">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tanggal</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Kunjungan" type="text" id="tanggal-input" name="tanggal-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tim PPHO</label>
                                                            <div class="col-md-5">
                                                                <select style="width:100%" class="select2 form-control" id="pihak_satu-input" name="pihak_satu-input"></select>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <select style="width:100%" class="select2 form-control" id="pihak_dua-input" name="pihak_dua-input"></select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-personil">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Chief</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="chief-filter"></select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <br>
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Danru</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pegawai</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="danru-filter"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah1-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <br>
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <legend>Satpam</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pegawai</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="satpam-filter"></select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-info" type="button" id="tambah2-button">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-kpi">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Seragam</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Seragam" type="text" id="seragam-filter">
                                                            </div>
                                                            <label class="col-md-2 control-label">Sikap</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Sikap" type="text" id="sikap-filter">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Penampilan</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Penampilan/Kerapihan" type="text" id="rapih-filter">
                                                            </div>
                                                            <label class="col-md-2 control-label">Kerajinan</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Kerajinan" type="text" id="rajin-filter">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Komunikasi</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Komunikasi" type="text" id="komunikasi-filter">
                                                            </div>
                                                            <label class="col-md-2 control-label">Respon</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Respon Terhadap Masalah" type="text" id="respon-filter">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Performance</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="KPI Performance" type="text" id="performa-filter">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-lapangan">
                                                <form class="form-horizontal">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Keadaan</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Keadaan Lapangan" rows="4" id="keadaan-filter"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-foto">
                                                <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                                <div class="row" id="galeri-div">
                                                    <hr>
                                                    <div class="superbox col-sm-12" id="galeri-akun">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelPPHO'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                basicTable('#dt_basic1', [{"data": "kode", 'visible': false}, {"data": "pegawai"}]);
                basicTable('#dt_basic2', [{"data": "kode", 'visible': false}, {"data": "pegawai"}]);
                var tabel1 = $('#dt_basic1').DataTable();
                var tabel2 = $('#dt_basic2').DataTable();
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>', [tabel1, tabel2]);
                initUploader();
                $('#galeri-div').hide();

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#chief-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Chief'});
                    }
                });
                $('#danru-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Danru'});
                    }
                });
                $('#satpam-filter').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Satpam'});
                    }
                });
                $('#pihak_satu-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Tim PPHO (Pihak Pertama)'});
                    }
                });
                $('#pihak_dua-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Tim PPHO (Pihak Kedua)'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    reInit($('#chief-filter'), 'Pilih Chief', kode);
                    reInit($('#danru-filter'), 'Pilih Danru', kode);
                    reInit($('#satpam-filter'), 'Pilih Satpam', kode);
                    reInit($('#pihak_satu-input'), 'Pilih Tim PPHO (Pihak Pertama)', kode);
                    reInit($('#pihak_dua-input'), 'Pilih Tim PPHO (Pihak Kedua)', kode);
                });

                $('#dt_basic1').on('click', 'tbody tr', function () {
                    delPersonil(tabel1.row(this));
                });
                $('#dt_basic2').on('click', 'tbody tr', function () {
                    delPersonil(tabel2.row(this));
                });

                $('#tambah1-button').click(function () {
                    addPersonil($('#danru-filter').select2('data'), tabel1, tabel2, tabel1.row);
                });
                $('#tambah2-button').click(function () {
                    addPersonil($('#satpam-filter').select2('data'), tabel1, tabel2, tabel2.row);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    assignPersonil(tabel1, tabel2);
                    $('#chief-input').val($('#chief-filter').val());
                    $('#keadaan-input').val($('#keadaan-filter').val());
                    $('#seragam-input').val($('#seragam-filter').val());
                    $('#sikap-input').val($('#sikap-filter').val());
                    $('#rapih-input').val($('#rapih-filter').val());
                    $('#rajin-input').val($('#rajin-filter').val());
                    $('#komunikasi-input').val($('#komunikasi-filter').val());
                    $('#respon-input').val($('#respon-filter').val());
                    $('#performa-input').val($('#performa-filter').val());
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/form/tabelPPHO'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param, tables) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=ppho&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#keadaan-filter').val(response['data'].keadaan);
                        $('#seragam-filter').val(response['data'].seragam);
                        $('#sikap-filter').val(response['data'].sikap);
                        $('#rapih-filter').val(response['data'].rapih);
                        $('#rajin-filter').val(response['data'].rajin);
                        $('#komunikasi-filter').val(response['data'].komunikasi);
                        $('#respon-filter').val(response['data'].respon);
                        $('#performa-filter').val(response['data'].performa);
                        $('#tanggal-input').val(response['data'].tanggal);

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            if (response['data'].chief !== '' && response['data'].chief !== '-' && response['data'].chief.length > 0) {
                                editPilih($('#chief-filter'), response['data'].chief);
                            }

                            editPilih($('#pihak_satu-input'), response['data'].pihak_satu);
                            editPilih($('#pihak_dua-input'), response['data'].pihak_dua);
                            $('#lat-input').val(response['data'].lat);
                            $('#long-input').val(response['data'].long);
                            isiUlang(tables, [response['data'].danru, response['data'].satpam]);
                            viewImages(response['data'].kode);
                        } else {
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(meOn, gagalGPS);
                            }
                        }
                    }
                });
            }

            function reInit(selectID, placeholder, kode) {
                selectID.select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: placeholder});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            }

            function editPilih(selector, kode) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + kode,
                    dataType: 'json', type: 'POST', cache: false
                }).then(function (data) {
                    // create the option and append to Select2
                    selector.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                    // manually trigger the `select2:select` event
                    selector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data['data']
                        }
                    });
                });
            }

            function meOn(gps) {
                $('#lat-input').val(gps.coords.latitude);
                $('#long-input').val(gps.coords.longitude);
            }

            function gagalGPS(error) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        notifSwal('Akses "Geolocation" Anda tolak!');

                        break;
                    case error.POSITION_UNAVAILABLE:
                        notifSwal('Informasi "Geolocation" tidak ditemukan!');

                        break;
                    case error.TIMEOUT:
                        notifSwal('Request "Geolocation" timed out!');

                        break;
                    case error.UNKNOWN_ERROR:
                        notifSwal('Terjadi masalah pada saat Request "Geolocation"!');

                        break;
                }
            }

            function notifSwal(text) {
                swal({
                    title: 'Peringatan',
                    html: text,
                    timer: 3000,
                    type: 'warning',
                    showConfirmButton: false
                });
            }

            function addPersonil(dataOpt, tabel1, tabel2, activeRow) {
                if ((typeof dataOpt[0] !== 'undefined')) {
                    nambahItemVer1(dataOpt[0], tabel1, tabel2, activeRow);
                } else {
                    notifSwal('Pilih Pegawai terlebih dahulu!');
                }
            }

            function nambahItemVer1(dataOpt, tabel1, tabel2, activeRow) {
                var isExist = false;

                tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (!isExist) {
                        isExist = (this.data().kode === dataOpt.id);
                    }
                });
                tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (!isExist) {
                        isExist = (this.data().kode === dataOpt.id);
                    }
                });

                if (!isExist) {
                    activeRow.add({
                        'kode': dataOpt.id, 'pegawai': dataOpt.text
                    }).draw();
                }
            }

            function delPersonil(dataRow) {
                swal({
                    title: 'Lanjutkan Menghapus "' + dataRow.data().pegawai + '"?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    dataRow.remove().draw();
                });
            }

            function assignPersonil(tabel1, tabel2) {
                var danru = '';
                var satpam = '';

                tabel1.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (danru !== '') {
                        danru += '___';
                    }

                    danru += this.data().kode + ':' + this.data().pegawai;
                });
                tabel2.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (satpam !== '') {
                        satpam += '___';
                    }

                    satpam += this.data().kode + ':' + this.data().pegawai;
                });

                $('#danru-input').val(danru);
                $('#satpam-input').val(satpam);
            }

            function isiUlang(tables, items) {
                $.each(items, function (index, item) {
                    if (item.trim() !== '') {
                        var values = item.split('___');

                        $.each(values, function (at, value) {
                            var info = value.split(':');
                            tables[index].row.add({'kode': info[0], 'pegawai': info[1]}).draw();
                        });
                    }
                });
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 3, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/ppho', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '') {
                        viewImages($('#kode-input').val());
                    }

                    $('#filedocument').fileinput('clear');
                });
            }

            function viewImages(kode) {
                $.when(lihatGaleri('etc/ppho/' + kode)).done(function (galleries) {
                    var images = '';

                    $.each(galleries.galeri, function (index, value) {
                        images += '<div class="superbox-list">';
                        images += '<a href="<?php echo base_url('etc/ppho'); ?>/' + kode + '/' + value + '" data-toggle="lightbox" data-gallery="galeri-' + kode + '">';
                        images += '<img src="<?php echo base_url('etc/ppho'); ?>/' + kode + '/' + value + '" class="superbox-img">';
                        images += '</a>';
                        images += '</div>';
                    });

                    if (images !== '') {
                        $('#galeri-akun').html(images);
                        $('#galeri-div').show();
                    }
                });
            }

            function aksi(table) {}
        </script>
    </body>
</html>