<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Operasional</li><li>Pendataan</li><li>KPI (Penilaian) Chief/Danru</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                KPI (Penilaian) Chief/Danru
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>KPI (Penilaian) Chief/Danru</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <!-- Model Mandatories -->
                                            <input type="hidden" name="model-input" value="kpi">
                                            <input type="hidden" name="action-input" id="action-input" value="">
                                            <input type="hidden" name="key-input" id="key-input" value="">
                                            <input type="hidden" name="kode-input" id="kode-input" value="">
                                            <!-- Model Data -->
                                            <input type="hidden" name="terpakai-input" value="1">
                                            <input type="hidden" name="level-input" value="ketua">
                                            <input type="hidden" name="pembuat-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                            <input type="hidden" name="pengawas-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                            <input type="hidden" name="penerima-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                            <input type="hidden" id="kpi-input" name="kpi-input" value="">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Proyek</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Pegawai</label>
                                                    <div class="col-md-6">
                                                        <select style="width:100%" class="select2 form-control" id="pegawai-input" name="pegawai-input"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Tanggal</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Tanggal Pendataan" type="text" id="tanggal-input" name="tanggal-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="no-padding">
                                                    <ul class="nav nav-tabs bordered">
                                                        <li class="active"><a href="#fieldset-performa" data-toggle="tab">Performance Anggota</a></li>
                                                        <li><a href="#fieldset-lengkap" data-toggle="tab">Kelengkapan Data Lapangan</a></li>
                                                        <li><a href="#fieldset-hubungan" data-toggle="tab">Komunikasi &amp; Koordinasi</a></li>
                                                        <li><a href="#fieldset-target" data-toggle="tab">Target Kerja</a></li>
                                                    </ul>
                                                    <div class="tab-content padding-10">
                                                        <div class="tab-pane fade in active" id="fieldset-performa">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Mengontrol Kerapihan Seragam Anggotanya, Serta Kelengkapannya</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1a-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Mengontrol Penampilan Fisik Anggotanya</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1b-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Membimbing Anggota Dalam Hal Sikap Dan Mental</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1c-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Mengarahkan Dan Mengawasi Absensi Anggota</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1d-input">
                                                                        <span class="input-group-addon"> <strong>/ 6</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Mengontrol Waktu Hadir Kerja Anggota</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1e-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Meminimalisir Jumlah Anggota Yang Bermasalah (Dikenakan SP)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1f-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Merefresh Anggota Dalam Hal PBB Maupun SOP</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p1g-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-lengkap">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Memiliki Data Kontak Anggota</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2a-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Memiliki Data Kelengkapan Inventaris Barang</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2b-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Mengisi &amp; Melakukan Pengecekan Buku Duty Log (Mutasi)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2c-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Menampung Informasi Keadaan Anggota (Back-Up, Izin, Sakit, Dll)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2d-input">
                                                                        <span class="input-group-addon"> <strong>/ 6</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Memenuhi Proses &amp; Prosedur Data Kejadian (Berita Acara, Laporan Kejadian, Dll)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p2e-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-hubungan">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Mudah Dihubungi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3a-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Memberikan Informasi Terbaru Kepada Anggota Dari Atasan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3b-input">
                                                                        <span class="input-group-addon"> <strong>/ 6</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Memberikan Arahan Disetiap APEL Serah Terima Jaga</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3c-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Mampu Menjadi Contoh Kepada Anggota Baik Secara SOP Maupun Peraturan</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3d-input">
                                                                        <span class="input-group-addon"> <strong>/ 6</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Membantu Chief Sebagai Penghubung Antara Anggota Dan Team Operasional (HO) Dalam Hal Administrasi (Perijinan, Resign, Dll)</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p3e-input">
                                                                        <span class="input-group-addon"> <strong>/ 4</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="fieldset-target">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Memiliki Target Anggota Yang Akan Dibentuk Sesuai KPI &amp; SOP</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4a-input">
                                                                        <span class="input-group-addon"> <strong>/ 6</strong> </span>
                                                                    </div>
                                                                </div>
                                                                <label class="col-md-4 control-label">Membentuk Anggota - Anggota Yang Berpotensi</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4b-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Terbentuknya Leadership Danru Terhadap Anggota</label>
                                                                <div class="col-md-2">
                                                                    <div class="input-group">
                                                                        <input class="form-control" placeholder="....." type="text" id="p4c-input">
                                                                        <span class="input-group-addon"> <strong>/ 5</strong> </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Total Bobot</label>
                                                    <div class="col-md-2">
                                                        <div class="input-group">
                                                            <input class="form-control" placeholder="....." type="text" id="poin-input" name="poin-input" readonly>
                                                            <span class="input-group-addon"> <strong>/ 100</strong> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <hr>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelDKPI'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#pegawai-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Pegawai'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    reInit($('#pegawai-input'), 'Pilih Pegawai', kode);
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});

                    if (joinPoin()) {
                        doSave('Disimpan', "<?php echo site_url('modul/tampil/form/tabelDKPI'); ?>", $('#model-form'));
                    } else {
                        $.unblockUI();
                        swal({
                            title: 'Peringatan',
                            html: 'Total Bobot KPI tidak boleh melebihi 100!',
                            timer: 3000,
                            type: 'warning',
                            showConfirmButton: false
                        });
                    }
                });
            });

            function fillInputs(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=kpi&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#tanggal-input').val(response['data'].tanggal);
                        $('#kpi-input').val(response['data'].kpi);
                        $('#poin-input').val(response['data'].poin);

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            editPilih($('#pegawai-input'), response['data'].pegawai);

                            if (response['data'].kpi !== '') {
                                var kpiItems = response['data'].kpi.split(',');

                                $.each(kpiItems, function (index, value) {
                                    var values = value.split('=');
                                    var info = values[0].split(':');
                                    $('#' + info[0] + '-input').val(values[1]);
                                });
                            }
                        }
                    }
                });
            }

            function joinPoin() {
                var poin = 0;
                var strKPI = '';

                // Performance Anggota
                poin += validatePoin($('#p1a-input'), 5);
                poin += validatePoin($('#p1b-input'), 5);
                poin += validatePoin($('#p1c-input'), 5);
                poin += validatePoin($('#p1d-input'), 6);
                poin += validatePoin($('#p1e-input'), 5);
                poin += validatePoin($('#p1f-input'), 4);
                poin += validatePoin($('#p1g-input'), 4);
                strKPI += 'p1a:3=' + validatePoin($('#p1a-input'), 5);
                strKPI += ',p1b:3=' + validatePoin($('#p1b-input'), 5);
                strKPI += ',p1c:3=' + validatePoin($('#p1c-input'), 5);
                strKPI += ',p1d:4=' + validatePoin($('#p1d-input'), 6);
                strKPI += ',p1e:3=' + validatePoin($('#p1e-input'), 5);
                strKPI += ',p1f:3=' + validatePoin($('#p1f-input'), 4);
                strKPI += ',p1g:3=' + validatePoin($('#p1g-input'), 4);
                // Kelengkapan Data Lapangan
                poin += validatePoin($('#p2a-input'), 4);
                poin += validatePoin($('#p2b-input'), 4);
                poin += validatePoin($('#p2c-input'), 5);
                poin += validatePoin($('#p2d-input'), 6);
                poin += validatePoin($('#p2e-input'), 5);
                strKPI += ',p2a:3=' + validatePoin($('#p2a-input'), 4);
                strKPI += ',p2b:3=' + validatePoin($('#p2b-input'), 4);
                strKPI += ',p2c:3=' + validatePoin($('#p2c-input'), 5);
                strKPI += ',p2d:3=' + validatePoin($('#p2d-input'), 6);
                strKPI += ',p2e:3=' + validatePoin($('#p2e-input'), 5);
                // Komunikasi & Koordinasi
                poin += validatePoin($('#p3a-input'), 5);
                poin += validatePoin($('#p3b-input'), 6);
                poin += validatePoin($('#p3c-input'), 5);
                poin += validatePoin($('#p3d-input'), 6);
                poin += validatePoin($('#p3e-input'), 4);
                strKPI += ',p3a:3=' + validatePoin($('#p3a-input'), 5);
                strKPI += ',p3b:3=' + validatePoin($('#p3b-input'), 6);
                strKPI += ',p3c:2=' + validatePoin($('#p3c-input'), 5);
                strKPI += ',p3d:3=' + validatePoin($('#p3d-input'), 6);
                strKPI += ',p3e:4=' + validatePoin($('#p3e-input'), 4);
                // Target Kerja
                poin += validatePoin($('#p4a-input'), 6);
                poin += validatePoin($('#p4b-input'), 5);
                poin += validatePoin($('#p4c-input'), 5);
                strKPI += ',p4a:5=' + validatePoin($('#p4a-input'), 6);
                strKPI += ',p4b:4=' + validatePoin($('#p4b-input'), 5);
                strKPI += ',p4c:3=' + validatePoin($('#p4c-input'), 5);

                if (poin > 0) {
                    $('#poin-input').val(poin);
                    $('#kpi-input').val(strKPI);

                    if (poin > 100) {
                        $('#poin-input').val('');
                        $('#kpi-input').val('');
                    }
                }

                return (poin <= 100);
            }

            function validatePoin(input, max) {
                var intValue = 0;

                if (input.val() !== '') {
                    intValue = parseInt(input.val());

                    if (intValue > max) {
                        intValue = max;
                    }
                }

                if (intValue > 0) {
                    input.val(intValue);
                }

                return intValue;
            }

            function reInit(selectID, placeholder, kode) {
                selectID.select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: placeholder});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            }

            function editPilih(selector, kode) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + kode,
                    dataType: 'json', type: 'POST', cache: false
                }).then(function (data) {
                    // create the option and append to Select2
                    selector.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                    // manually trigger the `select2:select` event
                    selector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data['data']
                        }
                    });
                });
            }
        </script>
    </body>
</html>