<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-ribbon fixed-page-footer">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Komplain</li><li>Pendataan</li><li>Komplain Klien</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-edit"></i>
                            Pendataan
                            <span>>
                                Komplain Klien
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                                    <h2>Komplain Klien</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <ul class="nav nav-tabs bordered">
                                            <li class="active"><a href="#fieldset-umum" data-toggle="tab">Umum</a></li>
                                            <li><a href="#fieldset-foto" data-toggle="tab">Lampiran (Gambar)</a></li>
                                        </ul>
                                        <div class="tab-content padding-10">
                                            <div class="tab-pane fade in active" id="fieldset-umum">
                                                <form class="form-horizontal" id="model-form">
                                                    <!-- Model Mandatories -->
                                                    <input type="hidden" name="model-input" value="teror">
                                                    <input type="hidden" name="action-input" id="action-input" value="">
                                                    <input type="hidden" name="key-input" id="key-input" value="">
                                                    <input type="hidden" name="kode-input" id="kode-input" value="">
                                                    <!-- Model Data -->
                                                    <input type="hidden" name="terpakai-input" value="1">
                                                    <input type="hidden" name="pembuat-input" value="<?php echo $this->session->userdata('_bio'); ?>">
                                                    <input type="hidden" id="komplain-input" name="komplain-input" value="">
                                                    <fieldset>
                                                        <legend>Umum</legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Proyek</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="proyek-input" name="proyek-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Penerima</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="penerima-input" name="penerima-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pihak Pertama</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="pihak_satu-input" name="pihak_satu-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Pihak Kedua</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="pihak_dua-input" name="pihak_dua-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Klien</label>
                                                            <div class="col-md-6">
                                                                <select style="width:100%" class="select2 form-control" id="klien-input" name="klien-input"></select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Nomor</label>
                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Nomor Komplain" type="text" id="nomor-input" name="nomor-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Lokasi</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" placeholder="Detail Lokasi" type="text" id="lokasi-input" name="lokasi-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tanggal</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Komplain" type="text" id="tanggal_aduan-input" name="tanggal_aduan-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Diterima/Proses" type="text" id="tanggal_proses-input" name="tanggal_proses-input" data-provide="datepicker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Status</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="setuju-input" name="setuju-input">
                                                                    <option value="0">Baru/Proses</option>
                                                                    <option value="1">Sudah Disetujui</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <legend>Detail Komplain</legend>
                                                        <div class="form-group">
                                                            <div class="col-md-12 text-right">
                                                                <button class="btn btn-info" type="button" id="tambah-button">
                                                                    <i class="fa fa-plus"></i> Tambah Komplain
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Keterangan Komplain</th>
                                                            <th>Penyelesaian Komplain</th>
                                                            <th>Target Penyelesaian</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="fieldset-foto">
                                                <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                                <div class="row" id="galeri-div">
                                                    <hr>
                                                    <div class="superbox col-sm-12" id="galeri-akun">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="form-horizontal">
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default" href="<?php echo site_url('modul/tampil/form/tabelKomplain'); ?>">Batal</a>
                                                        <button class="btn btn-primary" type="button" id="simpan-button">
                                                            <i class="fa fa-save"></i>
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal -->
                            <div class="modal fade" id="uraian-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Detail Komplain</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Keterangan</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Keterangan Komplain" rows="4" id="isi-filter"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Penyelesaian</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Penyelesaian Komplain" rows="4" id="solusi-filter"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Target</label>
                                                        <div class="col-md-10">
                                                            <textarea class="form-control text-tetap" placeholder="Target Penyelesaian" rows="4" id="target-filter"></textarea>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="button" id="isi-button">Tambahkan</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                basicTable('#dt_basic', [{"data": "isi"}, {"data": "solusi"}, {"data": "target"}]);
                var tabel = $('#dt_basic').DataTable();
                fillInputs('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>', tabel);
                initUploader();
                $('#galeri-div').hide();

                $('#proyek-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Proyek'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/proyek'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
                $('#penerima-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Diterima Oleh'});
                    }
                });
                $('#pihak_satu-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Diketahui Oleh (Pihak Pertama)'});
                    }
                });
                $('#pihak_dua-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Diketahui Oleh (Pihak Kedua)'});
                    }
                });
                $('#klien-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Klien'});
                    }
                });
                $('#proyek-input').change(function () {
                    var kode = $(this).val();
                    reInit($('#penerima-input'), 'Pilih Diterima Oleh', kode);
                    reInit($('#pihak_satu-input'), 'Pilih Diketahui Oleh (Pihak Pertama)', kode);
                    reInit($('#pihak_dua-input'), 'Pilih Diketahui Oleh (Pihak Kedua)', kode);
                    $('#klien-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Klien'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/klien'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#dt_basic').on('click', 'tbody tr', function () {
                    var dataRow = tabel.row(this);
                    swal({
                        title: 'Lanjutkan Menghapus "' + dataRow.data().isi.substring(0, 15) + '..."?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal'
                    }).then(function () {
                        dataRow.remove().draw();
                    });
                });
                $('#tambah-button').click(function () {
                    $('#isi-filter').val('');
                    $('#solusi-filter').val('');
                    $('#target-filter').val('');
                    $('#uraian-modal').modal();
                });
                $('#isi-button').click(function () {
                    if ($('#isi-filter').val() !== '' && $('#solusi-filter').val() !== '' && $('#target-filter').val() !== '') {
                        tabel.row.add({
                            'isi': $('#isi-filter').val().replace(/\r?\n/g, '<br/>'),
                            'solusi': $('#solusi-filter').val().replace(/\r?\n/g, '<br/>'),
                            'target': $('#target-filter').val().replace(/\r?\n/g, '<br/>')
                        }).draw();
                        $('#uraian-modal').modal('hide');
                    }
                });

                $('#simpan-button').click(function () {
                    $.blockUI({message: '<h1>Memproses...</h1>'});
                    var laporan = '';

                    tabel.rows().every(function (rowIdx, tableLoop, rowLoop) {
                        if (laporan !== '') {
                            laporan += '___';
                        }

                        laporan += this.data().isi + '|' + this.data().solusi + '|' + this.data().target;
                    });

                    $('#komplain-input').val(laporan);
                    doSave('Disimpan', "<?php echo site_url('modul/tampil/form/tabelKomplain'); ?>", $('#model-form'));
                });
            });

            function fillInputs(param, tabel) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=teror&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#nomor-input').val(response['data'].nomor);
                        $('#lokasi-input').val(response['data'].lokasi);
                        $('#komplain-input').val(response['data'].komplain);
                        $('#tanggal_aduan-input').val(response['data'].tanggal_aduan);
                        $('#tanggal_proses-input').val(response['data'].tanggal_proses);
                        $('#setuju-input').val(response['data'].setuju).trigger('change');

                        if (response['data'].key > 0) {
                            var pilihProyek = $('#proyek-input');
                            var pilihKlien = $('#klien-input');
                            var values = response['data'].komplain.split('___');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=proyek&kode=' + response['data'].proyek,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProyek.append(new Option(data['data'].proyek + ' | ' + data['data'].tipeText, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProyek.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=klien&kode=' + response['data'].klien,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKlien.append(new Option(data['data'].nama + ' (@' + data['data'].id + ')', data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKlien.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            editPilih($('#penerima-input'), response['data'].penerima);
                            editPilih($('#pihak_satu-input'), response['data'].pihak_satu);
                            editPilih($('#pihak_dua-input'), response['data'].pihak_dua);
                            viewImages(response['data'].kode);

                            $.each(values, function (at, value) {
                                var info = value.split('|');
                                tabel.row.add({
                                    'isi': info[0].replace(/\r?\n/g, '<br/>'),
                                    'solusi': info[1].replace(/\r?\n/g, '<br/>'),
                                    'target': info[2].replace(/\r?\n/g, '<br/>')
                                }).draw();
                            });
                        }
                    }
                });
            }

            function reInit(selectID, placeholder, kode) {
                selectID.select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: placeholder});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/bio'); ?>/" + kode,
                        dataType: 'json', type: 'POST', cache: false
                    }
                });
            }

            function editPilih(selector, kode) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=bio&kode=' + kode,
                    dataType: 'json', type: 'POST', cache: false
                }).then(function (data) {
                    // create the option and append to Select2
                    selector.append(new Option(data['data'].nama.toUpperCase() + ' [' + data['data'].id.toUpperCase() + ']', data['data'].kode, true, true)).trigger('change');
                    // manually trigger the `select2:select` event
                    selector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data['data']
                        }
                    });
                });
            }

            function initUploader() {
                var filename = '';
                $('#filedocument').fileinput({
                    maxFileCount: 3, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-link", browseLabel: "Pilih", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-link", removeLabel: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                    uploadClass: "btn btn-link", uploadLabel: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> ',
                    cancelClass: "btn btn-link", cancelLabel: "Batal", cancelIcon: '<i class="fa fa-minus"></i> ',
                    showCaption: false, language: 'id',
                    fileActionSettings: {
                        removeClass: "btn btn-link", removeTitle: "Hapus", removeIcon: '<i class="fa fa-times"></i> ',
                        uploadClass: "btn btn-link", uploadTitle: "Unggah", uploadIcon: '<i class="fa fa-upload"></i> '
                    },
                    uploadExtraData: function () {
                        return {path: 'etc/komplain', dir: $('#kode-input').val()};
                    }
                });
                $('#filedocument').on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });
                $('#filedocument').on('fileuploaded', function () {
                    if (filename !== '') {
                        viewImages($('#kode-input').val());
                    }

                    $('#filedocument').fileinput('clear');
                });
            }

            function viewImages(kode) {
                $.when(lihatGaleri('etc/komplain/' + kode)).done(function (galleries) {
                    var images = '';

                    $.each(galleries.galeri, function (index, value) {
                        images += '<div class="superbox-list">';
                        images += '<a href="<?php echo base_url('etc/komplain'); ?>/' + kode + '/' + value + '" data-toggle="lightbox" data-gallery="galeri-' + kode + '">';
                        images += '<img src="<?php echo base_url('etc/komplain'); ?>/' + kode + '/' + value + '" class="superbox-img">';
                        images += '</a>';
                        images += '</div>';
                    });

                    if (images !== '') {
                        $('#galeri-akun').html(images);
                        $('#galeri-div').show();
                    }
                });
            }

            function aksi(table) {}
        </script>
    </body>
</html>