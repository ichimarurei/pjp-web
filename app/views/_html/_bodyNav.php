<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <nav>
        <ul>
            <li>
                <a href="<?php echo site_url(); ?>"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent"><?php echo $this->session->userdata('_otoritas'); ?></span></a>
            </li>
            <li class="menu-data-master" style="display: none;">
                <a href="#" title="Data Master"><i class="fa fa-lg fa-fw fa-database"></i> <span class="menu-item-parent">Data Master</span></a>
                <ul>
                    <li class="menu-jenis-proyek">
                        <a href="#" title="Data Jenis Proyek">Jenis Proyek</a>
                        <ul>
                            <li class="menu-jenis-proyek-data">
                                <a href="<?php echo site_url('modul/tampil/master/dataTipe'); ?>" title="Pendataan Jenis Proyek"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-jenis-proyek-tabel">
                                <a href="<?php echo site_url('modul/tampil/master/tabelTipe'); ?>" title="Tabel/Daftar Jenis Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-jabatan">
                        <a href="#" title="Data Jabatan">Jabatan</a>
                        <ul>
                            <li class="menu-jabatan-data">
                                <a href="<?php echo site_url('modul/tampil/master/dataJabatan'); ?>" title="Pendataan Jabatan"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-jabatan-tabel">
                                <a href="<?php echo site_url('modul/tampil/master/tabelJabatan'); ?>" title="Tabel/Daftar Jabatan"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-bank">
                        <a href="#" title="Data Bank">Bank</a>
                        <ul>
                            <li class="menu-bank-data">
                                <a href="<?php echo site_url('modul/tampil/master/dataBank'); ?>" title="Pendataan Bank"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-bank-tabel">
                                <a href="<?php echo site_url('modul/tampil/master/tabelBank'); ?>" title="Tabel/Daftar Bank"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-uraian">
                        <a href="#" title="Data Uraian">Uraian</a>
                        <ul>
                            <li class="menu-uraian-data">
                                <a href="<?php echo site_url('modul/tampil/master/dataUraian'); ?>" title="Pendataan Uraian"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-uraian-tabel">
                                <a href="<?php echo site_url('modul/tampil/master/tabelUraian'); ?>" title="Tabel/Daftar Uraian"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-data-proyek" style="display: none;">
                <a href="#" title="Data Proyek"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent">Proyek</span></a>
                <ul>
                    <li class="menu-proyek">
                        <a href="#" title="Data Info Proyek">Data Proyek</a>
                        <ul>
                            <li class="menu-proyek-data">
                                <a href="<?php echo site_url('modul/tampil/proyek/dataProyek'); ?>" title="Pendataan Proyek"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-proyek-tabel">
                                <a href="<?php echo site_url('modul/tampil/proyek/tabelProyek'); ?>" title="Tabel/Daftar Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-klien">
                        <a href="#" title="Data Klien Proyek">Data Klien</a>
                        <ul>
                            <li class="menu-klien-data">
                                <a href="<?php echo site_url('modul/tampil/akun/dataKlien'); ?>" title="Pendataan Klien Proyek"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-klien-tabel">
                                <a href="<?php echo site_url('modul/tampil/akun/tabelKlien'); ?>" title="Tabel/Daftar Klien Proyek"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-data-personalia" style="display: none;">
                <a href="#" title="Data Personalia"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">Personalia</span></a>
                <ul>
                    <li class="menu-akun">
                        <a href="#" title="Data Pegawai">Data Pegawai</a>
                        <ul>
                            <li class="menu-akun-data">
                                <a href="<?php echo site_url('modul/tampil/akun/dataProfil'); ?>" title="Pendataan Pegawai"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-akun-tabel-semua">
                                <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/semua'); ?>" title="Tabel/Daftar Semua Pegawai"><span class="menu-item-parent">Tabel Semua Pegawai</span></a>
                            </li>
                            <li class="menu-akun-tabel-aktif">
                                <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/aktif'); ?>" title="Tabel/Daftar Pegawai Aktif"><span class="menu-item-parent">Tabel Pegawai Aktif</span></a>
                            </li>
                            <li class="menu-akun-tabel-pasif">
                                <a href="<?php echo site_url('modul/tampil/akun/tabelProfil/pasif'); ?>" title="Tabel/Daftar Pegawai Tidak Aktif"><span class="menu-item-parent">Tabel Pegawai Tidak Aktif</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-cuti">
                        <a href="#" title="Data Cuti Pegawai">Cuti</a>
                        <ul>
                            <li class="menu-cuti-data">
                                <a href="<?php echo site_url('modul/tampil/presensi/dataCuti'); ?>" title="Ajuan Cuti Pegawai"><span class="menu-item-parent">Pengajuan</span></a>
                            </li>
                            <li class="menu-cuti-tabel-pegawai">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelCuti'); ?>" title="Tabel/Daftar Cuti Pegawai"><span class="menu-item-parent">Tabel Cuti Pegawai</span></a>
                            </li>
                            <li class="menu-cuti-tabel-saya">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelCutiQ'); ?>" title="Tabel/Daftar Cuti Saya"><span class="menu-item-parent">Tabel Cuti Saya</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" title="Data Presensi Pegawai"><i class="fa fa-lg fa-fw fa-calendar-check-o"></i> <span class="menu-item-parent">Absensi Pegawai</span></a>
                <ul>
                    <li class="menu-presensi">
                        <a href="#" title="Data Presensi">Absensi</a>
                        <ul>
                            <li class="menu-presensi-data menu-data-presensi" style="display: none;">
                                <a href="<?php echo site_url('modul/tampil/presensi/dataPresensi'); ?>" title="Pendataan Absensi Kehadiran"><span class="menu-item-parent">Absensi</span></a>
                            </li>
                            <li class="menu-presensi-tabel-pegawai menu-data-presensi" style="display: none;">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelPresensi'); ?>" title="Tabel/Daftar Kehadiran Pegawai"><span class="menu-item-parent">Tabel Absensi Pegawai</span></a>
                            </li>
                            <li class="menu-presensi-tabel-lembur menu-data-presensi" style="display: none;">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelLembur'); ?>" title="Tabel/Daftar Lembur Pegawai"><span class="menu-item-parent">Tabel Lembur Pegawai</span></a>
                            </li>
                            <li class="menu-presensi-tabel-pulang menu-data-presensi" style="display: none;">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelPulang'); ?>" title="Tabel/Daftar Pulang Pegawai"><span class="menu-item-parent">Tabel Pulang Pegawai</span></a>
                            </li>
                            <li class="menu-presensi-tabel-saya">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelPresensiQ'); ?>" title="Tabel/Daftar Absensi Saya"><span class="menu-item-parent">Tabel Absensi Saya</span></a>
                            </li>
                            <li class="menu-presensi-tabel-begadang">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelLemburQ'); ?>" title="Tabel/Daftar Lembur Saya"><span class="menu-item-parent">Tabel Lembur Saya</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-kehadiran" style="display: none;">
                        <a href="#" title="Rekap Cut/Off Presensi Pegawai">Rekap Cut/Off Absensi</a>
                        <ul>
                            <li class="menu-kehadiran-data">
                                <a href="<?php echo site_url('modul/tampil/presensi/dataPRekap'); ?>" title="Pendataan Rekap Cut/Off Presensi"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-kehadiran-tabel">
                                <a href="<?php echo site_url('modul/tampil/presensi/tabelPRekap'); ?>" title="Tabel/Daftar Rekap Cut/Off Presensi"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-data-klaim" style="display: none;">
                <a href="#" title="Data Klaim Pegawai"><i class="fa fa-lg fa-fw fa-credit-card"></i> <span class="menu-item-parent">Klaim Pegawai</span></a>
                <ul>
                    <li class="menu-klaim-data">
                        <a href="<?php echo site_url('modul/tampil/klaim/dataKlaim'); ?>" title="Pendataan Klaim/Reimburse"><span class="menu-item-parent">Pengajuan</span></a>
                    </li>
                    <li class="menu-klaim-tabel-sudah">
                        <a href="<?php echo site_url('modul/tampil/klaim/tabelKlaim/sudah'); ?>" title="Tabel/Daftar Klaim/Reimburse Sudah Dibayar"><span class="menu-item-parent">Tabel Sudah Dibayar</span></a>
                    </li>
                    <li class="menu-klaim-tabel-belum">
                        <a href="<?php echo site_url('modul/tampil/klaim/tabelKlaim/belum'); ?>" title="Tabel/Daftar Klaim/Reimburse Belum Dibayar"><span class="menu-item-parent">Tabel Belum Dibayar</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-data-ig" style="display: none;">
                <a href="#" title="Data Invoice dan Gaji"><i class="fa fa-lg fa-fw fa-calculator"></i> <span class="menu-item-parent">Invoice &amp; Gaji</span></a>
                <ul>
                    <li class="menu-ig-data">
                        <a href="<?php echo site_url('modul/tampil/ig/dataIG'); ?>" title="Pendataan Invoice"><span class="menu-item-parent">Proses Invoice</span></a>
                    </li>
                    <li class="menu-ig-tabel-invoice-tagih">
                        <a href="<?php echo site_url('modul/tampil/ig/tabelInv/tagih'); ?>" title="Tabel/Daftar Invoice Ditagihkan"><span class="menu-item-parent">Tabel Invoice Ditagihkan</span></a>
                    </li>
                    <li class="menu-ig-tabel-invoice-bayar">
                        <a href="<?php echo site_url('modul/tampil/ig/tabelInv/bayar'); ?>" title="Tabel/Daftar Invoice Sudah Dibayarkan"><span class="menu-item-parent">Tabel Invoice Terbayar</span></a>
                    </li>
                    <li class="menu-ig-tabel-gaji">
                        <a href="<?php echo site_url('modul/tampil/ig/kelolaGaji'); ?>" title="Tabel/Daftar Penggajian"><span class="menu-item-parent">Tabel Penggajian</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-data-kpi" style="display: none;">
                <a href="#" title="Data KPI"><i class="fa fa-lg fa-fw fa-trophy"></i> <span class="menu-item-parent">KPI</span></a>
                <ul>
                    <li class="menu-pegawai">
                        <a href="#" title="Data Pegawai">Data Pegawai</a>
                        <ul>
                            <li class="menu-pegawai-info">
                                <a href="<?php echo site_url('modul/tampil/form/tabelProfil/aktif'); ?>" title="Tabel/Daftar Pegawai Aktif"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-akpi">
                        <a href="#" title="Data Penilaian Anggota (KPI)">Data KPI Anggota</a>
                        <ul>
                            <li class="menu-akpi-data">
                                <a href="<?php echo site_url('modul/tampil/form/dataAKPI'); ?>" title="Pendataan KPI"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-akpi-tabel">
                                <a href="<?php echo site_url('modul/tampil/form/tabelAKPI'); ?>" title="Tabel/Daftar KPI"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dkpi">
                        <a href="#" title="Data Penilaian Chief/Danru (KPI)">Data KPI Chief/Danru</a>
                        <ul>
                            <li class="menu-dkpi-data">
                                <a href="<?php echo site_url('modul/tampil/form/dataDKPI'); ?>" title="Pendataan KPI"><span class="menu-item-parent">Pendataan</span></a>
                            </li>
                            <li class="menu-dkpi-tabel">
                                <a href="<?php echo site_url('modul/tampil/form/tabelDKPI'); ?>" title="Tabel/Daftar KPI"><span class="menu-item-parent">Tabel/Daftar</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-data-operasional" style="display: none;">
                <a href="#" title="Data Operasional (PPHO)"><i class="fa fa-lg fa-fw fa-tasks"></i> <span class="menu-item-parent">PPHO</span></a>
                <ul>
                    <li class="menu-ppho-data">
                        <a href="<?php echo site_url('modul/tampil/form/dataPPHO'); ?>" title="Pendataan PPHO"><span class="menu-item-parent">Pendataan</span></a>
                    </li>
                    <li class="menu-ppho-tabel">
                        <a href="<?php echo site_url('modul/tampil/form/tabelPPHO'); ?>" title="Tabel/Daftar PPHO"><span class="menu-item-parent">Tabel/Daftar</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-data-kpp" style="display: none;">
                <a href="#" title="Data Kunjungan KPP"><i class="fa fa-lg fa-fw fa-bookmark"></i> <span class="menu-item-parent">KPP</span></a>
                <ul>
                    <li class="menu-kpp-data">
                        <a href="<?php echo site_url('modul/tampil/form/dataKPP'); ?>" title="Pendataan Kunjungan KPP"><span class="menu-item-parent">Pendataan</span></a>
                    </li>
                    <li class="menu-kpp-tabel">
                        <a href="<?php echo site_url('modul/tampil/form/tabelKPP'); ?>" title="Tabel/Daftar Kunjungan KPP"><span class="menu-item-parent">Tabel/Daftar</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-data-apel" style="display: none;">
                <a href="#" title="Data APEL Serah Terima"><i class="fa fa-lg fa-fw fa-retweet"></i> <span class="menu-item-parent">APEL Serah Terima</span></a>
                <ul>
                    <li class="menu-apel-data">
                        <a href="<?php echo site_url('modul/tampil/form/dataAPEL'); ?>" title="Pendataan APEL Serah Terima"><span class="menu-item-parent">Pendataan</span></a>
                    </li>
                    <li class="menu-apel-tabel">
                        <a href="<?php echo site_url('modul/tampil/form/tabelAPEL'); ?>" title="Tabel/Daftar APEL Serah Terima"><span class="menu-item-parent">Tabel/Daftar</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-data-teror" style="display: none;">
                <a href="#" title="Data Komplain Klien"><i class="fa fa-lg fa-fw fa-frown-o"></i> <span class="menu-item-parent">Komplain</span></a>
                <ul>
                    <li class="menu-teror-data">
                        <a href="<?php echo site_url('modul/tampil/form/dataKomplain'); ?>" title="Pendataan Komplain Klien"><span class="menu-item-parent">Pendataan</span></a>
                    </li>
                    <li class="menu-teror-tabel">
                        <a href="<?php echo site_url('modul/tampil/form/tabelKomplain'); ?>" title="Tabel/Daftar Komplain Klien"><span class="menu-item-parent">Tabel/Daftar</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo site_url('akses/untuk/keluar'); ?>"><i class="fa fa-lg fa-fw fa-sign-out"></i> <span class="menu-item-parent">Keluar</span></a>
            </li>
        </ul>
    </nav>

    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<!-- END NAVIGATION -->