<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url('res/SmartAdmin/js/plugin/pace/pace.min.js'); ?>"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="<?php echo base_url('res/SmartAdmin/js/libs/jquery-3.2.1.min.js'); ?>"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url('res/SmartAdmin/js/app.config.js'); ?>"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js'); ?>"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url('res/SmartAdmin/js/bootstrap/bootstrap.min.js'); ?>"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url('res/SmartAdmin/js/smartwidgets/jarvis.widget.min.js'); ?>"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js'); ?>"></script>

<!-- SPARKLINES -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/sparkline/jquery.sparkline.min.js'); ?>"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/jquery-validate/jquery.validate.min.js'); ?>"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/masked-input/jquery.maskedinput.min.js'); ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url('res/select2/js/select2.full.min.js'); ?>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js'); ?>"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/msie-fix/jquery.mb.browser.min.js'); ?>"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/fastclick/fastclick.min.js'); ?>"></script>

<!--[if IE 8]>
<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url('res/SmartAdmin/js/app.min.js'); ?>"></script>

<!-- Sweetalert -->
<script src="<?php echo base_url('res/sweetalert2.js/sweetalert2.min.js'); ?>"></script>
<!-- jQuery blockUI -->
<script src="<?php echo base_url('res/sweetalert2.js/jquery.blockUI.js'); ?>"></script>
<!-- File Input -->
<script src="<?php echo base_url('res/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>
<!-- DatePicker -->
<script src="<?php echo base_url('res/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('res/bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min.js'); ?>"></script>
<!-- TimePicker -->
<script src="<?php echo base_url('res/bootstrap-timepicker/js/bootstrap-timepicker.min.js'); ?>"></script>
<!-- Lightbox -->
<script src="<?php echo base_url('res/lightbox/ekko-lightbox.min.js'); ?>"></script>
<!-- Morris Chart Dependencies -->
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/morris/raphael.min.js'); ?>"></script>
<script src="<?php echo base_url('res/SmartAdmin/js/plugin/morris/morris.min.js'); ?>"></script>
<!-- #NAVIGATION -->
<?php
$halamanAktif = '';
$extraParam = '';

if ($this->uri->segment(4) !== FALSE) {
    $halamanAktif = $this->uri->segment(3) . '/' . $this->uri->segment(4);
}

if ($this->uri->segment(5) !== FALSE) {
    $extraParam = $this->uri->segment(5);
}
?>
<script>
    var otoritasAkun = "<?php echo $this->session->userdata('_otoritas'); ?>";
    var halamanAktif = "<?php echo $halamanAktif; ?>";
    var extraParam = "<?php echo $extraParam; ?>";

    function initAppMenus() {
        var aksesJenis1 = ['admin', 'bos', 'mhrd', 'keu'];
        var aksesJenis2 = ['admin', 'bos', 'mhrd', 'shrd'];
        var aksesJenis3 = ['admin', 'bos', 'mhrd', 'shrd', 'keu'];
        var aksesJenis4 = ['admin', 'bos', 'mhrd', 'shrd', 'ope'];
        var aksesJenis5 = ['admin', 'bos', 'mhrd', 'shrd', 'kpp'];
        var aksesJenis6 = ['admin', 'bos', 'ope', 'kpp', 'danru', 'staf', 'pic'];
        var aksesJenis7 = ['admin', 'bos', 'mhrd', 'shrd', 'kpp', 'ope'];

        if (aksesJenis1.includes(otoritasAkun)) {
            $('.menu-data-master').show();
            $('.menu-data-proyek').show();
            $('.menu-data-ig').show();
        }

        if (aksesJenis2.includes(otoritasAkun)) {
            $('.menu-data-personalia').show();
            $('.menu-data-presensi').show();
            $('.menu-kehadiran').show();
            $('.menu-data-teror').show();
        }

        if (aksesJenis3.includes(otoritasAkun)) {
            $('.menu-data-klaim').show();
        }

        if (aksesJenis4.includes(otoritasAkun)) {
            $('.menu-data-operasional').show();
        }

        if (aksesJenis5.includes(otoritasAkun)) {
            $('.menu-data-kpp').show();
        }

        if (aksesJenis6.includes(otoritasAkun)) {
            $('.menu-data-apel').show();
        }

        if (aksesJenis7.includes(otoritasAkun)) {
            $('.menu-data-kpi').show();
        }

        ifMenuMaster();
        ifMenuProyek();
        ifMenuPersonalia();
        ifMenuPresensi();
        ifMenuKlaim();
        ifMenuInvoiceGaji();
        ifMenuForm();
    }

    function ifMenuMaster() {
        if (halamanAktif === 'master/dataTipe') {
            openMenuNavs('data-master', 'jenis-proyek');
            $('.menu-jenis-proyek-data').addClass('active');
        }

        if (halamanAktif === 'master/tabelTipe') {
            openMenuNavs('data-master', 'jenis-proyek');
            $('.menu-jenis-proyek-tabel').addClass('active');
        }

        if (halamanAktif === 'master/dataJabatan') {
            openMenuNavs('data-master', 'jabatan');
            $('.menu-jabatan-data').addClass('active');
        }

        if (halamanAktif === 'master/tabelJabatan') {
            openMenuNavs('data-master', 'jabatan');
            $('.menu-jabatan-tabel').addClass('active');
        }

        if (halamanAktif === 'master/dataBank') {
            openMenuNavs('data-master', 'bank');
            $('.menu-bank-data').addClass('active');
        }

        if (halamanAktif === 'master/tabelBank') {
            openMenuNavs('data-master', 'bank');
            $('.menu-bank-tabel').addClass('active');
        }

        if (halamanAktif === 'master/dataUraian') {
            openMenuNavs('data-master', 'uraian');
            $('.menu-uraian-data').addClass('active');
        }

        if (halamanAktif === 'master/tabelUraian') {
            openMenuNavs('data-master', 'uraian');
            $('.menu-uraian-tabel').addClass('active');
        }
    }

    function ifMenuProyek() {
        if (halamanAktif === 'proyek/dataProyek') {
            openMenuNavs('data-proyek', 'proyek');
            $('.menu-proyek-data').addClass('active');
        }

        if (halamanAktif === 'proyek/tabelProyek') {
            openMenuNavs('data-proyek', 'proyek');
            $('.menu-proyek-tabel').addClass('active');
        }

        if (halamanAktif === 'akun/dataKlien') {
            openMenuNavs('data-proyek', 'klien');
            $('.menu-klien-data').addClass('active');
        }

        if (halamanAktif === 'akun/tabelKlien') {
            openMenuNavs('data-proyek', 'klien');
            $('.menu-klien-tabel').addClass('active');
        }
    }

    function ifMenuPersonalia() {
        var aktifAkun = ['akun/dataProfil', 'akun/dataAkun', 'proyek/dataPegawai', 'proyek/tabelPegawai'];

        if (aktifAkun.includes(halamanAktif)) {
            openMenuNavs('data-personalia', 'akun');
            $('.menu-akun-data').addClass('active');
        }

        if (halamanAktif === 'akun/tabelProfil') {
            openMenuNavs('data-personalia', 'akun');
            $('.menu-akun-tabel-' + extraParam).addClass('active');
        }

        if (halamanAktif === 'presensi/dataCuti') {
            openMenuNavs('data-personalia', 'cuti');
            $('.menu-cuti-data').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelCuti') {
            openMenuNavs('data-personalia', 'cuti');
            $('.menu-cuti-tabel-pegawai').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelCutiQ') {
            openMenuNavs('data-personalia', 'cuti');
            $('.menu-cuti-tabel-saya').addClass('active');
        }
    }

    function ifMenuPresensi() {
        var aktifRekap = ['presensi/tabelPRekap', 'presensi/dataPArsip'];

        if (halamanAktif === 'presensi/dataPresensi') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-data').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelPresensi') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-tabel-pegawai').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelLembur') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-tabel-lembur').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelPulang') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-tabel-pulang').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelPresensiQ') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-tabel-saya').addClass('active');
        }

        if (halamanAktif === 'presensi/tabelLemburQ') {
            openMenuNavs(null, 'presensi');
            $('.menu-presensi-tabel-begadang').addClass('active');
        }

        if (halamanAktif === 'presensi/dataPRekap') {
            openMenuNavs(null, 'kehadiran');
            $('.menu-kehadiran-data').addClass('active');
        }

        if (aktifRekap.includes(halamanAktif)) {
            openMenuNavs(null, 'kehadiran');
            $('.menu-kehadiran-tabel').addClass('active');
        }
    }

    function ifMenuKlaim() {
        if (halamanAktif === 'klaim/dataKlaim') {
            openMenuNavs('data-klaim', null);
            $('.menu-klaim-data').addClass('active');
        }

        if (halamanAktif === 'klaim/tabelKlaim') {
            openMenuNavs('data-klaim', null);
            $('.menu-klaim-tabel-' + extraParam).addClass('active');
        }
    }

    function ifMenuInvoiceGaji() {
        var aktifProses = ['ig/tabelRekap', 'ig/dataIG', 'ig/dataInv'];

        if (aktifProses.includes(halamanAktif)) {
            openMenuNavs('data-ig', null);
            $('.menu-ig-data').addClass('active');
        }

        if (halamanAktif === 'ig/tabelInv') {
            openMenuNavs('data-ig', null);
            $('.menu-ig-tabel-invoice-' + extraParam).addClass('active');
        }

        if (halamanAktif === 'ig/kelolaGaji') {
            openMenuNavs('data-ig', null);
            $('.menu-ig-tabel-gaji').addClass('active');
        }
    }

    function ifMenuForm() {
        var aktifPegawai = ['form/tabelProfil', 'form/dataProfil'];

        if (aktifPegawai.includes(halamanAktif)) {
            openMenuNavs('data-kpi', 'pegawai');
            $('.menu-pegawai-info').addClass('active');
        }

        if (halamanAktif === 'form/dataAKPI') {
            openMenuNavs('data-kpi', 'akpi');
            $('.menu-akpi-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelAKPI') {
            openMenuNavs('data-kpi', 'akpi');
            $('.menu-akpi-tabel').addClass('active');
        }

        if (halamanAktif === 'form/dataDKPI') {
            openMenuNavs('data-kpi', 'dkpi');
            $('.menu-dkpi-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelDKPI') {
            openMenuNavs('data-kpi', 'dkpi');
            $('.menu-dkpi-tabel').addClass('active');
        }

        if (halamanAktif === 'form/dataPPHO') {
            openMenuNavs('data-operasional', null);
            $('.menu-ppho-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelPPHO') {
            openMenuNavs('data-operasional', null);
            $('.menu-ppho-tabel').addClass('active');
        }

        if (halamanAktif === 'form/dataKPP') {
            openMenuNavs('data-kpp', null);
            $('.menu-kpp-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelKPP') {
            openMenuNavs('data-kpp', null);
            $('.menu-kpp-tabel').addClass('active');
        }

        if (halamanAktif === 'form/dataAPEL') {
            openMenuNavs('data-apel', null);
            $('.menu-apel-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelAPEL') {
            openMenuNavs('data-apel', null);
            $('.menu-apel-tabel').addClass('active');
        }

        if (halamanAktif === 'form/dataKomplain') {
            openMenuNavs('data-teror', null);
            $('.menu-teror-data').addClass('active');
        }

        if (halamanAktif === 'form/tabelKomplain') {
            openMenuNavs('data-teror', null);
            $('.menu-teror-tabel').addClass('active');
        }
    }

    function openMenuNavs(parent, prefix) {
        if (parent !== null) {
            $('.menu-' + parent).addClass('active').addClass('open');
        }

        if (prefix !== null) {
            $('.menu-' + prefix).addClass('active').addClass('open');
        }
    }
</script>
<script>
    runAllForms();
    pageSetUp();
    initAppMenus();

    $(document).keypress(function (e) {
        if (e.which === 13) { // enter key press event
            return (e.target.nodeName.toLowerCase() === 'textarea'); // disable enter event except for textarea
        }
    });

    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    function doSave(text, url, form) {
        $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false,
            success: function (response) {
                var title = 'Success';
                var message = '';
                var icon = 'warning';

                if (response['return'].code === 0) {
                    title = 'Eror';
                    icon = 'error';
                    message = 'Error !!!';
                } else if (response['return'].code === 1) {
                    title = 'Berhasil';
                    icon = 'success';
                    message = 'Data Berhasil ' + text;
                } else if (response['return'].code === 2) {
                    title = 'Gagal';
                    icon = 'error';
                    message = 'Data Gagal ' + text;
                } else if (response['return'].code === 3) {
                    title = 'Perhatian !!!';
                    icon = 'warning';
                    message = response['return'].message;
                }

                swal({
                    title: title,
                    html: message,
                    timer: 3000,
                    type: icon,
                    showConfirmButton: false,
                    onClose: function () {
                        $.unblockUI();

                        if (response['return'].code === 1) {
                            $(location).attr('href', url);
                        }
                    }
                });
            }
        });
    }

    function doSaveCallback(form) {
        return $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false
        });
    }

    function formatRp(angka) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        var separator;

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = (split[1] !== undefined) ? rupiah + ',' + split[1] : rupiah;
        return (rupiah ? 'Rp. ' + rupiah : '');
    }

    function nominalFormats() {
        $('.nominal-text').each(function (i, obj) {
            $(obj).val(rpToNum($(obj).val()));
        });
    }

    function rpToNum(nominal) {
        var nominalText = nominal.replace('Rp. ', '');

        return nominalText.replace(/\./g, '');
    }

    // duplication image processing
    function setImageAs(img, as, dir) {
        return $.ajax({
            url: "<?php echo site_url('data/salinFoto'); ?>", data: 'img=' + img + '&dir=' + dir + '&as=' + as,
            dataType: 'json', type: 'POST', cache: false
        });
    }

    // retrieve galleries
    function lihatGaleri(dir) {
        return $.ajax({
            url: "<?php echo site_url('data/galeri'); ?>", data: 'dir=' + dir,
            dataType: 'json', type: 'POST', cache: false
        });
    }
</script>