<?php

$lang['db_invalid_connection_str'] = 'Pengaturan koneksi ke database tidak benar !!!';
$lang['db_unable_to_connect'] = 'Tidak dapat terhubung dengan database !!!';
$lang['db_unable_to_select'] = 'Tidak dapat melakukan query SELECT pada database: %s !!!';
$lang['db_unable_to_create'] = 'Tidak dapat melakukan query CREATE pada database: %s !!!';
$lang['db_invalid_query'] = 'Query salah !!!';
$lang['db_must_set_table'] = 'Tidak ditemukan nama tabel dalam query !!!';
$lang['db_must_use_set'] = 'Tidak ditemukan fungsi "SET" dalam query !!!';
$lang['db_must_use_index'] = 'Tidak ditemukan index dalam query untuk "Batch Update" !!!';
$lang['db_batch_missing_index'] = 'Terdapat satu atau lebih index yang hilang saat melakukan "Batch Update" !!!';
$lang['db_must_use_where'] = 'Tidak ditemukan "WHERE" !!!';
$lang['db_del_must_use_where'] = 'Tidak ditemukan "WHERE" !!!';
$lang['db_field_param_missing'] = 'Tidak ditemukan nama tabel dalam query !!!';
$lang['db_unsupported_function'] = 'Fungsi database tidak tersedia !!!';
$lang['db_transaction_failure'] = 'Proses gagal: telah dilakukan pengembalian status data !!!';
$lang['db_unable_to_drop'] = 'Tidak bisa membuang database!!!';
$lang['db_unsuported_feature'] = 'Fungsi database tidak tersedia !!!';
$lang['db_unsuported_compression'] = 'Fungsi arsip database tidak tersedia !!!';
$lang['db_filepath_error'] = 'Tidak dapat menyimpan data pada berkas yang telah ditentukan !!!';
$lang['db_invalid_cache_path'] = 'Lokasi "cache" tidak ditemukan atau tidak dapat diakses !!!';
$lang['db_table_name_required'] = 'Tidak ditemukan nama tabel dalam query !!!';
$lang['db_column_name_required'] = 'Tidak ditemukan nama kolom dalam query !!!';
$lang['db_column_definition_required'] = 'Tidak ditemukan nama kolom dalam query !!!';
$lang['db_unable_to_set_charset'] = 'Tidak melakukan koneksi ke database: %s !!!';
$lang['db_error_heading'] = 'Terdapat Kesalahan Pada Database !!!';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */