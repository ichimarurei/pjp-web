<?php

$lang['required']			= "Harap isikan %s !!!";
$lang['isset']				= "%s harus berisi data yang sesuai !!!";
$lang['valid_email']		= "%s harus memiliki format email yang benar !!!";
$lang['valid_emails']		= "%s harus memiliki format email yang benar !!!";
$lang['valid_url']			= "%s harus memiliki format URL yang benar !!!";
$lang['valid_ip']			= "%s harus memiliki format IP yang benar !!!";
$lang['min_length']			= "%s harus memiliki minimal %s karakter !!!";
$lang['max_length']			= "%s tidak boleh melebihi %s karakter !!!";
$lang['exact_length']		= "%s harus memiliki %s karakter !!!";
$lang['alpha']				= "%s harus berupa huruf !!!";
$lang['alpha_numeric']		= "%s harus berupa huruf atau angka !!!";
$lang['alpha_dash']			= "%s harus berupa huruf, angka, garis bawah (_), atau garis pemisah (-) !!!";
$lang['numeric']			= "%s harus berupa angka !!!";
$lang['is_numeric']			= "%s harus boleh berupa angka !!!";
$lang['integer']			= "%s harus berupa angka !!!";
$lang['regex_match']		= "%s tidak memiliki format yang sesuai !!!";
$lang['matches']			= "%s tidak sama dengan %s !!!";
$lang['is_unique'] 			= "%s sudah terdata, gunakan pilihan yang lain !!!";
$lang['is_natural']			= "%s harus berupa bilangan positif !!!";
$lang['is_natural_no_zero']	= "%s harus berupa angka yang lebih besar dari 0 !!!";
$lang['decimal']			= "%s harus berupa bilangan desimal !!!";
$lang['less_than']			= "%s harus berupa angka yang lebih kecil dari %s !!!";
$lang['greater_than']		= "%s harus berupa angka yang lebih besar dari %s !!!";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */