<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of restapi
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class RestAPI extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header('Content-Type: application/json');
        header("Access-Control-Allow-Origin: *");
    }

    public function index() {
        echo json_encode(array('status' => 1, 'pesan' => 'Akses Tidak Valid!'));
    }

    public function masuk() {
        $request = $this->input->post(NULL, TRUE); // use XSS Filtering
        $data = array();
        $status = 0;

        if ($request !== FALSE) {
            $akun = $this->model->getRecord(array('table' => 'data_akun', 'where' => array('id' => $request['id-input'], 'pin' => $this->cryptorgram->encrypt($request['pin-input']), 'terpakai' => 1)));

            if ($akun != NULL) {
                $status = 1;
                $data = array('_akun' => $akun->kode, '_bio' => $akun->biodata, '_id' => $akun->id, '_otoritas' => $akun->otoritas, '_proyek' => $akun->proyek);
            }
        }

        echo json_encode(array('status' => $status, 'data' => $data));
    }

    // create, update, or delete
    public function simpan() {
        $request = $this->input->post(NULL, TRUE); // use XSS Filtering
        $pesan = 'Akses Tidak Valid!';
        $status = 0;

        if ($request !== FALSE) {
            $model = 'model' . $request['model-input'];
            $this->load->model($model);
            $this->form_validation->set_rules($this->$model->getRules($request['action-input'])); // create, update, delete rules

            if ($this->form_validation->run() == FALSE) {
                $delimiter = '>>>';
                $this->form_validation->set_error_delimiters($delimiter, '|');
                $pesan = validation_errors();
            } else {
                if ($this->$model->doAction($request)) {
                    $status = 1;
                    $pesan = 'Proses Berhasil';
                }
            }
        }

        echo json_encode(array('status' => $status, 'pesan' => $pesan));
    }

    // view record & records
    public function detail() {
        $model = 'model' . $this->input->post('param');
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getData($this->input->post('kode'))));
    }

    public function tabel($name, $query = NULL) {
        $model = 'model' . $name;
        $this->load->model($model);
        echo json_encode(array('data' => $this->$model->getTabel($query)));
    }

    // save images
    public function gambar() {
        $request = $this->input->post(NULL, FALSE); // don't use XSS Filtering
        $pesan = 'Akses Tidak Valid!';
        $status = 0;

        if ($request !== FALSE) {
            $path = 'etc/' . $request['dir-text'] . '/' . $request['kode-text'];

            if (self::_dirInit($path)) {
                $parts = explode(';base64,', $request['gambar-text']);
                $aux = explode('image/', $parts[0]);
                $type = $aux[1];
                $base64 = base64_decode($parts[1]);
                $file = $path . '/' . $request['kode-text'] . '' . random_string('alnum', 3) . '.' . $type;
                $simpan = file_put_contents($file, $base64);

                if ($simpan !== FALSE) {
                    $status = 1;
                    $pesan = 'Proses Berhasil';
                }
            }
        }

        echo json_encode(array('status' => $status, 'pesan' => $pesan));
    }

    private function _dirInit($path) {
        $continue = FALSE;

        if (!file_exists($path)) {
            if (!is_dir($path)) {
                $continue = (!file_exists($path)) ? mkdir($path, 0777, TRUE) : TRUE;
            } else {
                $continue = TRUE;
            }
        } else {
            $continue = TRUE;
        }

        return $continue;
    }

}
