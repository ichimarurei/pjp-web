<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of data
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class Data extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        redirect(site_url());
    }

    // create, update, or delete
    public function simpan() {
        /*
         * code info:
         * 	- 0 = akses tidak sah (user undefined)
         * 	- 1 = proses berhasil
         * 	- 2 = proses gagal
         * 	- 3 = input invalid
         */
        $code = 0;
        $message = '';
        /* collect request */
        $model = 'model' . $this->input->post('model-input');

        if ($this->sesiAkun !== FALSE) {
            $this->load->model($model);
            $this->form_validation->set_rules($this->$model->getRules($this->input->post('action-input'))); // create, update, delete rules

            if ($this->form_validation->run() == FALSE) { // Inject to SweetAlert
                $delimiter = '<i class="fa fa-exclamation"></i> ';
                $this->form_validation->set_error_delimiters($delimiter, '<br>');
                $message = validation_errors();
                $code = 3;
            } else {
                $result = $this->$model->doAction($this->input->post(NULL));
                $code = ($result) ? 1 : 2;
            }
        }

        echo json_encode(array('return' => array(
                'code' => $code,
                'message' => $message
        )));
    }

    // view record & records
    public function detail() {
        $data = array();

        if ($this->sesiAkun !== FALSE) {
            $model = 'model' . $this->input->post('param');
            $this->load->model($model);
            $data = $this->$model->getData($this->input->post('kode'));
        }

        echo json_encode(array('data' => $data));
    }

    public function tabel($name, $query = NULL) {
        $data = array();

        if ($this->sesiAkun !== FALSE) {
            $model = 'model' . $name;
            $this->load->model($model);
            $data = $this->$model->getTabel($query);
        }

        echo json_encode(array('data' => $data)); // DATATABLES
    }

    public function pilih($name, $query = NULL) {
        $data = array();

        if ($this->sesiAkun !== FALSE) {
            $model = 'model' . $name;
            $this->load->model($model);

            if ($this->input->post('term') !== FALSE) {
                if ($query == NULL) {
                    $query = $this->input->post('term');
                } else {
                    $query .= '___' . $this->input->post('term');
                }
            }

            $data = $this->$model->getPilih($query);
        }

        echo json_encode(array('results' => $data)); // SELECT2JS
    }

    // ===== UPLOAD ============================================================
    public function unggah() {
        if ($this->sesiAkun !== FALSE && isset($_FILES['filedocument'])) {
            // upload setting
            $request = array(// get the files posted
                'input' => 'filedocument[]',
                'file' => $_FILES['filedocument']
            );
            $inputs = $this->input->post(NULL, TRUE); // use XSS Filtering
            $path = $inputs['path'] . '/' . $inputs['dir'];
            echo self::_upload($request, $path); // start
        }
    }

    public function salinFoto() { // copy images
        if ($this->sesiAkun !== FALSE) {
            $requests = $this->input->post(NULL, TRUE); // use XSS Filtering
            $ext = strtolower(substr($requests['img'], strrpos($requests['img'], '.')));
            $originalImage = $requests['dir'] . '/' . $requests['img'];
            $outputImage = $requests['dir'] . '/' . $requests['as'] . '.jpg';
            $imageTmp = NULL;

            if (preg_match('/jpg|jpeg/i', $ext)) {
                $imageTmp = imagecreatefromjpeg($originalImage);
            } else if (preg_match('/png/i', $ext)) {
                $imageTmp = imagecreatefrompng($originalImage);
            } else if (preg_match('/gif/i', $ext)) {
                $imageTmp = imagecreatefromgif($originalImage);
            } else if (preg_match('/bmp/i', $ext)) {
                $imageTmp = imagecreatefrombmp($originalImage);
            }

            imagejpeg($imageTmp, $outputImage, 100);
            imagedestroy($imageTmp);
        }
    }

    // view galleries
    public function galeri() {
        $files = array();
        $dir = $this->input->post('dir', TRUE);

        if ($dir !== FALSE) {
            if (file_exists($dir)) {
                $files = directory_map($dir);
            }
        }

        echo json_encode(array('galeri' => $files));
    }

    private function _upload($request, $path) {
        $error = array('error' => 'Proses unggah foto gagal');

        if (self::_dirInit($path)) {
            $this->load->library('upload', array(
                'allowed_types' => '*',
                'overwrite' => TRUE,
                'upload_path' => './' . $path . '/', // file path ...
                'max_size' => 128000 // maximum size 128 MB (in kilobytes) match it with php.ini (upload_max_filesize)
            )); // upload config
            $fails = self::_doSave($request);

            if ($fails == 0) {
                $error = array();
            } else {
                $error = array('error' => $fails . ' proses unggah foto gagal');
            }
        }

        return json_encode($error); // return a json encoded response to let user know upload results
    }

    private function _dirInit($path) {
        $continue = FALSE;

        if (!file_exists($path)) {
            if (!is_dir($path)) {
                $continue = (!file_exists($path)) ? mkdir($path, 0777, TRUE) : TRUE;
            } else {
                $continue = TRUE;
            }
        } else {
            $continue = TRUE;
        }

        return $continue;
    }

    private function _doSave($request) {
        $fails = 0;

        /* upload begin */
        foreach ($request['file']['name'] as $at => $file) {
            // mapping files
            $_FILES[$request['input']]['name'] = $request['file']['name'][$at];
            $_FILES[$request['input']]['type'] = $request['file']['type'][$at];
            $_FILES[$request['input']]['tmp_name'] = $request['file']['tmp_name'][$at];
            $_FILES[$request['input']]['error'] = $request['file']['error'][$at];
            $_FILES[$request['input']]['size'] = $request['file']['size'][$at];

            // begin
            if (!$this->upload->do_upload($request['input'])) {
                $fails++;
            }
        }

        return $fails;
    }

}
