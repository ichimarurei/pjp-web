<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of modelakun
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelAkun extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_akun';
    }

    public function doAction($params) {
        $this->setValues($params, array('id-input'));
        $this->setValue('id', preg_replace('/\s+/', '', strtolower($params['id-input'])));
        $this->setValue('pin', $this->cryptorgram->encrypt($params['pin-input']));

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        $idUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.id]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $id = array(
            'field' => 'id-input', 'label' => 'ID',
            'rules' => 'trim|max_length[11]|required' . $idUnik
        );
        $pin = array(
            'field' => 'pin-input', 'label' => 'Sandi',
            'rules' => 'trim|max_length[11]|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );

        return array($kode, $id, $pin, $biodata);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'id' => '', 'pin' => '', 'biodata' => '', 'otoritas' => 'pegawai', 'proyek' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('biodata' => $kode)));

        if ($record != NULL) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'id' => $record->id,
                'pin' => $this->cryptorgram->decrypt($record->pin),
                'biodata' => $record->biodata,
                'proyek' => $record->proyek,
                'otoritas' => $record->otoritas,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        return array();
    }

    public function getPilih($query) {
        return array();
    }

}
