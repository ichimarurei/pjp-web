<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelppho
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelPPHO extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_ppho';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal', $this->formatdate->setDate($params['tanggal-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $danru = array(
            'field' => 'danru-input', 'label' => 'Data Danru',
            'rules' => 'trim|required'
        );
        $satpam = array(
            'field' => 'satpam-input', 'label' => 'Data Satpam',
            'rules' => 'trim|required'
        );
        $pihak1 = array(
            'field' => 'pihak_satu-input', 'label' => 'Data Tim PPHO (Pihak Pertama)',
            'rules' => 'trim|required'
        );
        $pihak2 = array(
            'field' => 'pihak_dua-input', 'label' => 'Data Tim PPHO (Pihak Kedua)',
            'rules' => 'trim|required'
        );
        $tanggal = array(
            'field' => 'tanggal-input', 'label' => 'Tanggal Kunjungan',
            'rules' => 'trim|required'
        );
        $long = array(
            'field' => 'long-input', 'label' => 'Longitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $lat = array(
            'field' => 'lat-input', 'label' => 'Latitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $keadaan = array(
            'field' => 'keadaan-input', 'label' => 'Keadaan Lapangan',
            'rules' => 'trim|required'
        );
        $seragam = array(
            'field' => 'seragam-input', 'label' => 'KPI Seragam',
            'rules' => 'trim|max_length[50]|required'
        );
        $sikap = array(
            'field' => 'sikap-input', 'label' => 'KPI Sikap',
            'rules' => 'trim|max_length[50]|required'
        );
        $rapih = array(
            'field' => 'rapih-input', 'label' => 'KPI Penampilan/Kerapihan',
            'rules' => 'trim|max_length[50]|required'
        );
        $rajin = array(
            'field' => 'rajin-input', 'label' => 'KPI Kerajinan',
            'rules' => 'trim|max_length[50]|required'
        );
        $komunikasi = array(
            'field' => 'komunikasi-input', 'label' => 'KPI Komunikasi',
            'rules' => 'trim|max_length[50]|required'
        );
        $respon = array(
            'field' => 'respon-input', 'label' => 'KPI Respon Terhadap Masalah',
            'rules' => 'trim|max_length[50]|required'
        );
        $performa = array(
            'field' => 'performa-input', 'label' => 'KPI Performance',
            'rules' => 'trim|max_length[50]|required'
        );

        return array(
            $kode, $proyek, $danru, $satpam, $pihak1, $pihak2, $long, $lat, $tanggal, $keadaan, $seragam, $sikap,
            $rajin, $rapih, $komunikasi, $respon, $performa
        );
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'chief' => '', 'danru' => '', 'satpam' => '',
            'pihak_satu' => '', 'pihak_dua' => '', 'tanggal' => '', 'long' => '',
            'lat' => '', 'keadaan' => '', 'seragam' => '', 'sikap' => '',
            'rapih' => '', 'rajin' => '', 'komunikasi' => '', 'respon' => '',
            'performa' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'chief' => $record->chief,
                'danru' => $record->danru, 'satpam' => $record->satpam,
                'pihak_satu' => $record->pihak_satu, 'pihak_dua' => $record->pihak_dua,
                'long' => $record->long, 'lat' => $record->lat,
                'keadaan' => $record->keadaan, 'seragam' => $record->seragam,
                'sikap' => $record->sikap, 'rapih' => $record->rapih,
                'rajin' => $record->rajin, 'komunikasi' => $record->komunikasi,
                'respon' => $record->respon, 'performa' => $record->performa,
                'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'tanggal desc')) as $record) {
            $rChief = NULL;
            $rPihak1 = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pihak_satu)));
            $rPihak2 = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pihak_dua)));

            if ($record->chief !== '' && $record->chief !== '-') {
                $rChief = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->chief)));
            }

            if ($rPihak1 !== NULL && $rPihak2 !== NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->long . '___' . $record->lat . '" class="mapBtn btn btn-info btn-flat">Peta</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'chief' => ($rChief !== NULL) ? ucwords($rChief->nama) : '-',
                    'pihak1' => ucwords($rPihak1->nama),
                    'pihak2' => ucwords($rPihak2->nama),
                    'tanggal' => $this->formatdate->getDate($record->tanggal),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
