<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelapel
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelAPEL extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_apel';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('waktu', $this->formatdate->setDateTime($params['waktu-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $penyerah = array(
            'field' => 'penyerah-input', 'label' => 'Data Penyerah',
            'rules' => 'trim|required'
        );
        $penerima = array(
            'field' => 'penerima-input', 'label' => 'Data Penerima',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Waktu APEL',
            'rules' => 'trim|required'
        );
        $lokasi = array(
            'field' => 'lokasi-input', 'label' => 'Lokasi APEL',
            'rules' => 'trim|max_length[255]|required'
        );
        $selama = array(
            'field' => 'selama-input', 'label' => 'Lama Serah Terima',
            'rules' => 'trim|max_length[50]|required'
        );
        $personil = array(
            'field' => 'personil-input', 'label' => 'Data Personil',
            'rules' => 'trim|required'
        );
        $long = array(
            'field' => 'long-input', 'label' => 'Longitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $lat = array(
            'field' => 'lat-input', 'label' => 'Latitude',
            'rules' => 'trim|max_length[50]|required'
        );

        return array($kode, $proyek, $penyerah, $penerima, $waktu, $lokasi, $personil, $selama, $long, $lat);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'penyerah' => '', 'penerima' => '', 'lokasi' => '', 'selama' => '',
            'waktu' => '', 'personil' => '', 'long' => '', 'lat' => '',
            'absen' => '', 'bko' => '', 'laporan' => '', 'masalah' => '', 'kondisi' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'penyerah' => $record->penyerah, 'penerima' => $record->penerima,
                'lokasi' => $record->lokasi, 'selama' => $record->selama,
                'waktu' => $this->formatdate->getDateTime($record->waktu, TRUE),
                'personil' => $record->personil, 'long' => $record->long, 'lat' => $record->lat,
                'absen' => $record->absen, 'bko' => $record->bko, 'laporan' => $record->laporan,
                'masalah' => $record->masalah, 'kondisi' => $record->kondisi,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'waktu desc')) as $record) {
            $rPenyerah = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->penyerah)));
            $rPenerima = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->penerima)));

            if ($rPenyerah != NULL && $rPenerima != NULL) {
                $waktu = $this->formatdate->getDateTime($record->waktu);
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->long . '___' . $record->lat . '" class="mapBtn btn btn-info btn-flat">Peta</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'penyerah' => ucwords($rPenyerah->nama), 'penerima' => ucwords($rPenerima->nama),
                    'selama' => $record->selama,
                    'lokasi' => $record->lokasi . '<hr><b>longitude:</b> ' . $record->long . ', <b>latitude:</b> ' . $record->lat,
                    'waktu' => substr($waktu, 0, (strlen($waktu) - 3)),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
