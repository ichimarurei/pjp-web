<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelteror
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelTeror extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_komplain';
    }

    public function doAction($params) {
        $this->setValues($params);
        // overwrite
        $this->setValue('tanggal_aduan', $this->formatdate->setDate($params['tanggal_aduan-input']));

        if ($params['tanggal_proses-input'] !== '') {
            $this->setValue('tanggal_proses', $this->formatdate->setDate($params['tanggal_proses-input']));
        }

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $pembuat = array(
            'field' => 'pembuat-input', 'label' => 'Data Pembuat',
            'rules' => 'trim|required'
        );
        $penerima = array(
            'field' => 'penerima-input', 'label' => 'Data Penerima',
            'rules' => 'trim|required'
        );
        $pihak1 = array(
            'field' => 'pihak_satu-input', 'label' => 'Data Diketahui Oleh (Pihak Pertama)',
            'rules' => 'trim|required'
        );
        $pihak2 = array(
            'field' => 'pihak_dua-input', 'label' => 'Data Diketahui Oleh (Pihak Kedua)',
            'rules' => 'trim|required'
        );
        $klien = array(
            'field' => 'klien-input', 'label' => 'Data Klien',
            'rules' => 'trim|required'
        );
        $tanggal = array(
            'field' => 'tanggal_aduan-input', 'label' => 'Tanggal Komplain',
            'rules' => 'trim|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'Nomor Komplain',
            'rules' => 'trim|max_length[50]|required'
        );
        $lokasi = array(
            'field' => 'lokasi-input', 'label' => 'Lokasi',
            'rules' => 'trim|max_length[100]'
        );
        $komplain = array(
            'field' => 'komplain-input', 'label' => 'Detail Komplain',
            'rules' => 'trim|required'
        );
        $setuju = array(
            'field' => 'setuju-input', 'label' => 'Status Persetujuan Klien',
            'rules' => 'trim|required'
        );

        return array(
            $kode, $proyek, $pembuat, $penerima, $pihak1, $pihak2, $nomor, $tanggal, $klien, $lokasi, $komplain, $setuju
        );
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'pembuat' => '', 'penerima' => '', 'pihak_satu' => '', 'pihak_dua' => '',
            'tanggal_aduan' => '', 'tanggal_proses' => '', 'nomor' => '', 'klien' => '',
            'lokasi' => '', 'komplain' => '', 'setuju' => 0,
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'nomor' => strtoupper($record->nomor),
                'pembuat' => $record->pembuat, 'penerima' => $record->penerima,
                'pihak_satu' => $record->pihak_satu, 'pihak_dua' => $record->pihak_dua,
                'klien' => $record->klien, 'lokasi' => $record->lokasi, 'komplain' => $record->komplain,
                'tanggal_aduan' => $this->formatdate->getDate($record->tanggal_aduan, TRUE),
                'tanggal_proses' => $this->formatdate->getDate($record->tanggal_proses, TRUE),
                'setuju' => $record->setuju,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'tanggal_aduan desc')) as $record) {
            $rPembuat = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pembuat)));
            $rPenerima = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->penerima)));
            $rKlien = $this->getRecord(array('table' => 'data_proyek_klien', 'where' => array('kode' => $record->klien)));

            if ($rPembuat !== NULL && $rPenerima !== NULL && $rKlien !== NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'pembuat' => ucwords($rPembuat->nama), 'penerima' => ucwords($rPenerima->nama),
                    'klien' => ucwords($rKlien->nama) . ' (<b>Telp:' . $rKlien->telepon . '</b>)',
                    'nomor' => strtoupper($record->nomor), 'lokasi' => $record->lokasi,
                    'aduan' => $this->formatdate->getDate($record->tanggal_aduan),
                    'proses' => $this->formatdate->getDate($record->tanggal_proses),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
