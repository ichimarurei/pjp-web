<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelcuti
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelCuti extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_presensi_cuti';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('waktu', $this->formatdate->setDate($params['waktu-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Biodata',
            'rules' => 'trim|required'
        );
        $atasan = array(
            'field' => 'atasan-input', 'label' => 'Data Profil Penyetuju',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Tanggal Cuti',
            'rules' => 'trim|required'
        );
        $selama = array(
            'field' => 'selama-input', 'label' => 'Lama Cuti (Hari)',
            'rules' => 'trim|required'
        );
        $status = array(
            'field' => 'status-input', 'label' => 'Status Cuti',
            'rules' => 'trim|required'
        );
        $keterangan = array(
            'field' => 'keterangan-input', 'label' => 'Keterangan',
            'rules' => 'trim|required'
        );

        return array($kode, $proyek, $biodata, $atasan, $waktu, $selama, $status, $keterangan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'atasan' => '-', 'biodata' => $this->session->userdata('_bio'), 'waktu' => '', 'selama' => 1,
            'status' => 'ajuan', 'keterangan' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'atasan' => $record->atasan, 'biodata' => $record->biodata,
                'waktu' => $this->formatdate->getDate($record->waktu, TRUE),
                'selama' => $record->selama, 'status' => $record->status, 'keterangan' => $record->keterangan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $where = array('terpakai' => 1, 'YEAR(waktu)' => date('Y'));

        if ($query != NULL) {
            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);

                if ($queries[0] !== 'all') {
                    $where['proyek'] = $queries[0];
                }

                if ($queries[1] !== 'all') {
                    $where['biodata'] = $queries[1];
                }
            }
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'waktu desc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));

            if ($rProyek != NULL && $rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'id' => strtoupper($rBiodata->id),
                    'biodata' => ucwords($rBiodata->nama),
                    'waktu' => $this->formatdate->getDate($record->waktu),
                    'selama' => $record->selama . ' Hari',
                    'status' => strtoupper($record->status),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
