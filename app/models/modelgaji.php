<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelgaji
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelGaji extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_gaji';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $presensi = array(
            'field' => 'presensi-input', 'label' => 'Data Presensi',
            'rules' => 'trim|required'
        );
        $biodata = array(
            'field' => 'biodata-input', 'label' => 'Data Pegawai',
            'rules' => 'trim|required'
        );
        $gaji = array(
            'field' => 'gaji-input', 'label' => 'Gaji',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $tTetap = array(
            'field' => 'tunjangan_tetap-input', 'label' => 'Tunjangan Tetap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $tRancu = array(
            'field' => 'tunjangan_rancu-input', 'label' => 'Tunjangan Tidak Tetap',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $thr = array(
            'field' => 'thr-input', 'label' => 'THR',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $rapel = array(
            'field' => 'rapel-input', 'label' => 'Rapel',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $paket = array(
            'field' => 'paket-input', 'label' => 'Lembur (Paket)',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $insentif = array(
            'field' => 'insentif-input', 'label' => 'Insentif',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $lembur = array(
            'field' => 'lembur-input', 'label' => 'Lembur',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $klaim = array(
            'field' => 'klaim-input', 'label' => 'Klaim Kesehatan',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $pHadir = array(
            'field' => 'pot_hadir-input', 'label' => 'Potongan Hadir',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $pLain = array(
            'field' => 'pot_lain_isi-input', 'label' => 'Potongan Lainnya',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array(
            $kode, $presensi, $biodata, $proyek, $pHadir, $pLain, $gaji, $tTetap, $tRancu, $thr, $rapel,
            $paket, $lembur, $insentif, $klaim
        );
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'presensi' => '', 'biodata' => '', 'status' => 'proses',
            'gaji' => '', 'tunjangan_tetap' => '', 'tunjangan_rancu' => '',
            'thr' => '', 'rapel' => '', 'paket' => '',
            'insentif' => '', 'lembur' => '', 'klaim' => '',
            'pot_hadir' => '', 'pot_lain_info' => '', 'pot_lain_isi' => '',
            'waktu' => date('Y-m-d H:i:s'), 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'presensi' => $record->presensi, 'biodata' => $record->biodata,
                'waktu' => $record->waktu, 'status' => $record->status, 'pot_lain_info' => $record->pot_lain_info,
                'gaji' => $this->toRp($record->gaji), 'thr' => $this->toRp($record->thr), 'rapel' => $this->toRp($record->rapel),
                'tunjangan_tetap' => $this->toRp($record->tunjangan_tetap), 'tunjangan_rancu' => $this->toRp($record->tunjangan_rancu),
                'paket' => $this->toRp($record->paket), 'insentif' => $this->toRp($record->insentif), 'lembur' => $this->toRp($record->lembur),
                'klaim' => $this->toRp($record->klaim), 'pot_lain_isi' => $this->toRp($record->pot_lain_isi), 'pot_hadir' => $this->toRp($record->pot_hadir),
                'terpakai' => $record->terpakai
            );

            if ($record->entitas > 0) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek, 'terpakai' => 1)));
                $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->biodata)));
                $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->biodata, 'terpakai' => 1, 'status' => 'PKWT'), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir

                if ($rProyek != NULL && $rBiodata != NULL && $rKontrak != NULL) {
                    $period = explode(' ', $this->formatdate->getDate($record->waktu));
                    $plus = $record->gaji + $record->tunjangan_tetap + $record->tunjangan_rancu + $record->thr + $record->paket + $record->insentif + $record->rapel + $record->klaim + $record->lembur;
                    $minus = $record->pot_hadir + $record->pot_lain_isi;
                    $total = $plus - $minus;
                    $sebagai = '-';

                    // kontrak masih berlaku
                    if ($rKontrak->habis >= date('Y-m-d')) {
                        $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));

                        if ($rJabatan != NULL) {
                            $sebagai = ucwords($rJabatan->jabatan);
                        }
                    }

                    $data['nama'] = strtoupper($rBiodata->nama);
                    $data['nik'] = strtoupper($rBiodata->id);
                    $data['jabatan'] = strtoupper($sebagai);
                    $data['periode'] = strtoupper($period[1]) . ' ' . $period[2];
                    $data['plus'] = $this->toRp($plus);
                    $data['minus'] = $this->toRp($minus);
                    $data['total'] = $this->toRp($total);
                    $data['terbilang'] = $this->terbilang->format($total);
                }
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = explode('___', $query);
        $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $queries[0], 'terpakai' => 1)));

        if ($rProyek != NULL) {
            $status = array('proses' => 'PROSES', 'bayar' => 'SUDAH DIBAYAR', 'na' => 'BELUM DIPROSES');
            $hariKerja = 0;
            $isUseDay = FALSE;
            $lPresensi = array();
            $rPresensi = $this->getRecord(array('table' => 'data_presensi_rekap', 'where' => array('kode' => $queries[1], 'jenis' => 'presensi', 'status' => 'setuju', 'terpakai' => 1)));

            if ($rPresensi != NULL) {
                $objDari = new DateTime($rPresensi->dari);
                $objHingga = new DateTime($rPresensi->hingga);
                $diff = $objHingga->diff($objDari);
                $hariKerja = (($diff != NULL) ? (intval($diff->d) + 1) : 0);

                if ($hariKerja > 26) {
                    $hariKerja = 26;
                }

                $isUseDay = ($hariKerja < 23);

                foreach ($this->getList(array('table' => 'data_presensi_arsip', 'where' => array('rekap' => $rPresensi->kode, 'jenis' => 'presensi', 'terpakai' => 1))) as $record) {
                    $rData = $this->getRecord(array('table' => 'data_presensi_info', 'where' => array('kode' => $record->data)));

                    if ($rData != NULL) {
                        $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $rData->biodata, 'terpakai' => 1)));

                        if ($rBiodata != NULL) {
                            $waktu = explode(' ', $rData->waktu);
                            $lPresensi[$rBiodata->kode][$waktu[0]] = array($rData, $rBiodata);
                        }
                    }
                }
            }

            foreach ($this->getList(array('table' => 'data_biodata', 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
                $rKontrak = $this->getRecord(array('table' => 'data_proyek_kontrak', 'where' => array('biodata' => $record->kode, 'proyek' => $rProyek->kode, 'status' => 'PKWT', 'terpakai' => 1), 'sort' => 'berlaku desc')); // ambil status pegawai terakhir
                $sebagai = '';
                $isShow = FALSE;
                $rJabatan = NULL;
                $gajian = 0;
                $tetapan = 0;
                $rancuan = 0;

                if ($rKontrak != NULL && $rProyek != NULL) {
                    // kontrak masih berlaku
                    if ($rKontrak->habis >= date('Y-m-d')) {
                        $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $rKontrak->jabatan, 'terpakai' => 1)));
                        $sebagai = (($rJabatan != NULL) ? ucwords($rJabatan->jabatan) : '-');
                        $isShow = TRUE;
                        $gajian = $rKontrak->gaji;
                        $tetapan = $rKontrak->tunjangan_tetap;
                        $rancuan = $rKontrak->tunjangan_rancu;
                    }
                }

                if ($isShow) {
                    $rGaji = NULL;
                    $total = 0;

                    if ($rPresensi != NULL) {
                        $rGaji = $this->getRecord(array('table' => $this->table, 'where' => array('proyek' => $rProyek->kode, 'presensi' => $rPresensi->kode, 'biodata' => $record->kode, 'terpakai' => 1)));

                        if ($rGaji != NULL) {
                            $plus = $rGaji->gaji + $rGaji->tunjangan_tetap + $rGaji->tunjangan_rancu + $rGaji->thr + $rGaji->paket + $rGaji->insentif + $rGaji->rapel + $rGaji->klaim + $rGaji->lembur;
                            $minus = $rGaji->pot_hadir + $rGaji->pot_lain_isi;
                            $total = $plus - $minus;
                        }
                    }

                    $linkBtn = '<a href="' . (($rGaji == NULL) ? '0' : $rGaji->kode) . '" '
                            . 'data-nik="' . strtoupper($record->id) . '" data-nama="' . ucwords($record->nama) . '" data-who="' . $record->kode . '" '
                            . 'data-gajian="' . $this->toRp($gajian) . '" data-tetapan="' . $this->toRp($tetapan) . '" data-rancuan="' . $this->toRp($rancuan) . '" '
                            . 'class="actionBtn btn btn-primary btn-flat">Proses</a>';
                    //$linkBtn .= ' <a href="' . (($rGaji == NULL) ? '0' : $rGaji->kode) . '" class="printBtn btn btn-success btn-flat">Cetak</a>';
                    $data[] = array(
                        'kode' => (($rGaji == NULL) ? '0' : $rGaji->kode),
                        'id' => strtoupper($record->id),
                        'biodata' => ucwords($record->nama),
                        'tanggal' => (($rGaji != NULL) ? $this->formatdate->getDate($rGaji->waktu) : ''),
                        'status' => $status[(($rGaji != NULL) ? $rGaji->status : 'na')],
                        'nominal' => $this->toRp($total),
                        'jabatan' => $sebagai,
                        'aksi' => $linkBtn
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
