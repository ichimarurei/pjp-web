<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelinvoice
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelInvoice extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_invoice';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $klien = array(
            'field' => 'klien-input', 'label' => 'Data Klien',
            'rules' => 'trim|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'No Invoice',
            'rules' => 'trim|max_length[100]|required'
        );

        return array($kode, $nomor, $klien, $proyek);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'penyetuju' => '-', 'klien' => '',
            'nomor' => '', 'status' => 'proses',
            'thp' => '', 'perusahaan' => '', 'peralatan' => '', 'fee' => '', 'bulat' => '',
            'total' => '', 'pph' => '', 'ppn' => '', 'waktu' => date('Y-m-d H:i:s'),
            'catatan' => '1. Penawaran bersifat Lumpsum untuk periode YYYY - YYYY' . "\n"
            . '2. Salary Petugas Keamanan sesuai dengan PROYEKSI UMP DKI YYYY - YYYY' . "\n"
            . '3. ATK dan Kelengkapan Tugas akan diberikan oleh PT.Padma Jaga Persada' . "\n"
            . '4. Jika ada Perubahan UMP khusus maka akan di revisi sesuai ketentuan perjanjian kerjasama' . "\n"
            . '5. Evaluasi Kerja akan dilakukan setiap 6 Bulan selama kontrak kerja',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nomor' => strtoupper($record->nomor), 'klien' => strtoupper($record->klien),
                'proyek' => $record->proyek, 'penyetuju' => $record->penyetuju,
                'thp' => $record->thp, 'perusahaan' => $record->perusahaan,
                'peralatan' => $record->peralatan, 'fee' => $record->fee,
                'total' => $record->total, 'ppn' => $record->ppn, 'pph' => $record->pph, 'bulat' => $record->bulat,
                'waktu' => $record->waktu, 'catatan' => $record->catatan, 'status' => $record->status,
                'terpakai' => $record->terpakai
            );

            if ($record->entitas > 0) {
                $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

                if ($rProyek != NULL) {
                    $period = explode(' ', $this->formatdate->getDate($record->waktu));
                    $totalInv = $record->total;

                    if ($record->bulat > 0) {
                        $totalInv = $record->bulat;
                    }

                    $data['tanggal'] = $this->formatdate->getDate($record->waktu);
                    $data['periode'] = strtoupper($period[1]) . ' ' . $period[2];
                    $data['pt'] = ucwords($rProyek->proyek);
                    $data['alamat'] = ucwords($rProyek->alamat);
                    $data['rp_fee'] = $this->toRp($record->fee);
                    $data['rp_total'] = $this->toRp($record->total);
                    $data['rp_pph'] = $this->toRp($record->pph);
                    $data['rp_ppn'] = $this->toRp($record->ppn);
                    $data['rp_bulat'] = $this->toRp($record->bulat);
                    $data['rp_terbilang'] = $this->terbilang->format($totalInv);
                    $data['persen_fee'] = $this->fixDec($rProyek->fee) . '%';
                    $data['persen_pph'] = $this->fixDec($rProyek->pph) . '%';
                    $data['persen_ppn'] = $this->fixDec($rProyek->ppn) . '%';
                }
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = explode('___', $query);
        $status = array('proses' => 'DITAGIHKAN', 'bayar' => 'SUDAH DIBAYAR');

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $queries[0], 'status' => $queries[1]), 'sort' => 'nomor asc')) as $record) {
            $rProyek = $this->getRecord(array('table' => 'data_proyek_info', 'where' => array('kode' => $record->proyek)));

            if ($rProyek != NULL) {
                $linkBtn = '<a href="' . $record->kode . '/' . $rProyek->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';

                if ($record->status === 'proses') {
                    $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                }

                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($rProyek->proyek),
                    'nomor' => strtoupper($record->nomor),
                    'waktu' => $this->formatdate->getDate($record->waktu),
                    'status' => $status[$record->status],
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
