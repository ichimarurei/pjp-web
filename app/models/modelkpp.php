<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkpp
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKPP extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_kpp';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('waktu', $this->formatdate->setDateTime($params['waktu-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $danru = array(
            'field' => 'danru-input', 'label' => 'Data Danru',
            'rules' => 'trim|required'
        );
        $satpam = array(
            'field' => 'satpam-input', 'label' => 'Data Satpam',
            'rules' => 'trim|required'
        );
        $pelaksana = array(
            'field' => 'pelaksana-input', 'label' => 'Data Pelaksana',
            'rules' => 'trim|required'
        );
        $perwakilan = array(
            'field' => 'perwakilan-input', 'label' => 'Data Perwakilan',
            'rules' => 'trim|required'
        );
        $waktu = array(
            'field' => 'waktu-input', 'label' => 'Tanggal Kunjungan',
            'rules' => 'trim|required'
        );
        $long = array(
            'field' => 'long-input', 'label' => 'Longitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $lat = array(
            'field' => 'lat-input', 'label' => 'Latitude',
            'rules' => 'trim|max_length[50]|required'
        );
        $hasil = array(
            'field' => 'hasil-input', 'label' => 'Hasil Kunjungan',
            'rules' => 'trim|required'
        );
        $seragam = array(
            'field' => 'seragam-input', 'label' => 'KPI Seragam',
            'rules' => 'trim|max_length[50]|required'
        );
        $sikap = array(
            'field' => 'sikap-input', 'label' => 'KPI Sikap',
            'rules' => 'trim|max_length[50]|required'
        );
        $rapih = array(
            'field' => 'rapih-input', 'label' => 'KPI Penampilan/Kerapihan',
            'rules' => 'trim|max_length[50]|required'
        );
        $rajin = array(
            'field' => 'rajin-input', 'label' => 'KPI Kerajinan',
            'rules' => 'trim|max_length[50]|required'
        );
        $komunikasi = array(
            'field' => 'komunikasi-input', 'label' => 'KPI Komunikasi',
            'rules' => 'trim|max_length[50]|required'
        );
        $respon = array(
            'field' => 'respon-input', 'label' => 'KPI Respon Terhadap Masalah',
            'rules' => 'trim|max_length[50]|required'
        );
        $shift1 = array(
            'field' => 'shift_satu-input', 'label' => 'Data Shift',
            'rules' => 'trim|max_length[50]|required'
        );
        $shift2 = array(
            'field' => 'shift_dua-input', 'label' => 'Data Shift',
            'rules' => 'trim|max_length[50]|required'
        );
        $shift3 = array(
            'field' => 'shift_tiga-input', 'label' => 'Data Shift',
            'rules' => 'trim|max_length[50]|required'
        );

        return array(
            $kode, $proyek, $danru, $satpam, $pelaksana, $perwakilan, $long, $lat, $waktu, $hasil, $seragam, $sikap,
            $rajin, $rapih, $komunikasi, $respon, $shift1, $shift2, $shift3
        );
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'chief' => '', 'danru' => '', 'satpam' => '',
            'pelaksana' => '', 'perwakilan' => '', 'waktu' => '', 'long' => '',
            'lat' => '', 'hasil' => '', 'seragam' => '', 'sikap' => '',
            'rapih' => '', 'rajin' => '', 'komunikasi' => '', 'respon' => '',
            'shift_satu' => '', 'shift_dua' => '', 'shift_tiga' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'chief' => $record->chief,
                'danru' => $record->danru, 'satpam' => $record->satpam,
                'pelaksana' => $record->pelaksana, 'perwakilan' => $record->perwakilan,
                'long' => $record->long, 'lat' => $record->lat,
                'hasil' => $record->hasil, 'seragam' => $record->seragam,
                'sikap' => $record->sikap, 'rapih' => $record->rapih,
                'rajin' => $record->rajin, 'komunikasi' => $record->komunikasi,
                'respon' => $record->respon, 'shift_satu' => $record->shift_satu,
                'shift_dua' => $record->shift_dua, 'shift_tiga' => $record->shift_tiga,
                'waktu' => $this->formatdate->getDateTime($record->waktu, TRUE),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'waktu desc')) as $record) {
            $rChief = NULL;
            $rPelaksana = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pelaksana)));
            $rPerwakilan = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->perwakilan)));

            if ($record->chief !== '' && $record->chief !== '-') {
                $rChief = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->chief)));
            }

            if ($rPelaksana !== NULL && $rPerwakilan !== NULL) {
                $waktu = $this->formatdate->getDateTime($record->waktu);
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->long . '___' . $record->lat . '" class="mapBtn btn btn-info btn-flat">Peta</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'chief' => ($rChief !== NULL) ? ucwords($rChief->nama) : '-',
                    'pelaksana' => ucwords($rPelaksana->nama),
                    'perwakilan' => ucwords($rPerwakilan->nama),
                    'waktu' => substr($waktu, 0, (strlen($waktu) - 3)),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
