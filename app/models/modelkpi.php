<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelkpi
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKPI extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_kpi';
    }

    public function doAction($params) {
        $this->setValues($params);
        $this->setValue('tanggal', $this->formatdate->setDate($params['tanggal-input'])); // overwrite

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $pegawai = array(
            'field' => 'pegawai-input', 'label' => 'Data Pegawai',
            'rules' => 'trim|required'
        );
        $pembuat = array(
            'field' => 'pembuat-input', 'label' => 'Data Penilai',
            'rules' => 'trim|required'
        );
        $tanggal = array(
            'field' => 'tanggal-input', 'label' => 'Tanggal Pendataan',
            'rules' => 'trim|required'
        );
        $kpi = array(
            'field' => 'kpi-input', 'label' => 'Item KPI',
            'rules' => 'trim|required'
        );
        $poin = array(
            'field' => 'poin-input', 'label' => 'Total Bobot',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $pegawai, $pembuat, $tanggal, $kpi, $poin);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'pegawai' => '', 'pembuat' => '', 'pengawas' => '', 'penerima' => '',
            'tanggal' => '', 'kpi' => '', 'poin' => '', 'level' => 'pegawai', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => $record->proyek, 'pegawai' => $record->pegawai, 'pembuat' => $record->pembuat,
                'pengawas' => $record->pengawas, 'penerima' => $record->penerima,
                'tanggal' => $this->formatdate->getDate($record->tanggal, TRUE),
                'kpi' => $record->kpi, 'poin' => $record->poin, 'level' => $record->level,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $queries = explode('___', $query);

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $queries[1], 'level' => $queries[0]), 'sort' => 'tanggal desc')) as $record) {
            $rBiodata = $this->getRecord(array('table' => 'data_biodata', 'where' => array('kode' => $record->pegawai)));

            if ($rBiodata != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'biodata' => ucwords($rBiodata->nama),
                    'poin' => $record->poin,
                    'tanggal' => $this->formatdate->getDate($record->tanggal),
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
