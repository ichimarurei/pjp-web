<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelproyek
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelProyek extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_info';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Nama Proyek',
            'rules' => 'trim|max_length[100]|required'
        );
        $tipe = array(
            'field' => 'tipe-input', 'label' => 'Data Jenis Proyek',
            'rules' => 'trim|required'
        );
        $pph = array(
            'field' => 'pph-input', 'label' => 'PPH 23',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $ppn = array(
            'field' => 'ppn-input', 'label' => 'PPN',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $fee = array(
            'field' => 'fee-input', 'label' => 'Management Fee',
            'rules' => 'trim|max_length[11]|numeric|required'
        );
        $personil = array(
            'field' => 'personil-input', 'label' => 'Jumlah Pegawai/Personil',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $proyek, $tipe, $fee, $pph, $ppn, $personil);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'proyek' => '', 'alamat' => '', 'tipe' => '', 'upah' => '', 'biaya' => '',
            'pph' => 0, 'ppn' => 0, 'fee' => 0, 'personil' => 0,
            'tipeText' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'proyek' => ucwords($record->proyek),
                'alamat' => $record->alamat, 'tipe' => $record->tipe,
                'upah' => $record->upah, 'biaya' => $record->biaya,
                'pph' => $this->fixDec($record->pph),
                'ppn' => $this->fixDec($record->ppn),
                'fee' => $this->fixDec($record->fee),
                'personil' => $record->personil,
                'terpakai' => $record->terpakai,
                'tipeText' => ''
            );

            if ($rTipe != NULL) {
                $data['tipeText'] = ucwords($rTipe->tipe);
            }
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'proyek asc')) as $record) {
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));

            if ($rTipe != NULL) {
                $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
                $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
                $data[] = array(
                    'kode' => $record->kode,
                    'proyek' => ucwords($record->proyek),
                    'tipe' => ucwords($rTipe->tipe), 'alamat' => $record->alamat,
                    'aksi' => $linkBtn
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('proyek' => $query), 'sort' => 'proyek asc')) as $record) {
            $rTipe = $this->getRecord(array('table' => 'data_proyek_tipe', 'where' => array('kode' => $record->tipe)));

            if ($rTipe != NULL) {
                array_push($data, array(
                    'id' => $record->kode,
                    'text' => ucwords($record->proyek) . ' | ' . ucwords($rTipe->tipe)
                ));
            }
        }

        return $data;
    }

}
