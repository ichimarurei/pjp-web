<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modeluraian
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelUraian extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_uraian';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $uraian = array(
            'field' => 'uraian-input', 'label' => 'Uraian',
            'rules' => 'trim|max_length[255]|required'
        );
        $penjelasan = array(
            'field' => 'penjelasan-input', 'label' => 'Penjelasan',
            'rules' => 'trim|max_length[100]'
        );
        $jabatan = array(
            'field' => 'jabatan-input', 'label' => 'Data Jabatan',
            'rules' => 'trim|required'
        );

        return array($kode, $uraian, $penjelasan, $jabatan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'uraian' => '', 'penjelasan' => '', 'jabatan' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'uraian' => strtoupper($record->uraian), 'penjelasan' => strtoupper($record->penjelasan),
                'jabatan' => $record->jabatan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'uraian asc')) as $record) {
            $rJabatan = NULL;
            $suffix = '';

            if ($record->jabatan !== '-') {
                $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $record->jabatan, 'terpakai' => 1)));

                if ($rJabatan != NULL) {
                    $suffix = strtoupper($rJabatan->jabatan);
                }
            }

            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $data[] = array(
                'kode' => $record->kode,
                'uraian' => strtoupper($record->uraian) . ' ' . $suffix,
                'penjelasan' => strtoupper($record->penjelasan),
                'aksi' => $linkBtn
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('uraian' => $query), 'sort' => 'uraian asc')) as $record) {
            $rJabatan = NULL;
            $suffix = '';

            if ($record->jabatan !== '-') {
                $rJabatan = $this->getRecord(array('table' => 'data_jabatan', 'where' => array('kode' => $record->jabatan, 'terpakai' => 1)));

                if ($rJabatan != NULL) {
                    $suffix = strtoupper($rJabatan->jabatan);
                }
            }

            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->uraian) . ' ' . $suffix));
        }

        return $data;
    }

}
