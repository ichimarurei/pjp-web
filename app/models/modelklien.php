<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelklien
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelKlien extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_proyek_klien';
    }

    public function doAction($params) {
        $this->setValues($params, array('id-input'));
        $this->setValue('id', preg_replace('/\s+/', '', strtolower($params['id-input'])));
        $this->setValue('pin', $this->cryptorgram->encrypt($params['pin-input']));

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        $idUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.id]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $id = array(
            'field' => 'id-input', 'label' => 'ID',
            'rules' => 'trim|max_length[11]|required' . $idUnik
        );
        $pin = array(
            'field' => 'pin-input', 'label' => 'Sandi',
            'rules' => 'trim|max_length[11]|required'
        );
        $proyek = array(
            'field' => 'proyek-input', 'label' => 'Data Proyek',
            'rules' => 'trim|required'
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Nama Klien',
            'rules' => 'trim|max_length[100]|required'
        );
        $jabatan = array(
            'field' => 'jabatan-input', 'label' => 'Jabatan Pihak Klien',
            'rules' => 'trim|max_length[50]'
        );
        $email = array(
            'field' => 'email-input', 'label' => 'Email Klien',
            'rules' => 'trim|max_length[50]|valid_email'
        );
        $telepon = array(
            'field' => 'telepon-input', 'label' => 'No Telepon Klien',
            'rules' => 'trim|max_length[50]|required'
        );

        return array($kode, $id, $pin, $proyek, $nama, $email, $telepon, $jabatan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'id' => '', 'pin' => '', 'proyek' => '',
            'nama' => '', 'jabatan' => '', 'email' => '', 'telepon' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != NULL) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'id' => $record->id,
                'pin' => $this->cryptorgram->decrypt($record->pin),
                'proyek' => $record->proyek,
                'nama' => ucwords($record->nama), 'jabatan' => strtoupper($record->jabatan),
                'email' => $record->email, 'telepon' => $record->telepon,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $query), 'sort' => 'nama asc')) as $record) {
            $linkBtn = '<a href="' . $record->kode . '" class="actionBtn btn btn-primary btn-flat">Lihat</a>';
            $linkBtn .= ' <a href="' . $record->kode . '" class="removeBtn btn btn-danger btn-flat">Hapus</a>';
            $data[] = array(
                'kode' => $record->kode,
                'id' => $record->id,
                'nama' => ucwords($record->nama), 'jabatan' => strtoupper($record->jabatan),
                'email' => $record->email, 'telepon' => $record->telepon,
                'aksi' => $linkBtn
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $queries = array($query, NULL);

        if (strpos($query, '___') !== FALSE) {
            $queries = explode('___', $query);
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'proyek' => $queries[0]), 'find' => array('nama' => $queries[1]), 'sort' => 'nama asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => ucwords($record->nama) . ' (@' . $record->id . ')'));
        }

        return $data;
    }

}
