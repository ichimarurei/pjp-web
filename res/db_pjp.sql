-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 09, 2019 at 09:55 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pjp`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_akun`
--

DROP TABLE IF EXISTS `data_akun`;
CREATE TABLE `data_akun` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_proyek_info(kode) / -',
  `biodata` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `id` varchar(11) NOT NULL,
  `pin` varchar(255) NOT NULL,
  `otoritas` enum('admin','bos','mhrd','shrd','keu','ope','kpp','pegawai','danru','staf','pic') NOT NULL COMMENT '- admin (Admin Sistem), \n- bos (Owner), \n- mhrd (Manager HRD), \n- shrd (Staff HRD), \n- keu (Keuangan), \n- ope (Operasional), \n- kpp (KPP), \n- pegawai (Karyawan), \n- danru (Chief/Danru), \n- staf (Admin HO), \n- pic (Admin Project)',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_akun`
--

INSERT INTO `data_akun` (`entitas`, `kode`, `proyek`, `biodata`, `id`, `pin`, `otoritas`, `terpakai`) VALUES
(1, '578ef3ddb638618a79912ec21cdea2e7', '-', '18aa54fb043fc501e14959253a4f68de', 'sysdev', '2f3929ba23c0b3dc4af58a825c74cb70b1e11e870c32e07fa9bcf0135959eb32', 'admin', 1),
(2, 'cce160fd78c39a8a595c7498b3253d5f', '-', '3f73be2b5e7731c125e259c2e782f7a7', 'oscar', 'e518bd72f29a256260ad576c53857376aef681c0df12c48937ca05a7489445ed', 'ope', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_bank`
--

DROP TABLE IF EXISTS `data_bank`;
CREATE TABLE `data_bank` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `singkatan` varchar(10) DEFAULT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_bank`
--

INSERT INTO `data_bank` (`entitas`, `kode`, `bank`, `singkatan`, `terpakai`) VALUES
(1, '61ac5a947a20686f93e5d4b55b8f7bf0', 'BANK CENTRAL ASIA', 'BCA', 1),
(2, '8094e7574824ede63adf88ba55279cfb', 'BANK MANDIRI', '', 1),
(3, 'cdb754911d76fb27fa1cc6c60610ed58', 'BANK RAKYAT INDONESIA', 'BRI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_biodata`
--

DROP TABLE IF EXISTS `data_biodata`;
CREATE TABLE `data_biodata` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `id` varchar(21) DEFAULT NULL COMMENT 'ID Pegawai',
  `ktp` varchar(50) DEFAULT NULL,
  `npwp` varchar(50) DEFAULT NULL,
  `bpjs` varchar(50) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `kelamin` enum('cowok','cewek') NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` enum('Islam','Katolik','Protestan','Hindu','Budha') NOT NULL,
  `pendidikan` enum('TK','SD','SMP','SMA','S1','S2','S3') NOT NULL,
  `telepon` varchar(100) NOT NULL COMMENT 'No1_No2',
  `email` varchar(255) DEFAULT NULL,
  `alamat` text NOT NULL,
  `rekening` varchar(100) NOT NULL COMMENT '@data_bank(kode)_Rekening',
  `nikah` enum('belum','nikah','cerai') NOT NULL,
  `anak` int(11) NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_biodata`
--

INSERT INTO `data_biodata` (`entitas`, `kode`, `id`, `ktp`, `npwp`, `bpjs`, `nama`, `kelamin`, `tempat_lahir`, `tanggal_lahir`, `agama`, `pendidikan`, `telepon`, `email`, `alamat`, `rekening`, `nikah`, `anak`, `terpakai`) VALUES
(1, '18aa54fb043fc501e14959253a4f68de', '201908200001', '1', '1', '1', 'System Developer', 'cowok', 'Jakarta', '1988-05-21', 'Islam', 'S1', '021_021', 'uknow@who.me', 'Jakarta Pusat', '61ac5a947a20686f93e5d4b55b8f7bf0_210588', 'belum', 0, 1),
(2, '1e42be33905c62040a34d27bec1b50ab', '201908180001', '2', '2', '2', 'Karyawati Cantik', 'cewek', 'Indonesia', '1988-05-21', 'Islam', 'S1', '1_1', 'cantik@soul.me', 'Jakarta', '61ac5a947a20686f93e5d4b55b8f7bf0_100101010', 'belum', 0, 1),
(3, 'e8169cd65071109c2a0bdf2d6d9af7ea', '201908180002', '0', '0', '0', 'Karyawan Kuat', 'cowok', 'Bulan', '1901-01-01', 'Protestan', 'TK', '0_0', 'who@mail.net', 'Rumah', 'cdb754911d76fb27fa1cc6c60610ed58_4501', 'cerai', 11, 1),
(4, '3f73be2b5e7731c125e259c2e782f7a7', '201908260001', '12345678', '313131331313131', '1234567', 'Oscar Firdaus', 'cowok', 'Jakarta', '1978-08-09', 'Islam', 'SMP', '000000000_000000000', 'oscar.fidaus@pjp.com', 'Bogor Bekasi', '61ac5a947a20686f93e5d4b55b8f7bf0_7878787989', 'nikah', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_jabatan`
--

DROP TABLE IF EXISTS `data_jabatan`;
CREATE TABLE `data_jabatan` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_jabatan`
--

INSERT INTO `data_jabatan` (`entitas`, `kode`, `jabatan`, `keterangan`, `terpakai`) VALUES
(1, '88832021a766ade1bf0961057753767b', 'Chief Executive Officer', 'CEO', 1),
(2, '9c0b4d4da9d3fff164a0e0613d9df82c', 'Chief Operating Officer', 'COO', 1),
(3, '924c1117ea8dd817a63f317e00b0631a', 'Junior Staff', '', 1),
(4, '5082985cfbe5ebcfdc001b2c6d0d78d9', 'Senior Staff', '', 1),
(5, 'af6954cd08a2107b818611b3279ec480', 'Supervisor', '', 1),
(6, '126b3019e67ef6d5e8db5427e8e278bc', 'CHIEF', '', 1),
(7, 'a0694c8559f0dcaf771d5edb299ba14c', 'DANRU', '', 1),
(8, '70e476f9996958bfa585fc6f4ab8344e', 'ANGGOTA', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_arsip`
--

DROP TABLE IF EXISTS `data_presensi_arsip`;
CREATE TABLE `data_presensi_arsip` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `rekap` varchar(32) NOT NULL COMMENT '@data_presensi_rekap(kode)',
  `data` varchar(32) NOT NULL COMMENT '@data_presensi_info(kode) atau @data_presensi_lembur(kode)',
  `jenis` enum('presensi','lembur') NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='daftar presensi yg sudah direkap';

--
-- Dumping data for table `data_presensi_arsip`
--

INSERT INTO `data_presensi_arsip` (`entitas`, `kode`, `rekap`, `data`, `jenis`, `terpakai`) VALUES
(1, '84d320e09e64665e4ca3bc2156019fad', '737cfde0cbbcd0c494436b626b680bab', '51f30d7d6e5b2fb9c8cc22aec2e3a7c3', 'presensi', 1),
(2, 'e4f80257d4a0ac47de0c6f4129689944', '737cfde0cbbcd0c494436b626b680bab', 'bfbc71a600b90d018dfe353a00b6f91a', 'presensi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_catatan`
--

DROP TABLE IF EXISTS `data_presensi_catatan`;
CREATE TABLE `data_presensi_catatan` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `pengubah` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `presensi` varchar(32) NOT NULL COMMENT '@data_presensi_info(kode) / @data_presensi_lembur(kode)',
  `keterangan` text NOT NULL,
  `untuk` enum('presensi','lembur') NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_catatan`
--

INSERT INTO `data_presensi_catatan` (`entitas`, `kode`, `pengubah`, `presensi`, `keterangan`, `untuk`, `terpakai`) VALUES
(1, '032188c3ed705a9723330e97467b70ae', '18aa54fb043fc501e14959253a4f68de', 'bfbc71a600b90d018dfe353a00b6f91a', 'Hadir earlier', 'presensi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_cuti`
--

DROP TABLE IF EXISTS `data_presensi_cuti`;
CREATE TABLE `data_presensi_cuti` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `atasan` varchar(32) NOT NULL COMMENT '@data_biodata(kode)\nyang menyetujui',
  `biodata` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `waktu` date NOT NULL,
  `selama` int(11) NOT NULL DEFAULT '1',
  `status` enum('ajuan','setuju','tolak') NOT NULL DEFAULT 'ajuan',
  `keterangan` text NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_cuti`
--

INSERT INTO `data_presensi_cuti` (`entitas`, `kode`, `proyek`, `atasan`, `biodata`, `waktu`, `selama`, `status`, `keterangan`, `terpakai`) VALUES
(1, '8218cb9555f5d2a3242ff1bdfb23af52', '231cd12fc610448d3cabe383d0b99212', '-', '18aa54fb043fc501e14959253a4f68de', '2019-08-23', 3, 'setuju', 'Mengikuti Kontes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_info`
--

DROP TABLE IF EXISTS `data_presensi_info`;
CREATE TABLE `data_presensi_info` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `biodata` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `status` enum('hadir','izin','alfa','sakit') NOT NULL,
  `waktu` datetime NOT NULL,
  `keterangan` text,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_info`
--

INSERT INTO `data_presensi_info` (`entitas`, `kode`, `proyek`, `biodata`, `status`, `waktu`, `keterangan`, `terpakai`) VALUES
(1, 'bfbc71a600b90d018dfe353a00b6f91a', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', 'hadir', '2019-08-20 04:04:00', '', 0),
(2, '51f30d7d6e5b2fb9c8cc22aec2e3a7c3', '231cd12fc610448d3cabe383d0b99212', '1e42be33905c62040a34d27bec1b50ab', 'sakit', '2019-08-20 06:15:00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_lembur`
--

DROP TABLE IF EXISTS `data_presensi_lembur`;
CREATE TABLE `data_presensi_lembur` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `presensi` varchar(32) NOT NULL COMMENT '@data_presensi_info(kode)',
  `waktu` datetime NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_lembur`
--

INSERT INTO `data_presensi_lembur` (`entitas`, `kode`, `presensi`, `waktu`, `terpakai`) VALUES
(1, '11d02b344e6d656003ab329dba59d29b', 'bfbc71a600b90d018dfe353a00b6f91a', '2019-08-20 06:10:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_pulang`
--

DROP TABLE IF EXISTS `data_presensi_pulang`;
CREATE TABLE `data_presensi_pulang` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `presensi` varchar(32) NOT NULL COMMENT '@data_presensi_info(kode)',
  `waktu` datetime NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_pulang`
--

INSERT INTO `data_presensi_pulang` (`entitas`, `kode`, `presensi`, `waktu`, `terpakai`) VALUES
(1, 'c8264cc8528139a73a9877e607970e93', 'bfbc71a600b90d018dfe353a00b6f91a', '2019-08-20 06:10:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_presensi_rekap`
--

DROP TABLE IF EXISTS `data_presensi_rekap`;
CREATE TABLE `data_presensi_rekap` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `dari` date NOT NULL,
  `hingga` date NOT NULL,
  `status` enum('ajuan','setuju','tolak') NOT NULL DEFAULT 'ajuan',
  `jenis` enum('presensi','lembur') NOT NULL,
  `waktu` datetime NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_presensi_rekap`
--

INSERT INTO `data_presensi_rekap` (`entitas`, `kode`, `proyek`, `dari`, `hingga`, `status`, `jenis`, `waktu`, `terpakai`) VALUES
(1, '737cfde0cbbcd0c494436b626b680bab', '231cd12fc610448d3cabe383d0b99212', '2019-01-01', '2019-11-09', 'setuju', 'presensi', '2019-11-08 22:39:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_apel`
--

DROP TABLE IF EXISTS `data_proyek_apel`;
CREATE TABLE `data_proyek_apel` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `penyerah` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `penerima` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `lokasi` varchar(255) NOT NULL,
  `waktu` datetime NOT NULL,
  `personil` text NOT NULL COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `absen` text COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `bko` text COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `selama` varchar(50) NOT NULL,
  `laporan` text COMMENT 'multi line dengan separator ( ___ )',
  `masalah` text COMMENT 'multi line dengan separator ( ___ )',
  `kondisi` text COMMENT 'multi @data_uraian(kode) dengan format [kode]:[text]=[status] dan separator ( ___ )',
  `lat` varchar(50) NOT NULL,
  `long` varchar(50) NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_apel`
--

INSERT INTO `data_proyek_apel` (`entitas`, `kode`, `proyek`, `penyerah`, `penerima`, `lokasi`, `waktu`, `personil`, `absen`, `bko`, `selama`, `laporan`, `masalah`, `kondisi`, `lat`, `long`, `terpakai`) VALUES
(1, 'f011c8eeaac83209d6d0ba75c8fb0fe4', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', '1e42be33905c62040a34d27bec1b50ab', 'Detos', '2019-08-21 21:05:00', '18aa54fb043fc501e14959253a4f68de:SYSTEM DEVELOPER [201908200001]', '', '1e42be33905c62040a34d27bec1b50ab:KARYAWATI CANTIK [201908180001]', 'Satu Jam', 'laporan satu___laporan dua-satu<br/>dua-dua dua-tiga dua-empat<br/>dua dua dua dua dua dua dua dua___askdjans kjasdkaj dnaksjdna<br/>asda sda fadgs dgsdgsdgsd', 'asd asdas dasdasd___knaskdlnaklsndlaksndklan<br/>adlnalks nalksdnalkdna kldasdkl akdslnaskldn<br/>ansdal nlaksn laksdn', '33182d6077b347fe23e751de520c5e30:HT=rusak___48a1134f6699d3b3036ca1d024906a92:SENTER=kelebihan___5ff763e4c7230fd01f448a472de0fbaa:ROMPI LALIN=100', '-6.2087634', '106.84559899999999', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_gaji`
--

DROP TABLE IF EXISTS `data_proyek_gaji`;
CREATE TABLE `data_proyek_gaji` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `presensi` varchar(32) NOT NULL COMMENT '@data_presensi_rekap(kode)\n#presensi',
  `biodata` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `gaji` int(11) NOT NULL,
  `tunjangan_tetap` int(11) NOT NULL,
  `tunjangan_rancu` int(11) NOT NULL,
  `thr` int(11) NOT NULL,
  `rapel` int(11) NOT NULL,
  `paket` int(11) NOT NULL,
  `insentif` int(11) NOT NULL,
  `lembur` int(11) NOT NULL,
  `klaim` int(11) NOT NULL,
  `pot_hadir` int(11) NOT NULL,
  `pot_lain_isi` int(11) NOT NULL,
  `pot_lain_info` text,
  `status` enum('proses','bayar') NOT NULL,
  `waktu` datetime NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_info`
--

DROP TABLE IF EXISTS `data_proyek_info`;
CREATE TABLE `data_proyek_info` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `tipe` varchar(32) NOT NULL COMMENT '@data_proyek_tipe(kode)',
  `proyek` varchar(100) NOT NULL,
  `alamat` text,
  `upah` text,
  `biaya` text,
  `fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ppn` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pph` decimal(10,2) NOT NULL DEFAULT '0.00',
  `personil` int(11) NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_info`
--

INSERT INTO `data_proyek_info` (`entitas`, `kode`, `tipe`, `proyek`, `alamat`, `upah`, `biaya`, `fee`, `ppn`, `pph`, `personil`, `terpakai`) VALUES
(1, '231cd12fc610448d3cabe383d0b99212', '2cedf33e3937cab608c8c7ce6d551c5d', 'PAM GEDUNG CIMB NIAGA', 'JABODETABEK', '640646b613ff5475558ce5e5f74f3aaf|GAJI/UPAH =Rp. 4.000.000:6933848c328b1ba892b8cdc45f5e77fe|TUNJANGAN JABATAN SUPERVISOR=Rp. 500.000:2c4393ab85c8c4663a825a362a2e908a|TUNJANGAN TRANSPORT SUPERVISOR =Rp. 1.100.000:bf251db8ee4c10981bfd2b6cb8f3d863|TUNJANGAN LEMBUR OTOMATIS 12 JAM =Rp. 0', '23a5be1d29b4d626bac2dfe0bc747e07|BPJS TENAGA KERJA =6.24 %:932c3b79d9d55b0e9eadfa423cb157f7|BPJS KESEHATAN =4 %:04488c6dca14e26b389281f0304acb29|SERAGAM 2 STEL PER PERSONIL =Rp. 450.000:1d7a1ddece6ce37b1f372f6dfbfca88f|PELATIHAN RUTIN 2X SEBULAN DAN DAMKAR =Rp. 600.000:20785dcea119cd99bb21a04d019da07f|SEWA KENDARAAN BERMOTOR =Rp. 7.200.000:900080e7acc2030f9a7183b2fc65a01e|ALAT KOMUNIKASI DIGITAL =Rp. 1.500.000:48a1134f6699d3b3036ca1d024906a92|SENTER =Rp. 0:555dbc2f1e888bf185a8b739b563cbdf|JAS HUJAN + PAYUNG =Rp. 0', '10.00', '10.00', '2.00', 25, 1),
(2, '3b602e1792ca735655f9864509a0f732', '311cd71ff2e8b99ab0e9fac0952315dd', 'PT GUNUNG RAYA MAS', 'Jl. Taruna Jaya Abadi No.15', '640646b613ff5475558ce5e5f74f3aaf|GAJI/UPAH =Rp. 3.648.000:6933848c328b1ba892b8cdc45f5e77fe|TUNJANGAN JABATAN SUPERVISOR=Rp. 500.000', '23a5be1d29b4d626bac2dfe0bc747e07|BPJS TENAGA KERJA =6,24 %', '10.00', '10.00', '2.00', 7, 1),
(3, '7ee50d99967bea56ec037c4b3d9aaabc', '311cd71ff2e8b99ab0e9fac0952315dd', 'HO Padma Jaga Persada', 'D Estrella Pekapuran Depok', '', '', '0.00', '0.00', '0.00', 0, 1),
(4, '11e04b4a141887d7e6942d954e783c8b', '311cd71ff2e8b99ab0e9fac0952315dd', 'PT. TANTO INTIM LINE', 'Jl. Yos Sudarso No.36 Kb. Bawang, Tj Priok, Kota Jakarta Utara Kota Jakarta 14230', '640646b613ff5475558ce5e5f74f3aaf|GAJI/UPAH =Rp. 3.940.973:6933848c328b1ba892b8cdc45f5e77fe|TUNJANGAN JABATAN SUPERVISOR=Rp. 500.000', '23a5be1d29b4d626bac2dfe0bc747e07|BPJS TENAGA KERJA =Rp. 227.637:932c3b79d9d55b0e9eadfa423cb157f7|BPJS KESEHATAN =Rp. 182.402:04488c6dca14e26b389281f0304acb29|SERAGAM 2 STEL PER PERSONIL =Rp. 916.667', '8.00', '10.00', '2.00', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_invoice`
--

DROP TABLE IF EXISTS `data_proyek_invoice`;
CREATE TABLE `data_proyek_invoice` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `penyetuju` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_biodata(kode) atau ''-''',
  `klien` varchar(32) NOT NULL COMMENT '@data_proyek_klien(kode)',
  `nomor` varchar(100) NOT NULL,
  `catatan` text,
  `thp` text NOT NULL COMMENT 'format[urai::volume::biaya::total] separator ( ___ )',
  `perusahaan` text NOT NULL COMMENT 'format[urai::volume::biaya::total] separator ( ___ )',
  `peralatan` text NOT NULL COMMENT 'format[urai::volume::biaya::total] separator ( ___ )',
  `fee` int(11) NOT NULL DEFAULT '0',
  `ppn` int(11) NOT NULL DEFAULT '0',
  `pph` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `bulat` int(11) NOT NULL DEFAULT '0',
  `waktu` datetime NOT NULL,
  `status` enum('proses','bayar') NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_invoice`
--

INSERT INTO `data_proyek_invoice` (`entitas`, `kode`, `proyek`, `penyetuju`, `klien`, `nomor`, `catatan`, `thp`, `perusahaan`, `peralatan`, `fee`, `ppn`, `pph`, `total`, `bulat`, `waktu`, `status`, `terpakai`) VALUES
(1, '73bbec48de3e36f6b5e2dbf39381c9d4', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', 'dab6c3f5b3f5ee3c2f8b7448567786fe', '0001/PJP-INV/XI/19', '1. Penawaran bersifat Lumpsum untuk periode YYYY - YYYY\n2. Salary Petugas Keamanan sesuai dengan PROYEKSI UMP DKI YYYY - YYYY\n3. ATK dan Kelengkapan Tugas akan diberikan oleh PT.Padma Jaga Persada\n4. Jika ada Perubahan UMP khusus maka akan di revisi sesuai ketentuan perjanjian kerjasama\n5. Evaluasi Kerja akan dilakukan setiap 6 Bulan selama kontrak kerja', 'GAJI/UPAH::25::Rp. 4.000.000::Rp. 100.000.000___TUNJANGAN JABATAN SUPERVISOR::25::Rp. 500.000::Rp. 12.500.000___TUNJANGAN TRANSPORT SUPERVISOR::25::Rp. 1.100.000::Rp. 27.500.000___TUNJANGAN LEMBUR OTOMATIS 12 JAM::25::Rp. 0::Rp. 0', 'BPJS TENAGA KERJA::25::Rp. 249.600::Rp. 6.240.000___BPJS KESEHATAN::25::Rp. 160.000::Rp. 4.000.000___SERAGAM 2 STEL PER PERSONIL::50::Rp. 22.500.000::Rp. 1.875.000___PELATIHAN RUTIN 2X SEBULAN DAN DAMKAR::25::Rp. 15.000.000::Rp. 1.250.000___THR::25::Rp. 140.000.000::Rp. 11.666.667', 'SEWA KENDARAAN BERMOTOR::1::Rp. 7.200.000::Rp. 600.000___ALAT KOMUNIKASI DIGITAL::1::Rp. 1.500.000::Rp. 125.000___SENTER::1::Rp. 0::Rp. 0___JAS HUJAN + PAYUNG::1::Rp. 0::Rp. 0', 16575667, 1657567, 3679798, 187669699, 0, '2019-11-08 23:30:49', 'proses', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_klaim`
--

DROP TABLE IF EXISTS `data_proyek_klaim`;
CREATE TABLE `data_proyek_klaim` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `pegawai` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pemohon` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `perihal` text NOT NULL,
  `tanggal` date NOT NULL COMMENT 'tanggal pengajuan klaim/reimburse',
  `nominal` int(11) NOT NULL,
  `status` enum('ajuan','tolak','terima','terbayar') NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_klaim`
--

INSERT INTO `data_proyek_klaim` (`entitas`, `kode`, `proyek`, `pegawai`, `pemohon`, `perihal`, `tanggal`, `nominal`, `status`, `terpakai`) VALUES
(1, '8b1161a5411584254cd73b7993f6230f', '231cd12fc610448d3cabe383d0b99212', '1e42be33905c62040a34d27bec1b50ab', '18aa54fb043fc501e14959253a4f68de', 'Makan malam', '2019-08-06', 215000, 'terima', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_klien`
--

DROP TABLE IF EXISTS `data_proyek_klien`;
CREATE TABLE `data_proyek_klien` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `nama` varchar(100) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `id` varchar(11) NOT NULL,
  `pin` varchar(255) NOT NULL,
  `terpakai` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_klien`
--

INSERT INTO `data_proyek_klien` (`entitas`, `kode`, `proyek`, `nama`, `telepon`, `email`, `jabatan`, `id`, `pin`, `terpakai`) VALUES
(1, 'dab6c3f5b3f5ee3c2f8b7448567786fe', '231cd12fc610448d3cabe383d0b99212', 'Kelayen Klien', '021', '', 'BOS', 'klien', 'c690a51196bbf6279eeab12b4404507ce7c8f3be04b6cd8391bcef849c21f9da', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_komplain`
--

DROP TABLE IF EXISTS `data_proyek_komplain`;
CREATE TABLE `data_proyek_komplain` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `pembuat` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `penerima` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pihak_satu` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pihak_dua` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `klien` varchar(32) NOT NULL COMMENT '@data_proyek_klien(kode)',
  `nomor` varchar(50) NOT NULL,
  `tanggal_aduan` date NOT NULL,
  `tanggal_proses` date DEFAULT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `komplain` longtext NOT NULL COMMENT 'multi line separator ( ___ ) format [komplain]|[solusi]|[target]',
  `setuju` tinyint(1) NOT NULL DEFAULT '0',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_komplain`
--

INSERT INTO `data_proyek_komplain` (`entitas`, `kode`, `proyek`, `pembuat`, `penerima`, `pihak_satu`, `pihak_dua`, `klien`, `nomor`, `tanggal_aduan`, `tanggal_proses`, `lokasi`, `komplain`, `setuju`, `terpakai`) VALUES
(1, 'c843c122f0aa82d1a3813bfe46fe8755', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', '1e42be33905c62040a34d27bec1b50ab', '1e42be33905c62040a34d27bec1b50ab', '18aa54fb043fc501e14959253a4f68de', 'dab6c3f5b3f5ee3c2f8b7448567786fe', '0101101001', '2019-08-14', '2019-08-15', 'Bumi', 'asdasd asdasdasd<br/>asdas asdasda sdas asdasdasdasdasda|asda sdaasdasdasda sdasdasdas asda sdasdasdasdasda sdasdasda dasdasdasdas asda sdasd|asda sasgsdg sdgsd gsd<br/>sdg sdg sdgsd gsdgsdgsdgs dgsd<br/>gsd gsdg sdg sdgsd<br/>sdgsdgsdg<br/><br/>sdg sdg sdgs dgsdgsd gsdg sdgsdgsd<br/>gsdgsdgs dgs dgsdgsd sdgsd gsd gsdg sdgsdg sdgsd gsdgsdgdsg___sdfs dfsfsdfsdfsdf sdf|sdfsd fsdfsdf sdfsdfsdf|asfs dfsdfs dfsf sdfsdf___sdfs dfsf|af asfasf as|asf asfa', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_kontrak`
--

DROP TABLE IF EXISTS `data_proyek_kontrak`;
CREATE TABLE `data_proyek_kontrak` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `biodata` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `jabatan` varchar(32) NOT NULL COMMENT '@data_jabatan(kode)',
  `status` enum('PKWT','Resign','Dikeluarkan') NOT NULL,
  `ke` int(11) NOT NULL DEFAULT '1' COMMENT 'gabungan jika status nya PWKT (ke-)',
  `berlaku` date NOT NULL,
  `habis` date NOT NULL,
  `masuk` varchar(5) NOT NULL COMMENT 'hh:mm',
  `selesai` varchar(5) NOT NULL COMMENT 'hh:mm',
  `gaji` int(11) NOT NULL DEFAULT '0',
  `tunjangan_tetap` int(11) NOT NULL DEFAULT '0',
  `tunjangan_rancu` int(11) NOT NULL DEFAULT '0',
  `terdata` date NOT NULL,
  `tetap` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'jika tetap, masa kontrak d buat selama 100 tahun',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_kontrak`
--

INSERT INTO `data_proyek_kontrak` (`entitas`, `kode`, `proyek`, `biodata`, `jabatan`, `status`, `ke`, `berlaku`, `habis`, `masuk`, `selesai`, `gaji`, `tunjangan_tetap`, `tunjangan_rancu`, `terdata`, `tetap`, `terpakai`) VALUES
(1, 'a34ac50ea3b5c4600036be89333d6185', '231cd12fc610448d3cabe383d0b99212', '1e42be33905c62040a34d27bec1b50ab', 'af6954cd08a2107b818611b3279ec480', 'PKWT', 1, '2019-01-01', '2020-12-31', '8:00', '17:00', 3000000, 1500000, 100000, '2019-08-18', 0, 1),
(2, '80369a70e02255c11d2c5c1e8127ae23', '231cd12fc610448d3cabe383d0b99212', 'e8169cd65071109c2a0bdf2d6d9af7ea', '924c1117ea8dd817a63f317e00b0631a', 'PKWT', 1, '2000-01-01', '2010-01-01', '6:45', '17:45', 2500000, 500000, 0, '2019-08-18', 0, 1),
(3, '28301d09647fffec567a83970a539c3c', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', '5082985cfbe5ebcfdc001b2c6d0d78d9', 'PKWT', 1, '2019-08-31', '2119-08-31', '3:00', '22:00', 21000000, 5000000, 198800, '2019-08-20', 1, 1),
(4, '831928a9401c255619e545416152af7a', '7ee50d99967bea56ec037c4b3d9aaabc', '3f73be2b5e7731c125e259c2e782f7a7', '5082985cfbe5ebcfdc001b2c6d0d78d9', 'PKWT', 1, '2019-05-01', '2020-05-01', '8:00', '17:00', 5000000, 3000000, 0, '2019-08-26', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_kpi`
--

DROP TABLE IF EXISTS `data_proyek_kpi`;
CREATE TABLE `data_proyek_kpi` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `pegawai` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pembuat` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pengawas` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_biodata(kode) / -',
  `penerima` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_biodata(kode) / -',
  `tanggal` date NOT NULL,
  `level` enum('pegawai','ketua') NOT NULL DEFAULT 'pegawai',
  `kpi` text NOT NULL COMMENT 'pemisah: (,)\n[kode_poin]:[max_poin]=[actual_poin]',
  `poin` int(11) NOT NULL DEFAULT '0' COMMENT 'total poin yg d dapat',
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_kpi`
--

INSERT INTO `data_proyek_kpi` (`entitas`, `kode`, `proyek`, `pegawai`, `pembuat`, `pengawas`, `penerima`, `tanggal`, `level`, `kpi`, `poin`, `terpakai`) VALUES
(1, '1bec835e5c7b7cce6eca1ab8cf66672b', '231cd12fc610448d3cabe383d0b99212', '1e42be33905c62040a34d27bec1b50ab', '18aa54fb043fc501e14959253a4f68de', '-', '-', '2019-08-01', 'pegawai', 'p1a:3=3,p1b:3=3,p1c:3=3,p1d:4=2,p1e:3=3,p1f:3=2,p1g:3=1,p1h:3=2,p1i:3=2,p2a:3=1,p2b:3=2,p2c:3=3,p2d:3=3,p2e:3=3,p2f:3=3,p2g:3=2,p2h:3=1,p3a:3=2,p3b:3=2,p3c:2=2,p3d:3=3,p3e:4=3,p3f:4=2,p3g:3=1,p3h:3=1,p4a:5=3,p4b:4=2,p4c:3=2,p4d:4=2,p4e:4=2,p4f:3=2', 68, 1),
(2, 'a43ddf927569dbb645135b8c4b1b6bd8', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', '18aa54fb043fc501e14959253a4f68de', '18aa54fb043fc501e14959253a4f68de', '18aa54fb043fc501e14959253a4f68de', '2019-08-02', 'ketua', 'p1a:3=5,p1b:3=5,p1c:3=5,p1d:4=6,p1e:3=5,p1f:3=4,p1g:3=4,p2a:3=2,p2b:3=2,p2c:3=2,p2d:3=3,p2e:3=2,p3a:3=5,p3b:3=3,p3c:2=3,p3d:3=3,p3e:4=1,p4a:5=2,p4b:4=2,p4c:3=2', 66, 1),
(3, '458e4b1a84e14473bfba2871bf18945f', '7ee50d99967bea56ec037c4b3d9aaabc', '3f73be2b5e7731c125e259c2e782f7a7', '18aa54fb043fc501e14959253a4f68de', '3f73be2b5e7731c125e259c2e782f7a7', '-', '2019-08-26', 'pegawai', 'p1a:3=3,p1b:3=3,p1c:3=3,p1d:4=4,p1e:3=3,p1f:3=3,p1g:3=3,p1h:3=3,p1i:3=3,p2a:3=3,p2b:3=3,p2c:3=3,p2d:3=3,p2e:3=3,p2f:3=3,p2g:3=3,p2h:3=3,p3a:3=3,p3b:3=3,p3c:2=2,p3d:3=3,p3e:4=4,p3f:4=4,p3g:3=3,p3h:3=3,p4a:5=5,p4b:4=4,p4c:3=3,p4d:4=4,p4e:4=4,p4f:3=3', 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_kpp`
--

DROP TABLE IF EXISTS `data_proyek_kpp`;
CREATE TABLE `data_proyek_kpp` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `chief` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_biodata(kode) atau ''-'' atau ''''',
  `danru` text NOT NULL COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `satpam` text NOT NULL COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `pelaksana` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `perwakilan` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `shift_satu` varchar(50) NOT NULL,
  `shift_dua` varchar(50) NOT NULL,
  `shift_tiga` varchar(50) NOT NULL,
  `waktu` datetime NOT NULL,
  `seragam` varchar(50) NOT NULL,
  `sikap` varchar(50) NOT NULL,
  `rapih` varchar(50) NOT NULL,
  `rajin` varchar(50) NOT NULL,
  `komunikasi` varchar(50) NOT NULL,
  `respon` varchar(50) NOT NULL,
  `hasil` text NOT NULL COMMENT 'multi line separator ( ___ )',
  `lat` varchar(50) NOT NULL,
  `long` varchar(50) NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_kpp`
--

INSERT INTO `data_proyek_kpp` (`entitas`, `kode`, `proyek`, `chief`, `danru`, `satpam`, `pelaksana`, `perwakilan`, `shift_satu`, `shift_dua`, `shift_tiga`, `waktu`, `seragam`, `sikap`, `rapih`, `rajin`, `komunikasi`, `respon`, `hasil`, `lat`, `long`, `terpakai`) VALUES
(1, '1a7db91bfb835a44858f2677ead8a91b', '231cd12fc610448d3cabe383d0b99212', '1e42be33905c62040a34d27bec1b50ab', '18aa54fb043fc501e14959253a4f68de:SYSTEM DEVELOPER [201908200001]', '1e42be33905c62040a34d27bec1b50ab:KARYAWATI CANTIK [201908180001]', '1e42be33905c62040a34d27bec1b50ab', '18aa54fb043fc501e14959253a4f68de', '1', '2', '3', '2019-09-04 11:30:00', '2', '3', '3', '2', '2', '2', 'hasil pertama___hasil kedua___closing', '-6.2087634', '106.84559899999999', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_ppho`
--

DROP TABLE IF EXISTS `data_proyek_ppho`;
CREATE TABLE `data_proyek_ppho` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `proyek` varchar(32) NOT NULL COMMENT '@data_proyek_info(kode)',
  `chief` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_biodata(kode) atau ''-'' atau ''''',
  `danru` text NOT NULL COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `satpam` text NOT NULL COMMENT 'multi @data_biodata(kode) dengan format [kode]:[nama] separator ( ___ )',
  `pihak_satu` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `pihak_dua` varchar(32) NOT NULL COMMENT '@data_biodata(kode)',
  `tanggal` date NOT NULL,
  `seragam` varchar(50) NOT NULL,
  `sikap` varchar(50) NOT NULL,
  `rapih` varchar(50) NOT NULL,
  `rajin` varchar(50) NOT NULL,
  `komunikasi` varchar(50) NOT NULL,
  `respon` varchar(50) NOT NULL,
  `performa` varchar(50) NOT NULL,
  `keadaan` longtext NOT NULL,
  `lat` varchar(50) NOT NULL,
  `long` varchar(50) NOT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_ppho`
--

INSERT INTO `data_proyek_ppho` (`entitas`, `kode`, `proyek`, `chief`, `danru`, `satpam`, `pihak_satu`, `pihak_dua`, `tanggal`, `seragam`, `sikap`, `rapih`, `rajin`, `komunikasi`, `respon`, `performa`, `keadaan`, `lat`, `long`, `terpakai`) VALUES
(1, 'ab642adafeeb1b14a55fab8d9642bfd2', '231cd12fc610448d3cabe383d0b99212', '18aa54fb043fc501e14959253a4f68de', '18aa54fb043fc501e14959253a4f68de:SYSTEM DEVELOPER [201908200001]', '1e42be33905c62040a34d27bec1b50ab:KARYAWATI CANTIK [201908180001]', '18aa54fb043fc501e14959253a4f68de', '1e42be33905c62040a34d27bec1b50ab', '2019-09-03', '3', '2', '2', '3', '3', '2', '3', 'bagus aja lah biar cepet', '-6.2087634', '106.84559899999999', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_proyek_tipe`
--

DROP TABLE IF EXISTS `data_proyek_tipe`;
CREATE TABLE `data_proyek_tipe` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_proyek_tipe`
--

INSERT INTO `data_proyek_tipe` (`entitas`, `kode`, `tipe`, `keterangan`, `terpakai`) VALUES
(1, '0d0556107b1c66ec22412859bee7226c', 'Proyek A', 'Contoh Jenis Proyek - A', 1),
(2, '2cedf33e3937cab608c8c7ce6d551c5d', 'Proyek B', '', 1),
(3, '311cd71ff2e8b99ab0e9fac0952315dd', 'LOTUS GUARD', '', 1),
(4, '4c1023a711878a9143c3b68bcb7aacaf', 'PADMA CLEANING', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_sesi`
--

DROP TABLE IF EXISTS `data_sesi`;
CREATE TABLE `data_sesi` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_sesi`
--

INSERT INTO `data_sesi` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('c1bfb9d1551f39a3e5487f390c83a607', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0', 1573230766, '');

-- --------------------------------------------------------

--
-- Table structure for table `data_uraian`
--

DROP TABLE IF EXISTS `data_uraian`;
CREATE TABLE `data_uraian` (
  `entitas` int(11) NOT NULL,
  `kode` varchar(32) NOT NULL,
  `jabatan` varchar(32) NOT NULL DEFAULT '-' COMMENT '@data_jabatan(kode) atau ''-''',
  `uraian` varchar(255) NOT NULL,
  `penjelasan` varchar(100) DEFAULT NULL,
  `terpakai` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_uraian`
--

INSERT INTO `data_uraian` (`entitas`, `kode`, `jabatan`, `uraian`, `penjelasan`, `terpakai`) VALUES
(1, '640646b613ff5475558ce5e5f74f3aaf', '-', 'GAJI/UPAH', '(PROYEKSI UMP DKI 2019 - 2020)', 1),
(2, '2c4393ab85c8c4663a825a362a2e908a', '-', 'TUNJANGAN TRANSPORT SUPERVISOR', '(RP 50.000 X 22)', 1),
(3, '6933848c328b1ba892b8cdc45f5e77fe', 'af6954cd08a2107b818611b3279ec480', 'Tunjangan Jabatan', '', 1),
(4, '23a5be1d29b4d626bac2dfe0bc747e07', '-', 'BPJS TENAGA KERJA', '6,24% X UMK', 1),
(5, '33182d6077b347fe23e751de520c5e30', '-', 'HT', '', 1),
(6, '48a1134f6699d3b3036ca1d024906a92', '-', 'Senter', '', 1),
(7, '5ff763e4c7230fd01f448a472de0fbaa', '-', 'Rompi Lalin', '', 1),
(8, '2e413d96b41739712dd786ee97c1e3f7', '-', 'Sepatu Bot', '', 1),
(9, 'fa847f0c08617a15b2a332eddac8d428', '-', 'Tongkat', '', 1),
(10, 'bf251db8ee4c10981bfd2b6cb8f3d863', '-', 'Tunjangan Lembur Otomatis 12 Jam', '', 1),
(11, '932c3b79d9d55b0e9eadfa423cb157f7', '-', 'BPJS KESEHATAN', '4% X UMK', 1),
(12, '04488c6dca14e26b389281f0304acb29', '-', 'Seragam 2 Stel Per Personil', '', 1),
(13, '1d7a1ddece6ce37b1f372f6dfbfca88f', '-', 'Pelatihan Rutin 2x Sebulan dan Damkar', '', 1),
(14, '900080e7acc2030f9a7183b2fc65a01e', '-', 'Alat Komunikasi Digital', '', 1),
(15, '20785dcea119cd99bb21a04d019da07f', '-', 'Sewa Kendaraan Bermotor', '', 1),
(16, '555dbc2f1e888bf185a8b739b563cbdf', '-', 'Jas Hujan + Payung', '', 1),
(17, '60edbd826b150d7bad00d7b5c86514f2', '-', 'Tunjangan Jabatan', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_akun`
--
ALTER TABLE `data_akun`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `data_bank`
--
ALTER TABLE `data_bank`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_biodata`
--
ALTER TABLE `data_biodata`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_jabatan`
--
ALTER TABLE `data_jabatan`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_arsip`
--
ALTER TABLE `data_presensi_arsip`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_catatan`
--
ALTER TABLE `data_presensi_catatan`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_cuti`
--
ALTER TABLE `data_presensi_cuti`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_info`
--
ALTER TABLE `data_presensi_info`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_lembur`
--
ALTER TABLE `data_presensi_lembur`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_pulang`
--
ALTER TABLE `data_presensi_pulang`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_presensi_rekap`
--
ALTER TABLE `data_presensi_rekap`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_apel`
--
ALTER TABLE `data_proyek_apel`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_gaji`
--
ALTER TABLE `data_proyek_gaji`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_info`
--
ALTER TABLE `data_proyek_info`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_invoice`
--
ALTER TABLE `data_proyek_invoice`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_klaim`
--
ALTER TABLE `data_proyek_klaim`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_klien`
--
ALTER TABLE `data_proyek_klien`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_komplain`
--
ALTER TABLE `data_proyek_komplain`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_kontrak`
--
ALTER TABLE `data_proyek_kontrak`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_kpi`
--
ALTER TABLE `data_proyek_kpi`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_kpp`
--
ALTER TABLE `data_proyek_kpp`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_ppho`
--
ALTER TABLE `data_proyek_ppho`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_proyek_tipe`
--
ALTER TABLE `data_proyek_tipe`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- Indexes for table `data_sesi`
--
ALTER TABLE `data_sesi`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `data_uraian`
--
ALTER TABLE `data_uraian`
  ADD PRIMARY KEY (`entitas`),
  ADD UNIQUE KEY `kode_UNIQUE` (`kode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_akun`
--
ALTER TABLE `data_akun`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_bank`
--
ALTER TABLE `data_bank`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_biodata`
--
ALTER TABLE `data_biodata`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_jabatan`
--
ALTER TABLE `data_jabatan`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_presensi_arsip`
--
ALTER TABLE `data_presensi_arsip`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_presensi_catatan`
--
ALTER TABLE `data_presensi_catatan`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_presensi_cuti`
--
ALTER TABLE `data_presensi_cuti`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_presensi_info`
--
ALTER TABLE `data_presensi_info`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_presensi_lembur`
--
ALTER TABLE `data_presensi_lembur`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_presensi_pulang`
--
ALTER TABLE `data_presensi_pulang`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_presensi_rekap`
--
ALTER TABLE `data_presensi_rekap`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_apel`
--
ALTER TABLE `data_proyek_apel`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_gaji`
--
ALTER TABLE `data_proyek_gaji`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_proyek_info`
--
ALTER TABLE `data_proyek_info`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_proyek_invoice`
--
ALTER TABLE `data_proyek_invoice`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_klaim`
--
ALTER TABLE `data_proyek_klaim`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_klien`
--
ALTER TABLE `data_proyek_klien`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_komplain`
--
ALTER TABLE `data_proyek_komplain`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_kontrak`
--
ALTER TABLE `data_proyek_kontrak`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_proyek_kpi`
--
ALTER TABLE `data_proyek_kpi`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_proyek_kpp`
--
ALTER TABLE `data_proyek_kpp`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_ppho`
--
ALTER TABLE `data_proyek_ppho`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_proyek_tipe`
--
ALTER TABLE `data_proyek_tipe`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_uraian`
--
ALTER TABLE `data_uraian`
  MODIFY `entitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
